<?php

/**
 * @file
 * Contains allianz_custom.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityInterface;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\user\Entity\User;

/**
 * Implements hook_help().
 */
function allianz_custom_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the allianz_custom module.
    case 'help.page.allianz_custom':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Custom functionality for allianz') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_form_alter().
 */
function allianz_custom_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_id == 'block_content_overlay_edit_form') {
    $form['field_cta_link']['widget'][0]['uri']['#description'] = 'Start typing the title of the basic page to select it or add external URL such as http://example.com.';
    $form['info']['widget'][0]['value']['#maxlength'] = 160;
  }
  if ($form_id == 'commerce_product_coupon_add_form' || $form_id == 'commerce_product_coupon_edit_form') {
    // Hide add to variation button for coupon.
    unset($form['actions']['submit_continue']);
    // Custom function to validate unique coupon code.
    array_unshift($form['#validate'], 'allianz_custom_coupon_code_field_validation');
    // Disabled coupon stock field.
    $form['field_stock']['widget'][0]['#disabled'] = TRUE;
    // Change end product url field states.
    // If without code option selected in display format field.
    $form['field_coupon_codes']['#states'] = [
      'visible' => [
        'input[name="field_display_format"]' => ['value' => 'with_code'],
      ],
    ];
    $form['field_stock']['#states'] = [
      'visible' => [
        'input[name="field_display_format"]' => ['value' => 'with_code'],
      ],
    ];
    $form['field_attachment']['#states'] = [
      'visible' => [
        'input[name="field_display_format"]' => ['value' => 'with_code'],
      ],
    ];
    $form['field_stock_email_threshold']['#states'] = [
      'visible' => [
        'input[name="field_display_format"]' => ['value' => 'with_code'],
      ],
    ];
    $form['#attached']['library'][] = 'allianz_custom/allianz_custom_css';
  }
  if ($form_id == 'commerce_product_voucher_add_form' || $form_id == 'commerce_product_voucher_edit_form' || $form_id == 'commerce_product_coupon_add_form' || $form_id == 'commerce_product_coupon_edit_form') {
    // Change Availability date field states.
    // If availability option selected in flap field.
    $form['field_availability_date']["widget"][0]['#states'] = [
      'visible' => [
        'input[name="field_flap"]' => ['value' => 'availability'],
      ],
    ];
    // Change stock threshold field states.
    // If stock option selected in flap field.
    $form['field_stock_threshold']['#states'] = [
      'visible' => [
        'input[name="field_flap"]' => ['value' => 'stock'],
      ],
    ];
    // Set default value N/A if default value is empty.
    if (empty($form['field_flap']['widget']['#default_value'])) {
      $form['field_flap']['widget']['#default_value'] = '_none';
    }
    // Get current user role
    $current_user = \Drupal::currentUser();
    $user = User::load($current_user->id());
    $roles = $current_user->getRoles();
    if (in_array('avp_partners', $roles)) {
      $form['field_supplier_id']['#access'] = FALSE;
      $form['field_supplier_id']['widget'][0]['target_id']['#default_value'] = $user;
    }
  }

  if (strpos($form_id, 'commerce_product_variation') === FALSE && strpos($form_id, 'commerce_product_type') === FALSE && strpos($form_id, 'commerce_product_') !== FALSE && (strpos($form_id, '_edit_form') !== FALSE || strpos($form_id, '_add_form') !== FALSE)) {
    // Attach custom css to hide availability date message.
    $form['#attached']['library'][] = 'allianz_custom/allianz_custom_css';
    // Limiting field maxlength.
    $form['title']['widget'][0]['value']['#maxlength'] = 150;
    $form['field_summary']['widget'][0]['value']['#maxlength'] = 250;
    // Adding custom validation for fields value.
    array_unshift($form['#validate'], 'allianz_custom_product_field_validation');
    // Removing Ckeditor help text.
    foreach ($form['field_description']['widget'] as $key => $value) {
      if (is_numeric($key)) {
        $form['field_description']['widget'][$key]['subform']['field_description']['widget']['#after_build'][] = '_allowed_formats_remove_textarea_help';
      }
    }
    // Altering availibility fields help text.
    $form['field_availability_date']['widget'][0]['value']['#attributes']['title'] = 'Date Time (e.g. 20/01/2019 08:30:20 AM)';
    $form['field_availability_date']['widget'][0]['end_value']['#attributes']['title'] = 'Date Time (e.g. 20/01/2019 08:30:20 AM)';
  }
  if ($form_id == 'entity_subqueue_voucher_edit_form') {
    $form['#validate'][] = 'allianz_custom_validate_unique_items';
  }
  if ($form_id == 'user_form' || $form_id == 'user_register_form') {
    // Custom user validate handler to validate password strength.
    $form['#validate'][] = 'allianz_custom_user_validation';
    // Showing supplier email and secret key field if supplier role is checked.
    $form['field_supplier_email']['#states'] = [
      'visible' => [
        'input[name="roles[supplier]"]' => ['checked' => TRUE],
      ],
    ];
    $form['field_secret_key']['#states'] = [
      'visible' => [
        'input[name="roles[supplier]"]' => ['checked' => TRUE],
      ],
    ];
  }
}

/**
 * Extra validation for unique items in premium slider.
 */
function allianz_custom_validate_unique_items(array &$form, FormStateInterface $form_state) {
  $element = $form_state->getTriggeringElement();
  $target_ids = [];
  if ($element['#name'] == 'op') {
    $items = $form_state->getValues()['items'];
    unset($items['add_more']);
    foreach ($items as $key => $value) {
      $target_ids[] = $value['target_id'];
    }
    if (count($target_ids) !== count(array_unique($target_ids))) {
      $form_state->setErrorByName('title', t('There are duplicate items in the list'));
    }
  }

}

/**
 * Callback method to hide text editor help text.
 */
function _allowed_formats_remove_textarea_help($form_element, FormStateInterface $form_state) {
  if (isset($form_element[0]['format'])) {
    unset($form_element[0]['format']['guidelines']);
    unset($form_element[0]['format']['help']);
  }
  return $form_element;
}

/**
 * Implements hook_js_alter().
 */
function allianz_custom_js_alter(&$javascript) {
  $route_name = \Drupal::routeMatch()->getRouteName();
  if ($route_name == 'entity.commerce_product.edit_form' || $route_name == 'entity.commerce_product.add_form') {
    unset($javascript['misc/tabledrag.js']);
  }
}

/**
 * Callback method for product field validation.
 */
function allianz_custom_product_field_validation(array $form, FormStateInterface $form_state) {
  $form_values = $form_state->getValues();
  // Condition to check field availability date.
  if (!empty($form_values['field_flap']) && $form_values['field_flap'][0]['value'] == 'availability') {
    if (empty($form_values['field_availability_date'][0]['value']) || empty($form_values['field_availability_date'][0]['end_value'])) {
      $form_state->setErrorByName('field_availability_date', t('Availability date can not be blank.'));
    }
    else {
      if (is_object($form_values['field_availability_date'][0]['value']) && is_object($form_values['field_availability_date'][0]['end_value'])) {
        $current_date = new DrupalDateTime();
        $current_date = $current_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
        $availability_start_date = $form_values['field_availability_date'][0]['value']->getPhpDateTime()->getTimestamp();
        $availability_start_date = date('Y-m-d\TH:i:s', $availability_start_date);
        $availability_end_date = $form_values['field_availability_date'][0]['end_value']->getPhpDateTime()->getTimestamp();
        $availability_end_date = date('Y-m-d\TH:i:s', $availability_end_date);
        if ($availability_start_date == $availability_end_date) {
          $form_state->setErrorByName('field_availability_date', t('Availability start date and end date can not be same.'));
        }
        if ($availability_end_date <= $current_date) {
          $form_state->setErrorByName('field_availability_date', t('Availability end date can not be less than current date time.'));
        }
      }
      else {
        $form_state->setErrorByName('field_availability_date', t('Time field cannot be empty in start date and end date.'));
      }
    }
  }
  // Condition to check stock threshold field.
  if (!empty($form_values['field_flap']) && $form_values['field_flap'][0]['value'] == 'stock' && !array_key_exists('field_display_format', $form_values)) {
    if (empty($form_values['field_stock_threshold'][0]['value'])) {
      $form_state->setErrorByName('field_stock_threshold', t('Stock threshold field can not be blank.'));
    }
  }
  // Condition to check title character length.
  if (strlen($form_values['title'][0]['value']) > 150) {
    $form_state->setErrorByName('title', t('Please limit product title to 150 characters'));
  }
  // Condition to check summary character length.
  if (strlen($form_values['field_summary'][0]['value']) > 250) {
    $form_state->setErrorByName('field_summary', t('Please limit your summary to 250 characters'));
  }
  // Check validation for unique sku code.
  if (array_key_exists('variations', $form_values)) {
    $form_storage = $form_state->getStorage();
    $form_storage_embed_entities = $form_storage['inline_entity_form'];
    $sku_code_array = [];
    $variation_id_array = [];
    $sku_count = 0;
    foreach ($form_storage_embed_entities as $key => $value) {
      $variation_entities = $value['entities'];
      foreach ($variation_entities as $variation_entity) {
        if (!empty($variation_entity['entity']->id())) {
          $variation_id_array[] = $variation_entity['entity']->id();
        }
        if (!empty($variation_entity['entity']->sku) && !empty($variation_entity['entity']->sku->value)) {
          if (!in_array($variation_entity['entity']->sku->value, $sku_code_array)) {
            $sku_code_array[] = $variation_entity['entity']->sku->value;
          }
          else {
            $sku_count++;
          }
        }
      }
    }
    if ($sku_count > 0) {
      $form_state->setErrorByName('sku', t('Sku code value should be unique'));
    }
    else {
      if (!empty($sku_code_array)) {
        // Get db connection.
        $connection = Database::getConnection();
        // Select query to get voucher code value.
        $query = $connection->select('commerce_product_variation_field_data', 'c');
        $query->fields('c', ['variation_id']);
        $query->condition('c.sku', $sku_code_array, 'IN');
        if (!empty($variation_id_array)) {
          $query->condition('c.variation_id', $variation_id_array, 'NOT IN');
        }
        $results = $query->execute()->fetchAssoc();
        if (!empty($results)) {
          $form_state->setErrorByName('sku', t('Sku code value should be unique'));
        }
      }
    }
  }
}

/**
 * Callback method for coupon code field validation.
 */
function allianz_custom_coupon_code_field_validation(array $form, FormStateInterface $form_state) {
  $form_values = $form_state->getValues();
  $duplicate_coupon_code = 0;
  $duplicate_coupon_code_arr = [];
  // Used to show duplicate coupon code on error.
  $duplicate_coupon_code_array = [];
  // Condition to check duplicate coupon code.
  if ($form_values['field_display_format'][0]['value'] == 'with_code') {
    // Condition to check stock threshold field.
    if (!empty($form_values['field_flap']) && $form_values['field_flap'][0]['value'] == 'stock') {
      if (empty($form_values['field_stock_threshold'][0]['value'])) {
        $form_state->setErrorByName('field_stock_threshold', t('Stock threshold field can not be blank.'));
      }
    }
    if (!empty($form_values['field_coupon_codes'])) {
      foreach ($form_values['field_coupon_codes'] as $key => $value) {
        if (array_key_exists('subform', $value)) {
          $coupon_code = $value['subform']['field_coupon_code'][0]['value'];
          if (!in_array($coupon_code, $duplicate_coupon_code_arr)) {
            $duplicate_coupon_code_arr[] = $coupon_code;
          }
          else {
            $duplicate_coupon_code++;
            $duplicate_coupon_code_array[] = $coupon_code;
          }
        }
        // Get coupon id from path source.
        $product_source = explode('/', $form_values['path'][0]['source']);
        if (!empty($coupon_code)) {
          // Get db connection.
          $connection = Database::getConnection();
          // Select query to check duplicate coupon code in other coupon.
          $query = $connection->select('paragraph__field_coupon_code', 'cc');
          $query->fields('cc', ['entity_id']);
          $query->innerJoin('commerce_product__field_coupon_codes', 'cp', 'cp.field_coupon_codes_target_id = cc.entity_id');
          $query->condition('cp.bundle', 'coupon', '=');
          if (array_key_exists(2, $product_source)) {
            $coupon_id = $product_source[2];
            $query->condition('cp.entity_id', $coupon_id, '!=');
          }
          $query->condition('cc.field_coupon_code_value', $coupon_code, '=');
          $results = $query->execute()->fetchAssoc();
          if (!empty($results)) {
            $duplicate_coupon_code++;
            $duplicate_coupon_code_array[] = $coupon_code;
          }
        }
        // Display errors for duplicate coupon code.
        if ($duplicate_coupon_code > 0) {
          $form_state->setErrorByName('field_coupon_codes][' . $key . '][subform][field_coupon_code', t('Duplicate coupon codes not allowed.'));
        }
        $duplicate_coupon_code = 0;
      }
      // Check if stock email threshold field is empty.
      if (empty($form_values['field_stock_email_threshold'][0]['value'])) {
        $form_state->setErrorByName('field_stock_email_threshold', t('Email stock threshold field can not be blank.'));
      }
      
      // Check if product coupon code field is empty.
      unset($form_values['field_coupon_codes']['add_more']);
      foreach ($form_values['field_coupon_codes'] as $coupon_key => $value) {
        if (empty($value['subform']['field_coupon_code'][0]['value'])) {
          $form_state->setErrorByName('field_coupon_codes][' . $coupon_key . '][subform][field_coupon_code', t('Coupon code field can not be blank.'));
        }
        if (empty($value['subform']['field_status'])) {
          $form_state->setErrorByName('field_coupon_codes][' . $coupon_key . '][subform][field_status', t('Coupon status field can not be blank.'));
        }
      }
    }
  }
  // Condition to check end product url.
  if ($form_values['field_display_format'][0]['value'] == 'without_code') {
    if (empty($form_values['field_end_product_url'][0]['uri'])) {
      $form_state->setErrorByName('field_end_product_url', t('End product url field can not be blank.'));
    }
    if (!empty($form_values['field_flap'])) {
      if ($form_values['field_flap'][0]['value'] == 'stock') {
       $form_state->setValue('field_flap', [['value'=> '']]);
      }
    }
  }
}

/**
 * Implements hook_page_attachments().
 */
function allianz_custom_page_attachments(array &$page) {
  $page['#attached']['library'][] = 'allianz_custom/allianz_custom_css';
}

/**
 * Implements hook_entity_operation().
 */
function allianz_custom_entity_operation_alter(array &$operations, EntityInterface $entity) {
  if ($entity->bundle() == 'coupon') {
    // Unset varition operation link for coupon on product listing page.
    unset($operations['variations']);
  }
}

/**
 * Implements hook_menu_local_tasks_alter().
 */
function allianz_custom_menu_local_tasks_alter(&$data, $route_name) {
  // Remove variation tab from coupon edit page.
  if ($route_name == 'entity.commerce_product.edit_form') {
    $request = \Drupal::request();
    $current_path = $request->getPathInfo();
    $path_args = explode('/', $current_path);
    $product = Product::load($path_args[2]);
    if (!empty($product) && $product->bundle() == 'coupon') {
      unset($data['tabs'][0]['entity.commerce_product_variation.collection']);
    }
  }
}

/**
 * Callback function to validate user password.
 */
function allianz_custom_user_validation(array &$form, FormStateInterface $form_state) {
  $form_values = $form_state->getValues();
  $password = $form_values['pass'];
  // Password field validation.
  if (!empty($password)) {
    $uppercase = preg_match('@[A-Z]@', $password);
    $lowercase = preg_match('@[a-z]@', $password);
    $number = preg_match('@[0-9]@', $password);
    $special_char = preg_match('@[^\w]@', $password);
    if (!$uppercase || !$lowercase || !$number || !$special_char || strlen($password) < 8) {
      $form_state->setErrorByName('pass', t('Password string must contains at least one uppercase letter, one lower case letter, one number, one special character and should be of at least 8 character length.'));
    }
  }
  // Secret field validation for supplier role.
  if (in_array('supplier', $form_values['roles'])) {
    if (empty($form_values['field_secret_key'][0]['value'])) {
      $form_state->setErrorByName('field_secret_key', t('Secret key field can not be balnk for supplier role.'));
    }
  }
}

/**
 * Implements hook_cron().
 */
function allianz_custom_cron() {
  // Creating a cron queue for voucher stock alert.
  // Get db connection.
  $connection = Database::getConnection();
  $query = $connection->select('stock_alert__field_status', 'sa');
  $query->leftJoin('stock_alert__field_user_id', 'ui', 'ui.entity_id = sa.entity_id');
  $query->leftJoin('stock_alert__field_voucher_id', 'vi', 'vi.entity_id = sa.entity_id');
  $query->fields('sa', ['entity_id']);
  $query->fields('vi', ['field_voucher_id_target_id']);
  $query->fields('ui', ['field_user_id_target_id']);
  $query->condition('sa.field_status_value', 'active', '=');
  $results = $query->execute()->fetchAll();
  if (!empty($results)) {
    /** @var QueueFactory $queue_factory */
    $queue_factory = \Drupal::service('queue');
    /** @var QueueInterface $queue */
    $queue = $queue_factory->get('voucher_stock_alert');
    foreach ($results as $key => $value) {
      $queue->createItem($value);
    }
  }
}

/**
 * Implements hook_views_data().
 */
function allianz_custom_views_data() {
  $data['views']['table']['group'] = t('Custom Global');
  $data['views']['table']['join'] = [
    '#global' => [],
  ];
  $data['views']['voucher_flap_status_field'] = [
    'title' => t('Voucher flap status'),
    'help' => t('Display Voucher flap status.'),
    'field' => [
      'id' => 'voucher_flap_status_field',
    ],
  ];
  $data['views']['voucher_out_of_stock_field'] = [
    'title' => t('Voucher Out of stock'),
    'help' => t('Display Voucher Out of stock.'),
    'field' => [
      'id' => 'voucher_out_of_stock_field',
    ],
  ];
  return $data;
}

function allianz_custom_commerce_product_access(EntityInterface $entity, $operation, AccountInterface $account) {
  if ($operation == 'delete' && $entity->bundle() == 'voucher') {
    return AccessResult::forbidden();
  }
  else {
    return AccessResult::neutral();
  }
}
