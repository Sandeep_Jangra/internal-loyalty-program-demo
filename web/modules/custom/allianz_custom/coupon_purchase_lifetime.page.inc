<?php

/**
 * @file
 * Contains coupon_purchase_lifetime.page.inc.
 *
 * Page callback for Coupon Purchase Lifetime Limit entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Coupon Purchase Lifetime Limit templates.
 *
 * Default template: coupon_purchase_lifetime.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_coupon_purchase_lifetime(array &$variables) {
  // Fetch CouponPurchaseLifetime Entity Object.
  $coupon_purchase_lifetime = $variables['elements']['#coupon_purchase_lifetime'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
