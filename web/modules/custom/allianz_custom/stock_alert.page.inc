<?php

/**
 * @file
 * Contains stock_alert.page.inc.
 *
 * Page callback for Stock alert entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Stock alert templates.
 *
 * Default template: stock_alert.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_stock_alert(array &$variables) {
  // Fetch StockAlert Entity Object.
  $stock_alert = $variables['elements']['#stock_alert'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
