<?php

namespace Drupal\allianz_custom;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Stock alert entity.
 *
 * @see \Drupal\allianz_custom\Entity\StockAlert.
 */
class StockAlertAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\allianz_custom\Entity\StockAlertInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished stock alert entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published stock alert entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit stock alert entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete stock alert entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add stock alert entities');
  }

}
