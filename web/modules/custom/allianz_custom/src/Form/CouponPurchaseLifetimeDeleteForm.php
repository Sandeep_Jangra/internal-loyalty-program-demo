<?php

namespace Drupal\allianz_custom\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Coupon Purchase Lifetime Limit entities.
 *
 * @ingroup allianz_custom
 */
class CouponPurchaseLifetimeDeleteForm extends ContentEntityDeleteForm {


}
