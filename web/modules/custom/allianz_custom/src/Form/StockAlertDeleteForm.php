<?php

namespace Drupal\allianz_custom\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Stock alert entities.
 *
 * @ingroup allianz_custom
 */
class StockAlertDeleteForm extends ContentEntityDeleteForm {


}
