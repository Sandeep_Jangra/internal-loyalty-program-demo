<?php

namespace Drupal\allianz_custom\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Custom access permission for views.
 */
class ViewsCustomAccessSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['extendViewsCustomAccess'];
    return $events;
  }

  /**
   * Perform our logic here.
   *
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   *   An instance of event.
   */
  public function extendViewsCustomAccess(FilterResponseEvent $event) {
    $request = \Drupal::request();
    $view_arr = [
      'coupon_listing',
      'product_queue',
      'voucher_and_coupon_listing',
      'search',
    ];
    if (!empty($request) && in_array($request->attributes->get('view_id'), $view_arr) && $request->attributes->get('display_id') == 'rest_export_1') {
      $authorization_token = trim(str_replace('Bearer ', '', $request->headers->get('Authorization')));
      if (!empty($authorization_token)) {
        // Load jwt transcoder object through services.
        $transcoder = \Drupal::service('jwt.transcoder');
        $token = $transcoder->decode($authorization_token);
        $account_uid = $token->getPayload()->drupal->uid;
        // Get db connection.
        $connection = \Drupal::database();
        // Select query to get requested user token for authentication.
        $query = $connection->select('custom_jwt_token', 'c')
          ->fields('c', ['uid', 'jwt_token'])
          ->condition('c.uid', $account_uid, '=')
          ->execute();
        $results = $query->fetchAssoc();
        if (!empty($results)) {
          if ($results['jwt_token'] != $authorization_token) {
            throw new AccessDeniedHttpException();
          }
        }
      }
    }
  }

}
