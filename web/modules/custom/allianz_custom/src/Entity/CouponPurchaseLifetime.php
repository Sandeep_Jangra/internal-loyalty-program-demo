<?php

namespace Drupal\allianz_custom\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Coupon Purchase Lifetime Limit entity.
 *
 * @ingroup allianz_custom
 *
 * @ContentEntityType(
 *   id = "coupon_purchase_lifetime",
 *   label = @Translation("Coupon Purchase Lifetime Limit"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\allianz_custom\CouponPurchaseLifetimeListBuilder",
 *     "views_data" = "Drupal\allianz_custom\Entity\CouponPurchaseLifetimeViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\allianz_custom\Form\CouponPurchaseLifetimeForm",
 *       "add" = "Drupal\allianz_custom\Form\CouponPurchaseLifetimeForm",
 *       "edit" = "Drupal\allianz_custom\Form\CouponPurchaseLifetimeForm",
 *       "delete" = "Drupal\allianz_custom\Form\CouponPurchaseLifetimeDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\allianz_custom\CouponPurchaseLifetimeHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\allianz_custom\CouponPurchaseLifetimeAccessControlHandler",
 *   },
 *   base_table = "coupon_purchase_lifetime",
 *   translatable = FALSE,
 *   admin_permission = "administer coupon purchase lifetime limit entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/coupon_purchase_lifetime/{coupon_purchase_lifetime}",
 *     "add-form" = "/admin/structure/coupon_purchase_lifetime/add",
 *     "edit-form" = "/admin/structure/coupon_purchase_lifetime/{coupon_purchase_lifetime}/edit",
 *     "delete-form" = "/admin/structure/coupon_purchase_lifetime/{coupon_purchase_lifetime}/delete",
 *     "collection" = "/admin/structure/coupon_purchase_lifetime",
 *   },
 *   field_ui_base_route = "coupon_purchase_lifetime.settings"
 * )
 */
class CouponPurchaseLifetime extends ContentEntityBase implements CouponPurchaseLifetimeInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Coupon Purchase Lifetime Limit entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Coupon Purchase Lifetime Limit entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Coupon Purchase Lifetime Limit is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
