<?php

namespace Drupal\allianz_custom\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Coupon Purchase Lifetime Limit entities.
 *
 * @ingroup allianz_custom
 */
interface CouponPurchaseLifetimeInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Coupon Purchase Lifetime Limit name.
   *
   * @return string
   *   Name of the Coupon Purchase Lifetime Limit.
   */
  public function getName();

  /**
   * Sets the Coupon Purchase Lifetime Limit name.
   *
   * @param string $name
   *   The Coupon Purchase Lifetime Limit name.
   *
   * @return \Drupal\allianz_custom\Entity\CouponPurchaseLifetimeInterface
   *   The called Coupon Purchase Lifetime Limit entity.
   */
  public function setName($name);

  /**
   * Gets the Coupon Purchase Lifetime Limit creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Coupon Purchase Lifetime Limit.
   */
  public function getCreatedTime();

  /**
   * Sets the Coupon Purchase Lifetime Limit creation timestamp.
   *
   * @param int $timestamp
   *   The Coupon Purchase Lifetime Limit creation timestamp.
   *
   * @return \Drupal\allianz_custom\Entity\CouponPurchaseLifetimeInterface
   *   The called Coupon Purchase Lifetime Limit entity.
   */
  public function setCreatedTime($timestamp);

}
