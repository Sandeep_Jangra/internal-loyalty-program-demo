<?php

namespace Drupal\allianz_custom\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Stock alert entities.
 *
 * @ingroup allianz_custom
 */
interface StockAlertInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Stock alert name.
   *
   * @return string
   *   Name of the Stock alert.
   */
  public function getName();

  /**
   * Sets the Stock alert name.
   *
   * @param string $name
   *   The Stock alert name.
   *
   * @return \Drupal\allianz_custom\Entity\StockAlertInterface
   *   The called Stock alert entity.
   */
  public function setName($name);

  /**
   * Gets the Stock alert creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Stock alert.
   */
  public function getCreatedTime();

  /**
   * Sets the Stock alert creation timestamp.
   *
   * @param int $timestamp
   *   The Stock alert creation timestamp.
   *
   * @return \Drupal\allianz_custom\Entity\StockAlertInterface
   *   The called Stock alert entity.
   */
  public function setCreatedTime($timestamp);

}
