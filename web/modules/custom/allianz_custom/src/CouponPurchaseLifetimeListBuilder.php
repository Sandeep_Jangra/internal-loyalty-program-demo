<?php

namespace Drupal\allianz_custom;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Used to build a listing of Coupon Purchase Lifetime Limit entities.
 *
 * @ingroup allianz_custom
 */
class CouponPurchaseLifetimeListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Coupon Purchase Lifetime Limit ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\allianz_custom\Entity\CouponPurchaseLifetime $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.coupon_purchase_lifetime.edit_form',
      ['coupon_purchase_lifetime' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
