<?php

namespace Drupal\allianz_custom\Services;

use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Database\Database;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\user\Entity\User;

/**
 * Class StockFlap used to get varition stock status.
 *
 * @package \Drupal\allianz_custom\Services
 */
class StockFlap {

  /**
   * StockCheckService constructor.
   */
  public function __construct() {

  }

  /**
   * Returns True/False status.
   */
  public function getVoucherStockFlap($product_id) {
    $product = Product::load($product_id);
    $invalid_stock = TRUE;
    $total_variation_stock = 0;
    // Get stock threshold value if stock is selected.
    if (!empty($product->field_flap->value) && $product->field_flap->value == 'stock') {
      if (!empty($product->field_stock_threshold->value)) {
        $stock_threshold = $product->field_stock_threshold->value;
        // Load all variations for the product.
        $variations = $product->getVariations();
        if (!empty($variations)) {
          foreach ($variations as $variation_key => $variation) {
            if (!empty($variation->field_stock->value)) {
              $total_variation_stock = $total_variation_stock + $variation->field_stock->value;
              if ($variation->field_stock->value > $stock_threshold) {
                $invalid_stock = FALSE;
                break;
              }
            }
          }
          // Check total variartion stock with stock threshold.
          if ($total_variation_stock >= $stock_threshold) {
            $invalid_stock = FALSE;
          }
        }
      }
    }
    else {
      $invalid_stock = FALSE;
    }

    return $invalid_stock;
  }

  /**
   * Returns True/False status.
   */
  public function isOutOfStock($product_id) {
    $product = Product::load($product_id);
    $stock = TRUE;
    // Load all variations for the product.
    $variations = $product->getVariations();
    if (!empty($variations)) {
      foreach ($variations as $variation_key => $variation) {
        if (!empty($variation->field_stock->value) && $variation->field_stock->value > 0) {
          $stock = FALSE;
          break;
        }
      }
    }

    return $stock;
  }

  /**
   * Returns True/False status.
   */
  public function existStockAlert($voucher_id, $user_id) {
    // Get db connection.
    $connection = Database::getConnection();
    // Query to check whether entity for this user and variation already exist.
    $query = $connection->select('stock_alert__field_status', 'sa');
    $query->leftJoin('stock_alert__field_user_id', 'ui', 'ui.entity_id = sa.entity_id');
    $query->leftJoin('stock_alert__field_voucher_id', 'vi', 'vi.entity_id = sa.entity_id');
    $query->fields('sa', ['entity_id']);
    $query->condition('ui.field_user_id_target_id', $user_id, '=');
    $query->condition('vi.field_voucher_id_target_id', $voucher_id, '=');
    $query->condition('sa.field_status_value', 'active', '=');
    $results = $query->execute()->fetchAllKeyed(0, 0);
    if (!empty($results)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

    /**
   * Get cart for current user for current product.
   */
  public function getCartStock($variation_id, $account_uid) {
    $storeId = 1;
    $entityManager = \Drupal::entityManager();
    $cartManager = \Drupal::service('commerce_cart.cart_manager');
    $cartProvider = \Drupal::service('commerce_cart.cart_provider');
    $store = \Drupal::entityTypeManager()
      ->getStorage('commerce_store')
      ->load($storeId);
    $account = User::load($account_uid);  
    $cart = $cartProvider->getCart('default', $store, $account);
    $cart_stock = 0;
    if (!empty($cart)) {
      $order_items = $cart->order_items;
      if (!empty($order_items)) {
        // Load all order items.
        foreach ($order_items as $item_key => $item_id) {
          $order_item = OrderItem::load($item_id->target_id);
          $variation = $order_item->getPurchasedEntity();
          if ($variation->id() == $variation_id) {
            $cart_stock = (int) $order_item->get('quantity')->getValue()[0]['value'];
          }
        }
      }
    }
    return $cart_stock;
  }

}
