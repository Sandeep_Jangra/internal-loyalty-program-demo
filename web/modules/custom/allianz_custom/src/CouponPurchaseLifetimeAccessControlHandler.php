<?php

namespace Drupal\allianz_custom;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Coupon Purchase Lifetime Limit entity.
 *
 * @see \Drupal\allianz_custom\Entity\CouponPurchaseLifetime.
 */
class CouponPurchaseLifetimeAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\allianz_custom\Entity\CouponPurchaseLifetimeInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished coupon purchase lifetime limit entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published coupon purchase lifetime limit entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit coupon purchase lifetime limit entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete coupon purchase lifetime limit entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add coupon purchase lifetime limit entities');
  }

}
