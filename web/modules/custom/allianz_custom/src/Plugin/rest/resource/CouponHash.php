<?php

namespace Drupal\allianz_custom\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_product\Entity\Product;
use Drupal\user\Entity\User;
use Drupal\Core\Database\Database;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "coupon_hash",
 *   label = @Translation("Coupon hash"),
 *   uri_paths = {
 *     "create" = "/api/v1/coupon/hash"
 *   }
 * )
 */
class CouponHash extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new CouponHash object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_custom'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param string $payload
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post($payload) {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $jwt = $authorization_token;
    $token = $transcoder->decode($jwt);
    $account_uid = $token->getPayload()->drupal->uid;
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();

    if ((!empty($results) && $results['jwt_token'] == $jwt)) {
      if (!empty($payload) && !empty($payload['coupon_id'])) {
        $coupon_id = $payload['coupon_id'];
        $coupon = Product::load($coupon_id);
        if (!empty($coupon) && $coupon->bundle() == 'coupon') {
          if ($coupon->field_display_format->value == 'without_code') {
            $response['status'] = 'success';
            // Get product supplier id.
            $supplier_id = $coupon->field_supplier_id->target_id;
            $account = User::load($supplier_id);
            $current_date = new DrupalDateTime();
            $current_date = $current_date->format('d.m.Y_H:i:s:v');
            $secret = $account->field_secret_key->value;
            $hash_value = $current_date . $secret;
            $hash = hash('sha256', $hash_value);
            $response['data']['hash'] = strtoupper($hash);
            $response['data']['date'] = $current_date;
            if (!empty($coupon->field_end_product_url) && !empty($coupon->field_end_product_url->uri)) {
              $redirect_url = $coupon->field_end_product_url->uri;
            }
            else {
              $redirect_url = NULL;
            }
            $response['data']['end_product_url'] = $redirect_url;
            $error = 200;
           }
          else {
            // Return 500 here if requested url is from another bundle.
            $response['status'] = 'failure';
            $response['error'] = 'Something went wrong!';
            $error = 500;
          }
        }
        else {
          // Return 500 here if requested url is from another bundle.
          $response['status'] = 'failure';
          $response['error'] = 'Something went wrong!';
          $error = 500;
        }
      }
      else {
        // Return 500 if requested request is invalid.
        $response['status'] = 'failure';
        $response['error'] = 'Something went wrong!';
        $error = 500;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }

    return new ModifiedResourceResponse($response, $error);
  }

}
