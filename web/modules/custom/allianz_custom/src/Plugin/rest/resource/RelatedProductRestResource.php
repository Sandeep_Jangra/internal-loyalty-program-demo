<?php

namespace Drupal\allianz_custom\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\allianz_product\Controller\AllianzField;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Returns coupon related product based on category.
 *
 * @RestResource(
 *   id = "related_product_rest_resource",
 *   label = @Translation("Related coupon product rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/coupon/{coupon_url}/related-product"
 *   }
 * )
 */
class RelatedProductRestResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new RelatedProductRestResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_custom'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
      );
  }

  /**
   * Responds to GET requests.
   *
   * @param string $coupon_url
   *   Get coupon_url on request.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get($coupon_url) {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $jwt = $authorization_token;
    $token = $transcoder->decode($jwt);
    $account_uid = $token->getPayload()->drupal->uid;
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if ((!empty($results) && $results['jwt_token'] == $jwt)) {
      // Getting product id from product url alias.
      $path = \Drupal::service('path.alias_manager')->getPathByAlias('/coupon/' . $coupon_url);
      if (preg_match('/product\/(\d+)/', $path, $matches)) {
        $product_id = $matches[1];
        $response = [];
        $life_time_purchase = $this->lifeTimePurchasedCoupon($account_uid);
        // Load product.
        $product = Product::load($product_id);
        // Checking whether requested product is empty or not.
        if (!empty($product)) {
          try {
            $category_id = $product->field_category->target_id;
            // Get out of stock coupon.
            $out_of_stock_coupon = $this->getOutOfStockCoupon($category_id);
            $related_coupon_ids = array_merge($life_time_purchase, $out_of_stock_coupon);
            if (!in_array($product_id, $related_coupon_ids)) {
              $related_coupon_ids[] = $product_id;
            }
            $query = \Drupal::entityQuery('commerce_product');
            $query->condition('status', 1);
            $query->condition('type', 'coupon');
            $query->condition('product_id', $related_coupon_ids, 'NOT IN');
            $query->condition('field_category', $category_id);
            $query->accessCheck(FALSE);
            $query->range(0, 4);
            $coupon_ids = $query->execute();
            if (!empty($coupon_ids)) {
              $response['status'] = 'success';
              $limit_count = 0;
              foreach ($coupon_ids as $key => $coupon_id) {
                // Create AllianzField class object.
                $allianz_field = new AllianzField();
                // Load voucher product.
                $coupon = Product::load($coupon_id);
                // Get current time.
                $current_date = new DrupalDateTime();
                $current_date = $current_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
                $flap = $coupon->field_flap->value;
                $availibility_to = NULL;
                if ($flap == 'availability') {
                  $end_date = new DrupalDateTime($coupon->field_availability_date->end_value, 'UTC');
                  $end_date->setTimezone(timezone_open(date_default_timezone_get()));
                  $availibility_to = $end_date->format('Y-m-d\TH:i:s');
                }
                if(($flap == 'availability' && $availibility_to > $current_date)  || $flap != 'availability') {
                  $flap_status = FALSE;
                  if ($coupon->field_flap->value == 'stock') {
                    $stock = $coupon->field_stock->value;
                    $stock_threshold = $coupon->field_stock_threshold->value;
                    if (!empty($stock_threshold) && !empty($stock) && $stock < $stock_threshold) {
                      $flap_status = TRUE;
                    }
                  }
                  if ($coupon->field_display_format->value == 'without_code' || (!empty($coupon->field_stock->value) && $coupon->field_stock->value > 0)) {
                    $out_of_stock = FALSE;
                  }
                  else {
                    $out_of_stock = TRUE;
                  }
                  $product_url = \Drupal::service('path.alias_manager')->getAliasByPath('/product/' . $coupon_id);
                  $response['data'][$limit_count]['coupon_id'] = $coupon_id;
                  $response['data'][$limit_count]['title'] = ucfirst($coupon->getTitle());
                  $response['data'][$limit_count]['summary'] = ucfirst($coupon->field_summary->value);
                  $response['data'][$limit_count]['discount'] = (int) $coupon->field_coupon_value->value;
                  $response['data'][$limit_count]['display_format'] = $coupon->field_display_format->value;
                  $response['data'][$limit_count]['coupon_url'] = $product_url;
                  $coupon_image = [];
                  if (!empty($coupon->field_coupon_image->getValue())) {
                    $coupon_image_value = $coupon->field_coupon_image->getValue();
                    $coupon_image = $allianz_field->getImageStyleUrl($coupon_image_value[0]['target_id'], 'related');
                    $coupon_image['alt'] = $coupon_image_value[0]['alt'];
                    $coupon_image['title'] = $coupon_image_value[0]['title'];
                  }
                  $response['data'][$limit_count]['coupon_image'] = $coupon_image;
                  $response['data'][$limit_count]['flap_details']['flap'] = $coupon->field_flap->value;
                  $response['data'][$limit_count]['flap_details']['availability_date_to'] = $availibility_to;
                  $response['data'][$limit_count]['flap_details']['flap_status'] = $flap_status;
                  $response['data'][$limit_count]['flap_details']['out_of_stock'] = $out_of_stock;
                  if ($coupon->field_display_format->value == 'with_code') {
                    $response['data'][$limit_count]['flap_details']['stock'] = $coupon->field_stock->value;
                  }
                  $limit_count++;
                }
              }
              $error = 200;
            }
            else {
              // Return 404 if related product does not exists.
              $response['status'] = 'failure';
              $response['error'] = 'Related product does not exist.';
              $error = 404;
            }
          }
          catch (RequestException $e) {
            $response['status'] = 'failure';
            $response['error'] = 'Something went wrong!';
            $error = 500;
          }
        }
      }
      else {
        // Return 404 if requested product does not exists.
        $response['status'] = 'failure';
        $response['error'] = 'Related product does not exist.';
        $error = 404;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }
    $response = new ResourceResponse($response, $error);
    // Disable api cache.
    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);
    $response->addCacheableDependency($disable_cache);

    return $response;
  }

  /**
   * Function used to get out of stock coupon product.
   */
  public function getOutOfStockCoupon($category_id) {
    $oos_coupon_id = [];
    $query = \Drupal::entityQuery('commerce_product');
    $query->condition('status', 1);
    $query->condition('type', 'coupon');
    $query->condition('field_stock', 0, '=');
    $query->condition('field_display_format', 'with_code', '=');
    $query->condition('field_category', $category_id);
    $query->accessCheck(FALSE);
    $coupon_ids = $query->execute();
    if (!empty($coupon_ids)) {
      foreach ($coupon_ids as $key => $value) {
        if (!in_array($value, $oos_coupon_id)) {
          $oos_coupon_id[] = $value;
        }
      }
    }
    return $oos_coupon_id;
  }

  /**
   * Function used to get limited purchased coupon product.
   */
  public function lifeTimePurchasedCoupon($account_uid) {
    $coupon_ids = [];
    // Get db connection.
    $connection = Database::getConnection();
    $query = $connection->select('coupon_purchase_lifetime__field_quantity', 'q');
    $query->leftJoin('coupon_purchase_lifetime__field_coupon_id', 'ci', 'ci.entity_id = q.entity_id');
    $query->leftJoin('coupon_purchase_lifetime__field_user_id', 'ui', 'ui.entity_id = q.entity_id');
    $query->fields('ci', ['field_coupon_id_target_id']);
    $query->condition('ui.field_user_id_target_id', $account_uid, '=');
    $query->addExpression('COUNT(ci.field_coupon_id_target_id)', 'count');
    $query->groupBy('ci.field_coupon_id_target_id');
    $limit_results = $query->execute()->fetchAll();
    if (!empty($limit_results)) {
      foreach ($limit_results as $key => $value) {
        $coupon = Product::load($value->field_coupon_id_target_id);
        $purchase_limit = $coupon->field_purchase_limit->value;
        if (!empty($purchase_limit) && $purchase_limit <= $value->count) {
          $coupon_ids[] = $value->field_coupon_id_target_id;
        }
      }
    }
    return $coupon_ids;
  }

}
