<?php

namespace Drupal\allianz_custom\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\allianz_product\Controller\AllianzField;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\Component\Utility\UrlHelper;

/**
 * CustomBlockRestResource custom rest get api that return custom block detail.
 *
 * @RestResource(
 *   id = "custom_block_rest_resource",
 *   label = @Translation("Custom Block Rest Resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/custom/block/{block_id}"
 *   }
 * )
 */
class CustomBlockRestResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new CustomBlockRestResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_custom'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
      );
  }

  /**
   * Responds to GET requests.
   *
   * @param string $block_id
   *   Get block_id on request.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get($block_id) {
    // Get jwt token from request header.
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $token = $transcoder->decode($authorization_token);
    $account_uid = $token->getPayload()->drupal->uid;
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if ((!empty($results) && $results['jwt_token'] == $authorization_token)) {
      // Load custom block using block id.
      $custom_block = \Drupal::entityTypeManager()->getStorage('block_content')->load($block_id);
      $response = [];
      // Checking whether block is empty or not.
      if (!empty($custom_block)) {
        try {
          $body = '';
          $response['status'] = 'success';
          $response['data']['id'] = $block_id;
          $response['data']['type'] = $custom_block->bundle();
          $response['data']['title'] = $custom_block->info->value;
          $body_value = $custom_block->body->value;

          // Check if custom block type is overlay.
          if ($custom_block->bundle() == 'overlay') {
            if (!empty($body_value)) {
              $body = strlen($body_value) > 160 ? substr($body_value, 0, 160) . "..." : $body_value;
            }
            if (!empty($custom_block->field_image)) {
              // Create AllianzField class object.
              $allianz_field = new AllianzField();
              $file_id = $custom_block->field_image->target_id;
              // getImageUrl Used to get image url with thumbnail.
              $response['data']['image'] = $allianz_field->getImageUrl($file_id, 'overlay', 'overlay');
              $response['data']['image']['alt'] = $custom_block->field_image->alt;
              $response['data']['image']['title'] = $custom_block->field_image->title;
            }
            if (!empty($custom_block->field_cta_link)) {
              $uri_external = UrlHelper::isExternal($custom_block->field_cta_link->uri);
              $uriString = $custom_block->field_cta_link->uri;
              if ($uri_external) {
                $cta_link_url = $uriString;
              }
              else {
                $urlObj = Url::fromUri($uriString);
                $cta_link_url = \Drupal::service('path.alias_manager')->getAliasByPath('/' . $urlObj->getInternalPath());
              }
              $response['data']['cta_link']['url'] = $cta_link_url;
              $response['data']['cta_link']['title'] = $custom_block->field_cta_link->title;
            }
            $response['data']['visibility_status'] = $custom_block->field_visibility_status->value;
          }
          else {
            if (!empty($body_value)) {
              $body = $body_value;
            }
          }
          $response['data']['body'] = $body;
          $error = 200;
        }
        catch (RequestException $e) {
          $response['status'] = 'failure';
          $response['error'] = 'Something went wrong!';
          $error = 500;
        }
      }
      else {
        // Return 404 if requested block does not exists.
        $response['status'] = 'failure';
        $response['error'] = 'Requested block does not exist.';
        $error = 404;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }
    $response = new ResourceResponse($response, $error);
    // Disable api cache.
    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);
    $response->addCacheableDependency($disable_cache);

    return $response;
  }

}
