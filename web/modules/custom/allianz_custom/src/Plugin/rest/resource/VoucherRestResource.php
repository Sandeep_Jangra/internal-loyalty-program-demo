<?php

namespace Drupal\allianz_custom\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_product\Entity\Product;
use Drupal\allianz_product\Controller\AllianzField;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * VoucherRestResource custom rest get api that return voucher product detail.
 *
 * @RestResource(
 *   id = "voucher_rest_resource",
 *   label = @Translation("Voucher rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/voucher/{voucher_title}"
 *   }
 * )
 */
class VoucherRestResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new VoucherRestResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_custom'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
      );
  }

  /**
   * Responds to GET requests.
   *
   * @param string $voucher_title
   *   Get voucher_title on request.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get($voucher_title) {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $jwt = $authorization_token;
    $token = $transcoder->decode($jwt);
    $account_uid = $token->getPayload()->drupal->uid;
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if ((!empty($results) && $results['jwt_token'] == $jwt)) {
      // Geting product id from product url alias.
      $path = \Drupal::service('path.alias_manager')->getPathByAlias('/voucher/' . $voucher_title);
      if (preg_match('/product\/(\d+)/', $path, $matches)) {
        $product_id = $matches[1];
        // Load product.
        $product = Product::load($product_id);
        // Checking if product bundle is voucher.
        if ($product->bundle() == 'voucher') {
          global $base_url;
          $response = [];
          // Create AllianzField class object.
          $allianz_field = new AllianzField();
          if (!empty($product) && $product->isPublished() == 1) {
            $voucher_session_service = \Drupal::service('allianz_voucher_session.session_level_checkout');
            $session_level_limit = $voucher_session_service->getCartListSessionLimit($account_uid, $product);
            if ($session_level_limit == "not_exist" && $session_level_limit != "0") {
              $session_level_limit = 1;
            }
            // Get current time.
            $current_date = new DrupalDateTime();
            $current_date = $current_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
            $flap = $product->field_flap->value;
            $availibility_to = NULL;
            if ($flap == 'availability') {
              $end_date = new DrupalDateTime($product->field_availability_date->end_value, 'UTC');
              $end_date->setTimezone(timezone_open(date_default_timezone_get()));
              $availibility_to = $end_date->format('Y-m-d\TH:i:s');
            }
            if ($session_level_limit > 0 && (($flap == 'availability' && $availibility_to > $current_date)  || $flap != 'availability')) {
              try {
                $response['status'] = 'success';
                // Stock flap service to get stock flap status.
                $stock_flap_service = \Drupal::service('allianz_custom.stock_check');
                $stock_flap = $stock_flap_service->getVoucherStockFlap($product_id);
                $stock_flap_status = "false";
                if (!empty($stock_flap)) {
                  $stock_flap_status = "true";
                }
                $response['data']['voucher_id'] = $product_id;
                $response['data']['type'] = $product->bundle();
                $response['data']['title'] = $product->getTitle();
                $product_descriptions = $product->field_description->getValue();
                if (!empty($product_descriptions)) {
                  foreach ($product_descriptions as $key => $paragraph_id) {
                    $product_description = $allianz_field->getParagraphData($paragraph_id['target_id']);
                    $product_description = str_replace('src="', 'src="' . $base_url, $product_description);
                    $response['data']['description'][] = $product_description;
                  }
                }
                $response['data']['summary'] = $product->field_summary->value;
                $response['data']['flap'] = $product->field_flap->value;
                if ($product->field_flap->value == 'availability') {
                  $start_date = new DrupalDateTime($product->field_availability_date->value, 'UTC');
                  $start_date->setTimezone(timezone_open(date_default_timezone_get()));
                  $response['data']['availability_date_from'] = $start_date->format('Y-m-d\TH:i:s');
                  $response['data']['availability_date_to'] = $availibility_to;
                }
                $response['data']['flap_status'] = $stock_flap_status;
                // Get voucher session stock limit.
                $voucher_session_service = \Drupal::service('allianz_voucher_session.session_level_checkout');
                $session_level_limit = $voucher_session_service->getSessionCheckoutLimit($account_uid, $product);
                $response['data']['session_level_limit'] = $session_level_limit;
                $voucher_image = [];
                if (!empty($product->field_voucher_image->getValue())) {
                  foreach ($product->field_voucher_image->getValue() as $voucher_image_key => $voucher_image_value) {
                    $voucher_image[$voucher_image_key] = $allianz_field->getImageUrl($voucher_image_value['target_id'], 'voucher', 'thumbnail_64');
                    $voucher_image[$voucher_image_key]['alt'] = $voucher_image_value['alt'];
                    $voucher_image[$voucher_image_key]['title'] = $voucher_image_value['title'];
                  }
                }
                $response['data']['voucher_image'] = $voucher_image;
                $response['data']['discount'] = $product->field_discount->value;
                // Checkout out of stock variation.
                $out_of_stock = $stock_flap_service->isOutOfStock($product_id);
                $out_of_stock_status = "false";
                if (!empty($out_of_stock)) {
                  $out_of_stock_status = "true";
                }
                $response['data']['out_of_stock'] = $out_of_stock_status;
                // Checking if Stock Alert Entity for user and variation.
                // Already exist.
                $stock_alert_service = \Drupal::service('allianz_custom.stock_check');
                if ($stock_alert_service->existStockAlert($product_id, $account_uid)) {
                  $response['data']['stock_alert_exist'] = "true";
                }
                else {
                  $response['data']['stock_alert_exist'] = "false";
                }
                if (!empty($product->getVariations())) {
                  $variation_key_count = 0;
                  foreach ($product->getVariations() as $variation_key => $variation_value) {
                    $stock = $variation_value->field_stock->value;
                    $total_stock = $stock;
                    if ($stock > 0) {
                      $cart_stock = $stock_alert_service->getCartStock($variation_value->id(), $account_uid);
                      if ($cart_stock > 0) {
                        $stock = $stock - $cart_stock;
                      }
                      $response['data']['variation'][$variation_key_count]['variation_id'] = $variation_value->id();
                      $response['data']['variation'][$variation_key_count]['stock'] = $stock;
                      $response['data']['variation'][$variation_key_count]['total_stock'] = $total_stock;
                      $actual_price = round($variation_value->price->getValue()[0]['number'], 2);
                      $currency_code = $variation_value->getPrice()->getCurrencyCode();
                      $discount_percentage = $product->field_discount->value;
                      // Calculating discount price.
                      if (!empty($variation_value->price)) {
                        $discount_price = round(($actual_price * $discount_percentage) / 100, 2);
                        $discount = round($actual_price - $discount_price, 2);
                        $response['data']['variation'][$variation_key_count]['price']['actual_price'] = $actual_price;
                        $response['data']['variation'][$variation_key_count]['price']['purchase_price'] = $discount;
                        $response['data']['variation'][$variation_key_count]['price']['discount'] = $discount_price;
                        $response['data']['variation'][$variation_key_count]['price']['currency'] = $currency_code;
                      }
                      $variation_key_count++;
                    }
                  }
                }
                $error = 200;
              }
              catch (RequestException $e) {
                $response['status'] = 'failure';
                $response['error'] = 'Something went wrong!';
                $error = 500;
              }
            }
            else {
              // Return 404 if requested product is unpublished.
              $response['status'] = 'failure';
              $response['error'] = 'Something went wrong.';
              $error = 500;
            }
          }
          else {
            // Return 404 if requested product is unpublished.
            $response['status'] = 'failure';
            $response['error'] = 'Requested product is unpublished.';
            $error = 500;
          }
        }
        else {
          // Return 500 if product bundle is from another bundle.
          $response['status'] = 'failure';
          $response['error'] = 'Something went wrong!';
          $error = 500;
        }
      }
      else {
        // Return 404 if requested url does not exists.
        $response['status'] = 'failure';
        $response['error'] = 'Requested product does not exist.';
        $error = 404;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }
    $response = new ResourceResponse($response, $error);
    // Disable api cache.
    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);
    $response->addCacheableDependency($disable_cache);

    return $response;
  }

}
