<?php

namespace Drupal\allianz_custom\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_product\Entity\Product;
use Drupal\allianz_product\Controller\AllianzField;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\user\Entity\User;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * CouponRestResource custom rest get api that return coupon product detail.
 *
 * @RestResource(
 *   id = "coupon_rest_resource",
 *   label = @Translation("Coupon Rest Resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/coupon/{coupon_title}"
 *   }
 * )
 */
class CouponRestResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new CouponRestResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_custom'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
      );
  }

  /**
   * Responds to GET requests.
   *
   * @param string $coupon_title
   *   Get coupon_title on request.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get($coupon_title) {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $jwt = $authorization_token;
    $token = $transcoder->decode($jwt);
    $account_uid = $token->getPayload()->drupal->uid;
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if ((!empty($results) && $results['jwt_token'] == $jwt)) {
      // Getting coupon product id based on coupon product url.
      $path = \Drupal::service('path.alias_manager')->getPathByAlias('/coupon/' . $coupon_title);
      if (preg_match('/product\/(\d+)/', $path, $matches)) {
        $product_id = $matches[1];
        // Load the product.
        $product = Product::load($product_id);
        // Checking if product bundle is coupon.
        if ($product->bundle() == 'coupon') {
          global $base_url;
          $display_format = $product->field_display_format->value;
          $purchase_limit = $product->field_purchase_limit->value;
          $stock = $product->field_stock->value;
          // Get current time.
          $current_date = new DrupalDateTime();
          $current_date = $current_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
          $flap = $product->field_flap->value;
          $availibility_to = NULL;
          if ($flap == 'availability') {
            $end_date = new DrupalDateTime($product->field_availability_date->end_value, 'UTC');
            $end_date->setTimezone(timezone_open(date_default_timezone_get()));
            $availibility_to = $end_date->format('Y-m-d\TH:i:s');
          }
          $query = $connection->select('coupon_purchase_lifetime__field_quantity', 'q');
          $query->leftJoin('coupon_purchase_lifetime__field_coupon_id', 'ci', 'ci.entity_id = q.entity_id');
          $query->leftJoin('coupon_purchase_lifetime__field_user_id', 'ui', 'ui.entity_id = q.entity_id');
          $query->fields('q', ['field_quantity_value']);
          $query->condition('ui.field_user_id_target_id', $account_uid, '=');
          $query->condition('ci.field_coupon_id_target_id', $product_id, '=');
          $limit_results = $query->execute()->fetchAll();
          if ($purchase_limit > count($limit_results) && ($stock > 0 || $display_format == 'without_code') && (($flap == 'availability' && $availibility_to > $current_date)  || $flap != 'availability')) {
            $response = [];
            // Create AllianzField class object.
            $allianz_field = new AllianzField();
            // Checking whether coupon product is empty or not.
            if (!empty($product) && $product->isPublished() == 1) {
              try {
                $response['status'] = 'success';
                $error = 200;
                $response['data']['coupon_id'] = (int) $product_id;
                $response['data']['type'] = $product->bundle();
                $response['data']['title'] = $product->getTitle();
                $product_descriptions = $product->field_description->getValue();
                if (!empty($product_descriptions)) {
                  foreach ($product_descriptions as $key => $paragraph_id) {
                    $product_description = $allianz_field->getParagraphData($paragraph_id['target_id']);
                    $product_description = str_replace('src="', 'src="' . $base_url, $product_description);
                    $response['data']['description'][] = $product_description;
                  }
                }
                $response['data']['summary'] = $product->field_summary->value;
                if ($product->field_flap->value == 'availability') {
                  $end_date = new DrupalDateTime($product->field_availability_date->end_value, 'UTC');
                  $end_date->setTimezone(timezone_open(date_default_timezone_get()));
                  $availability_date_to = $end_date->format('Y-m-d\TH:i:s');
                }
                else {
                  $availability_date_to = NULL;
                }
                $flap_status = FALSE;
                if ($product->field_flap->value == 'stock') {
                  $stock_threshold = $product->field_stock_threshold->value;
                  if (!empty($stock_threshold) && !empty($stock) && $stock < $stock_threshold) {
                    $flap_status = TRUE;
                  }
                }
                if ($product->field_display_format->value == 'without_code' || (!empty($product->field_stock->value) && $product->field_stock->value > 0)) {
                  $out_of_stock = FALSE;
                }
                else {
                  $out_of_stock = TRUE;
                }
                $response['data']['display_format'] = $product->field_display_format->value;
                $response['data']['discount'] = (int) $product->field_coupon_value->value;
                $attachment_url = NULL;
                if (!empty($product->field_attachment) && !empty($product->field_attachment->target_id)) {
                  $attachment_id = $product->field_attachment->target_id;
                  $attachment_url = $allianz_field->getFileUrl($attachment_id);
                }
                $response['data']['attachment'] = $attachment_url;
                $coupon_image = [];
                if (!empty($product->field_coupon_image->getValue())) {
                  foreach ($product->field_coupon_image->getValue() as $coupon_image_key => $coupon_image_value) {
                    $coupon_image[$coupon_image_key] = $allianz_field->getImageUrl($coupon_image_value['target_id'], 'coupon', 'thumbnail_64');
                    $voucher_image[$coupon_image_key]['alt'] = $coupon_image_value['alt'];
                    $voucher_image[$coupon_image_key]['title'] = $coupon_image_value['title'];
                  }
                }
                $response['data']['coupon_image'] = $coupon_image;
                $response['data']['flap_details']['flap'] = $product->field_flap->value;
                $response['data']['flap_details']['availability_date_to'] = $availability_date_to;
                $response['data']['flap_details']['flap_status'] = $flap_status;
                $response['data']['flap_details']['out_of_stock'] = $out_of_stock;
                if ($product->field_display_format->value == 'with_code') {
                  $response['data']['flap_details']['stock'] = $product->field_stock->value;
                  if (!empty($product->field_end_product_url) && !empty($product->field_end_product_url->uri)) {
                    $response['data']['end_product_url'] = $product->field_end_product_url->uri;
                  }
                }
              }
              catch (RequestException $e) {
                $response['status'] = 'failure';
                $response['error'] = 'Something went wrong!';
                $error = 500;
              }
            }
            else {
              // Return 500 if requested product is unpublished.
              $response['status'] = 'failure';
              $response['error'] = 'Requested product is unpublished.';
              $error = 500;
            }
          }
          else {
            // Return 404 here if purchased limit exceeded.
            $response['status'] = 'failure';
            $response['error'] = 'Requested product does not exist.';
            $error = 404;
          }
        }
        else {
          // Return 500 here if requested url is from another bundle.
          $response['status'] = 'failure';
          $response['error'] = 'Something went wrong!';
          $error = 500;
        }
      }
      else {
        // Return 404 if url does not exists.
        $response['status'] = 'failure';
        $response['error'] = 'Requested product does not exist.';
        $error = 404;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }
    $response = new ResourceResponse($response, $error);
    // Disable api cache.
    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);
    $response->addCacheableDependency($disable_cache);

    return $response;
  }

}
