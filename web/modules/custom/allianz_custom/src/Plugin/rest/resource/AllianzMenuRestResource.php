<?php

namespace Drupal\allianz_custom\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;

/**
 * AllianzMenuRestResource custom rest get api that footer menu.
 *
 * @RestResource(
 *   id = "allianz_menu_rest_resource",
 *   label = @Translation("Allianz menu rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/menu/{menu_name}"
 *   }
 * )
 */
class AllianzMenuRestResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new AllianzMenuRestResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
      array $configuration,
      $plugin_id,
      $plugin_definition,
      array $serializer_formats,
      LoggerInterface $logger,
      AccountProxyInterface $current_user,
      Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->getParameter('serializer.formats'),
          $container->get('logger.factory')->get('allianz_custom'),
          $container->get('current_user'),
          $container->get('request_stack')->getCurrentRequest()
      );
  }

  /**
   * Responds to GET requests.
   *
   * @param string $menu_name
   *   Get menu_name on request.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get($menu_name) {
    // Get jwt token from request header.
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $token = $transcoder->decode($authorization_token);
    $account_uid = $token->getPayload()->drupal->uid;
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if ((!empty($results) && $results['jwt_token'] == $authorization_token)) {
      $menu_tree = \Drupal::menuTree();
      $parameters = $menu_tree->getCurrentRouteMenuTreeParameters($menu_name);
      // Load menu tree.
      $tree = $menu_tree->load($menu_name, $parameters);
      $response = [];
      // Checking whether menu tree is empty or not.
      if (!empty($tree)) {
        try {
          $menu_links = [];
          $manipulators = [
            ['callable' => 'menu.default_tree_manipulators:checkAccess'],
            ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
          ];
          $tree = $menu_tree->transform($tree, $manipulators);
          $menu_data = $menu_tree->build($tree);
          foreach ($menu_data['#items'] as $item) {
            if ($item['url']->getRouteName() != '') {
              // Getiing url alias based on item internal path.
              $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/' . $item['url']->getInternalPath());
              $menu_links[] = ['menu_title' => $item['title'], 'menu_url' => $alias];
            }
          }
          $response['status'] = 'success';
          $response['data']['menu_link'] = $menu_links;
          $error = 200;
        }
        catch (RequestException $e) {
          $response['status'] = 'failure';
          $response['error'] = 'Something went wrong!';
          $error = 500;
        }
      }
      else {
        // Return 404 if requested menu does not exists.
        $response['status'] = 'failure';
        $response['error'] = 'Requested menu type does not exits.';
        $error = 404;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }
    $response = new ResourceResponse($response, $error);
    // Disable api cache.
    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);
    $response->addCacheableDependency($disable_cache);

    return $response;
  }

}
