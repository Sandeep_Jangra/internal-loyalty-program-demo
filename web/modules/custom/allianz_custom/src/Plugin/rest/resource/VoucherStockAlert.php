<?php

namespace Drupal\allianz_custom\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\Core\Database\Database;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\allianz_custom\Entity\StockAlert;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "voucher_stock_alert",
 *   label = @Translation("Voucher Stock Alert"),
 *   uri_paths = {
 *     "create" = "/api/v1/stock/alert"
 *   }
 * )
 */
class VoucherStockAlert extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new VoucherStockAlert object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
        $configuration,
        $plugin_id,
        $plugin_definition,
        $container->getParameter('serializer.formats'),
        $container->get('logger.factory')->get('allianz_custom'),
        $container->get('current_user'),
        $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param string $payload
   *   Get payload object on request.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post($payload) {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $jwt = $authorization_token;
    $token = $transcoder->decode($jwt);
    $account_uid = $token->getPayload()->drupal->uid;
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();

    if ((!empty($results) && $results['jwt_token'] == $jwt)) {
      if (!empty($payload) && !empty($payload['voucher_id'])) {
        $voucher_id = $payload['voucher_id'];
        // Check if Stock Alert Entity already exists for user and variation.
        $query = $connection->select('stock_alert__field_status', 'sa');
        $query->leftJoin('stock_alert__field_user_id', 'ui', 'ui.entity_id = sa.entity_id');
        $query->leftJoin('stock_alert__field_voucher_id', 'vi', 'vi.entity_id = sa.entity_id');
        $query->fields('sa', ['entity_id']);
        $query->condition('ui.field_user_id_target_id', $account_uid, '=');
        $query->condition('vi.field_voucher_id_target_id', $voucher_id, '=');
        $query->condition('sa.field_status_value', 'active', '=');
        $results = $query->execute()->fetchAssoc();
        if ($payload['subscribe']) {
          if (empty($results)) {
            try {
              $stock_alert = StockAlert::create([
                'name' => 'Stock Alert -' . REQUEST_TIME,
                'user_id' => 1,
                'created' => REQUEST_TIME,
                'changed' => REQUEST_TIME,
                'status' => TRUE,
                'field_user_id' => [
                  'target_id' => $account_uid,
                ],
                'field_voucher_id' => [
                  'target_id' => $voucher_id,
                ],
                'field_status' => 'active',
              ]);
              $stock_alert->save();
              $response['status'] = 'success';
              $error = 200;
            }
            catch (RequestException $e) {
              $response['status'] = 'failure';
              $response['error'] = 'Something went wrong!';
              $error = 500;
            }
          }
          else {
            // Return 500 if requested request is invalid.
            $response['status'] = 'failure';
            $response['error'] = 'User has already subscribed for this variation';
            $error = 400;
          }
        }
        else {
          if (!empty($results)) {
            $entity_id = $results['entity_id'];
            try {
              $stock_alert = StockAlert::load($entity_id);
              $stock_alert->delete();
              $response['status'] = 'success';
              $error = 200;
            }
            catch (Exception $e) {
              $response['status'] = 'failure';
              $response['error'] = 'Something went wrong!';
              $error = 500;
            }
          }
          else {
            // Return 500 if requested request is invalid.
            $response['status'] = 'failure';
            $response['error'] = 'User has not subscribed for this variation';
            $error = 400;
          }
        }
      }
      else {
        // Return 500 if requested request is invalid.
        $response['status'] = 'failure';
        $response['error'] = 'Something went wrong!';
        $error = 500;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }

    return new ModifiedResourceResponse($response, $error);
  }

}
