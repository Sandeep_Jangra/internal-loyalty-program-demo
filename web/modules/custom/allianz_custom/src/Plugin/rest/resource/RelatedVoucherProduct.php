<?php

namespace Drupal\allianz_custom\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\allianz_product\Controller\AllianzField;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Returns voucher related product based on category.
 *
 * @RestResource(
 *   id = "related_voucher_product",
 *   label = @Translation("Related Voucher Product"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/voucher/{voucher_url}/related-product"
 *   }
 * )
 */
class RelatedVoucherProduct extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new RelatedVoucherProduct object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_custom'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
      );
  }

  /**
   * Responds to GET requests.
   *
   * @param string $voucher_url
   *   Get voucher_url on request.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get($voucher_url) {
    // Get jwt token from request header.
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $token = $transcoder->decode($authorization_token);
    $account_uid = $token->getPayload()->drupal->uid;
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if ((!empty($results) && $results['jwt_token'] == $authorization_token)) {
      // Getting product id from product url alias.
      $path = \Drupal::service('path.alias_manager')->getPathByAlias('/voucher/' . $voucher_url);
      if (preg_match('/product\/(\d+)/', $path, $matches)) {
        $product_id = $matches[1];
        $response = [];
        // Load product.
        $product = Product::load($product_id);
        // Checking whether requested product is empty or not.
        if (!empty($product)) {
          try {
            $query = \Drupal::entityQuery('commerce_product');
            $query->condition('status', 1);
            $query->condition('type', 'voucher');
            $query->condition('product_id', $product_id, '!=');
            if (!empty($product->field_category)) {
              $query->condition('field_category', $product->field_category->target_id);
            }
            $query->accessCheck(FALSE);
            $voucher_ids = $query->execute();
            if (!empty($voucher_ids)) {
              $response['status'] = 'success';
              $limit_count = 0;
              foreach ($voucher_ids as $key => $voucher_id) {
                // Create AllianzField class object.
                $allianz_field = new AllianzField();
                // Load voucher product.
                $voucher = Product::load($voucher_id);
                $voucher_session_service = \Drupal::service('allianz_voucher_session.session_level_checkout');
                $session_level_limit = $voucher_session_service->getCartListSessionLimit($account_uid, $voucher);
                if ($session_level_limit == "not_exist" && $session_level_limit != "0") {
                  $session_level_limit = 1;
                }
                // Get current time.
                $current_date = new DrupalDateTime();
                $current_date = $current_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
                $flap = $voucher->field_flap->value;
                $availibility_to = NULL;
                if ($flap == 'availability') {
                  $end_date = new DrupalDateTime($voucher->field_availability_date->end_value, 'UTC');
                  $end_date->setTimezone(timezone_open(date_default_timezone_get()));
                  $availibility_to = $end_date->format('Y-m-d\TH:i:s');
                }
                if ($session_level_limit > 0 && (($flap == 'availability' && $availibility_to > $current_date)  || $flap != 'availability')) {
                  $product_url = \Drupal::service('path.alias_manager')->getAliasByPath('/product/' . $voucher_id);
                  $variation_ids = $voucher->getVariationIds();
                  // Checking whether voucher have variations or not.
                  if (!empty($variation_ids)) {
                    // Get total voucher stock.
                    $voucher_stock = 0;
                    $variations = $voucher->getVariations();
                    if (!empty($variations)) {
                      foreach ($variations as $key => $variation) {
                        $voucher_stock = $voucher_stock + $variation->field_stock->value;
                      }
                    }
                    // Getting first variation id.
                    $variation_id = reset($variation_ids);
                    // Load voucher variation.
                    $voucher_variation = ProductVariation::load($variation_id);
                    $response['data'][$limit_count]['product_id'] = $voucher_id;
                    $response['data'][$limit_count]['title'] = ucfirst($voucher->getTitle());
                    $response['data'][$limit_count]['summary'] = ucfirst($voucher->field_summary->value);
                    $response['data'][$limit_count]['flap'] = $voucher->field_flap->value;
                    if ($voucher->field_flap->value == 'availability') {
                      $start_date = new DrupalDateTime($voucher->field_availability_date->value, 'UTC');
                      $start_date->setTimezone(timezone_open(date_default_timezone_get()));
                      $response['data'][$limit_count]['availability_date_from'] = strtotime($start_date->format('Y-m-d\TH:i:s'));
                      $response['data'][$limit_count]['availability_date_to'] = strtotime($availibility_to);
                    }
                    $voucher_image = [];
                    if (!empty($voucher->field_voucher_image->getValue())) {
                      $voucher_image_value = $voucher->field_voucher_image->getValue();
                      $voucher_image = $allianz_field->getImageStyleUrl($voucher_image_value[0]['target_id'], 'related');
                      $voucher_image['alt'] = $voucher_image_value[0]['alt'];
                      $voucher_image['title'] = $voucher_image_value[0]['title'];
                    }
                    $response['data'][$limit_count]['voucher_image'] = $voucher_image;
                    if (!empty($voucher->list_price)) {
                      $response['data'][$limit_count]['price'] = round($voucher_variation->list_price->number, 2);
                    }
                    $response['data'][$limit_count]['stock'] = $voucher_stock;
                    // Stock flap service to get stock flap status.
                    $stock_flap_service = \Drupal::service('allianz_custom.stock_check');
                    $stock_flap = $stock_flap_service->getVoucherStockFlap($voucher_id);
                    $stock_flap_status = "false";
                    if (!empty($stock_flap)) {
                      $stock_flap_status = "true";
                    }
                    $response['data'][$limit_count]['flap_status'] = $stock_flap_status;
                    if (!empty($voucher->field_discount)) {
                      $response['data'][$limit_count]['discount'] = $voucher->field_discount->value;
                    }
                    // Checking if Stock Alert Entity.
                    // For user and variation already exist.
                    $stock_alert_service = \Drupal::service('allianz_custom.stock_check');
                    if ($stock_alert_service->existStockAlert($voucher_id, $account_uid)) {
                      $response['data'][$limit_count]['stock_alert_exist'] = "true";
                    }
                    else {
                      $response['data'][$limit_count]['stock_alert_exist'] = "false";
                    }
                    $response['data'][$limit_count]['product_url'] = $product_url;
                    // Checking item is flagged or not by requested user.
                    $query = $connection->select('flagging', 'c')
                      ->fields('c', ['id'])
                      ->condition('c.entity_type', 'commerce_product_variation', '=')
                      ->condition('c.entity_id', $variation_id, '=')
                      ->condition('c.uid', $account_uid, '=')
                      ->execute();
                    $results = $query->fetchAssoc();
                    if (!empty($results)) {
                      $response['data'][$limit_count]['flagged'] = "true";
                    }
                    else {
                      $response['data'][$limit_count]['flagged'] = "false";
                    }
                    if ($limit_count >= 4) {
                      break;
                    }
                    $limit_count++;
                  }
                }
              }
              $error = 200;
            }
            else {
              // Return 404 if related product does not exists.
              $response['status'] = 'failure';
              $response['error'] = 'Related product does not exist.';
              $error = 404;
            }
          }
          catch (RequestException $e) {
            $response['status'] = 'failure';
            $response['error'] = 'Something went wrong!';
            $error = 500;
          }
        }
      }
      else {
        // Return 500 if requested product url does not exists.
        $response['status'] = 'failure';
        $response['error'] = 'Something went wrong!';
        $error = 500;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }
    $response = new ResourceResponse($response, $error);
    // Disable api cache.
    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);
    $response->addCacheableDependency($disable_cache);

    return $response;
  }

}
