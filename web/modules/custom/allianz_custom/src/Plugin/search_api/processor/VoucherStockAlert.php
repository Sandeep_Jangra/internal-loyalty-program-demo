<?php

namespace Drupal\allianz_custom\Plugin\search_api\processor;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;
use Drupal\commerce_product\Entity\Product;

/**
 * Adds the item's URL to the indexed data.
 *
 * @SearchApiProcessor(
 *   id = "voucher_stock_alert",
 *   label = @Translation("Voucher stock alert"),
 *   description = @Translation("Custom voucher stock alert"),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class VoucherStockAlert extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Voucher Stock Alert'),
        'description' => $this->t('Custom voucher stock alert'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['voucher_stock_alert'] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    $entity_type = $item->getDatasource()->getEntityTypeId($item->getOriginalObject());
    $entity_id = $item->getDatasource()->getItemId($item->getOriginalObject());
    $voucher_id = explode(":", $entity_id)[0];
    $voucher = Product::load($voucher_id);
    $voucher_stock_alert = "false";
    if ($voucher->bundle() == 'voucher') {
      // Query to validate coupon lifetime purchase limit.
      $request = \Drupal::request();
      if (!empty($request->headers->get('Authorization'))) {
        $authorization_token = trim(str_replace('Bearer ', '', $request->headers->get('Authorization')));
        // Load jwt transcoder object through services.
        $transcoder = \Drupal::service('jwt.transcoder');
        $token = $transcoder->decode($authorization_token);
        $account_uid = $token->getPayload()->drupal->uid;
        // Checking if Stock Alert Entity.
        // For this user and variation already exist.
        $stock_alert_service = \Drupal::service('allianz_custom.stock_check');
        if ($stock_alert_service->existStockAlert($voucher_id, $account_uid)) {
          $voucher_stock_alert = "true";
        }
      }
    }
    $fields = $this->getFieldsHelper()->filterForPropertyPath($item->getFields(), NULL, 'voucher_stock_alert');
    foreach ($fields as $field) {
      $field->addValue($voucher_stock_alert);
    }
  }

}
