<?php

namespace Drupal\allianz_custom\Plugin\search_api\processor;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Database\Database;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Adds the item's URL to the indexed data.
 *
 * @SearchApiProcessor(
 *   id = "voucher_display_status",
 *   label = @Translation("Voucher Display Status"),
 *   description = @Translation("Custom voucher display status"),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class VoucherDisplayStatus extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Voucher Display Status'),
        'description' => $this->t('Custom voucher display status'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['voucher_display_status'] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    $entity_type = $item->getDatasource()->getEntityTypeId($item->getOriginalObject());
    $entity_id = $item->getDatasource()->getItemId($item->getOriginalObject());
    $voucher_id = explode(":", $entity_id)[0];
    $voucher = Product::load($voucher_id);
    $voucher_display_status = TRUE;
    if ($voucher->bundle() == 'voucher') {
      // Get db connection.
      $connection = Database::getConnection();
      // Query to validate coupon lifetime purchase limit.
      $request = \Drupal::request();
      if (!empty($request->headers->get('Authorization'))) {
        $authorization_token = trim(str_replace('Bearer ', '', $request->headers->get('Authorization')));
        // Load jwt transcoder object through services.
        $transcoder = \Drupal::service('jwt.transcoder');
        $token = $transcoder->decode($authorization_token);
        $account_uid = $token->getPayload()->drupal->uid;
        $voucher_session_service = \Drupal::service('allianz_voucher_session.session_level_checkout');
        $session_level_limit = $voucher_session_service->getCartListSessionLimit($account_uid, $voucher);
        if ($session_level_limit == "not_exist" && $session_level_limit != "0") {
          $session_level_limit = 1;
        }
        // Get current time.
        $current_date = new DrupalDateTime();
        $current_date = $current_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
        $flap = $voucher->field_flap->value;
        $availibility_to = NULL;
        if ($flap == 'availability') {
          $end_date = new DrupalDateTime($voucher->field_availability_date->end_value, 'UTC');
          $end_date->setTimezone(timezone_open(date_default_timezone_get()));
          $availibility_to = $end_date->format('Y-m-d\TH:i:s');
        }
        if ($session_level_limit <= 0 || ($flap == 'availability' && $availibility_to < $current_date)) {
          $voucher_display_status = FALSE;
        }
      }
    }
    $fields = $this->getFieldsHelper()->filterForPropertyPath($item->getFields(), NULL, 'voucher_display_status');
    foreach ($fields as $field) {
      $field->addValue($voucher_display_status);
    }
  }

}
