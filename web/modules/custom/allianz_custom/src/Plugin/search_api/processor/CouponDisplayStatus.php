<?php

namespace Drupal\allianz_custom\Plugin\search_api\processor;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Database\Database;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Adds the item's URL to the indexed data.
 *
 * @SearchApiProcessor(
 *   id = "coupon_display_status",
 *   label = @Translation("Coupon Display Status"),
 *   description = @Translation("Custom coupon display status"),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class CouponDisplayStatus extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Coupon Display Status'),
        'description' => $this->t('Custom coupon display status'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['coupon_display_status'] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    $entity_type = $item->getDatasource()->getEntityTypeId($item->getOriginalObject());
    $entity_id = $item->getDatasource()->getItemId($item->getOriginalObject());
    $coupon_id = explode(":", $entity_id)[0];
    $coupon = Product::load($coupon_id);
    $coupon_display_status = TRUE;
    if ($coupon->bundle() == 'coupon') {
      $display_format = $coupon->field_display_format->value;
      // Get db connection.
      $connection = Database::getConnection();
      // Query to validate coupon lifetime purchase limit.
      $request = \Drupal::request();
      if (!empty($request->headers->get('Authorization'))) {
        $authorization_token = trim(str_replace('Bearer ', '', $request->headers->get('Authorization')));
        // Load jwt transcoder object through services.
        $transcoder = \Drupal::service('jwt.transcoder');
        $token = $transcoder->decode($authorization_token);
        $account_uid = $token->getPayload()->drupal->uid;
        $purchase_limit = $coupon->field_purchase_limit->value;
        $query = $connection->select('coupon_purchase_lifetime__field_quantity', 'q');
        $query->leftJoin('coupon_purchase_lifetime__field_coupon_id', 'ci', 'ci.entity_id = q.entity_id');
        $query->leftJoin('coupon_purchase_lifetime__field_user_id', 'ui', 'ui.entity_id = q.entity_id');
        $query->fields('q', ['field_quantity_value']);
        $query->condition('ui.field_user_id_target_id', $account_uid, '=');
        $query->condition('ci.field_coupon_id_target_id', $coupon_id, '=');
        $limit_results = $query->execute()->fetchAll();
        if ($purchase_limit <= count($limit_results)) {
          $coupon_display_status = FALSE;
        }
        $stock = $coupon->field_stock->value;
        if ($stock <= 0 && $display_format == 'with_code') {
          $coupon_display_status = FALSE;
        }
        // Get current time.
        $current_date = new DrupalDateTime();
        $current_date = $current_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
        $flap = $coupon->field_flap->value;
        $availibility_to = NULL;
        if ($flap == 'availability') {
          $end_date = new DrupalDateTime($coupon->field_availability_date->end_value, 'UTC');
          $end_date->setTimezone(timezone_open(date_default_timezone_get()));
          $availibility_to = $end_date->format('Y-m-d\TH:i:s');
        }
        if ($flap == 'availability' && $availibility_to < $current_date) {
          $coupon_display_status = FALSE;
        }
      }
    }
    $fields = $this->getFieldsHelper()->filterForPropertyPath($item->getFields(), NULL, 'coupon_display_status');
    foreach ($fields as $field) {
      $field->addValue($coupon_display_status);
    }
  }

}
