<?php

namespace Drupal\allianz_custom\Plugin\search_api\processor;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;
use Drupal\commerce_product\Entity\Product;

/**
 * Adds the item's URL to the indexed data.
 *
 * @SearchApiProcessor(
 *   id = "coupon_flap_status",
 *   label = @Translation("Coupon Flap Status Field"),
 *   description = @Translation("Custom coupon flap status field"),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class CouponFlapStatus extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Coupon Flap Status Field'),
        'description' => $this->t('Custom coupon flap status field'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['coupon_flap_status'] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    $entity_type = $item->getDatasource()->getEntityTypeId($item->getOriginalObject());
    $entity_id = $item->getDatasource()->getItemId($item->getOriginalObject());
    $coupon_id = explode(":", $entity_id)[0];
    $coupon = Product::load($coupon_id);
    $coupon_flap_status = "false";
    if ($coupon->bundle() == 'coupon') {
      $stock = $coupon->field_stock->value;
      if ($coupon->field_flap->value == 'stock') {
        $stock_threshold = $coupon->field_stock_threshold->value;
        if (!empty($stock_threshold) && !empty($stock) && $stock < $stock_threshold) {
          $coupon_flap_status = "true";
        }
      }
    }
    $fields = $this->getFieldsHelper()->filterForPropertyPath($item->getFields(), NULL, 'coupon_flap_status');
    foreach ($fields as $field) {
      $field->addValue($coupon_flap_status);
    }
  }

}
