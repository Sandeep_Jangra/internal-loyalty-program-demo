<?php

namespace Drupal\allianz_custom\Plugin\search_api\processor;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;
use Drupal\commerce_product\Entity\Product;

/**
 * Adds the item's URL to the indexed data.
 *
 * @SearchApiProcessor(
 *   id = "coupon_stock",
 *   label = @Translation("Coupon Stock"),
 *   description = @Translation("Custom coupon stock"),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class CouponStock extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Coupon Stock'),
        'description' => $this->t('Custom coupon stock'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['coupon_stock'] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    $entity_type = $item->getDatasource()->getEntityTypeId($item->getOriginalObject());
    $entity_id = $item->getDatasource()->getItemId($item->getOriginalObject());
    $coupon_id = explode(":", $entity_id)[0];
    $coupon = Product::load($coupon_id);
    $coupon_stock = "null";
    if ($coupon->bundle() == 'coupon') {
      $display_format = $coupon->field_display_format->value;
      if ($display_format == 'with_code') {
        $coupon_stock = $coupon->field_stock->value;
      }
    }
    $fields = $this->getFieldsHelper()->filterForPropertyPath($item->getFields(), NULL, 'coupon_stock');
    foreach ($fields as $field) {
      $field->addValue($coupon_stock);
    }
  }

}
