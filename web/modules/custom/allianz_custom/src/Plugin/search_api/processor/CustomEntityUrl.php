<?php

namespace Drupal\allianz_custom\Plugin\search_api\processor;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;

/**
 * Adds the item's URL to the indexed data.
 *
 * @SearchApiProcessor(
 *   id = "custom_entity_url",
 *   label = @Translation("Custom Entity Url"),
 *   description = @Translation("Custom entity url for search api"),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class CustomEntityUrl extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Custom Entity Url'),
        'description' => $this->t('Custom entity url for search api'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['custom_entity_url'] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    $entity_type = $item->getDatasource()->getEntityTypeId($item->getOriginalObject());
    $entity_id = $item->getDatasource()->getItemId($item->getOriginalObject());
    $entity_id = explode(":", $entity_id)[0];
    $entity_url = \Drupal::service('path.alias_manager')->getAliasByPath('/product/' . $entity_id);
    $fields = $this->getFieldsHelper()->filterForPropertyPath($item->getFields(), NULL, 'custom_entity_url');
    foreach ($fields as $field) {
      $field->addValue($entity_url);
    }
  }

}
