<?php

namespace Drupal\allianz_custom\Plugin\search_api\processor;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;
use Drupal\commerce_product\Entity\Product;

/**
 * Adds the item's URL to the indexed data.
 *
 * @SearchApiProcessor(
 *   id = "voucher_flap_status",
 *   label = @Translation("Voucher Flap Status Field"),
 *   description = @Translation("Custom voucher flap status field"),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class VoucherFlapStatus extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Voucher Flap Status Field'),
        'description' => $this->t('Custom voucher flap status field'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['voucher_flap_status'] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    $entity_type = $item->getDatasource()->getEntityTypeId($item->getOriginalObject());
    $entity_id = $item->getDatasource()->getItemId($item->getOriginalObject());
    $voucher_id = explode(":", $entity_id)[0];
    $voucher = Product::load($voucher_id);
    $voucher_flap_status = "false";
    if ($voucher->bundle() == 'voucher') {
      $voucher_id = $voucher->id();
      // Stock flap service to get stock flap status.
      $stock_flap_service = \Drupal::service('allianz_custom.stock_check');
      $stock_flap = $stock_flap_service->getVoucherStockFlap($voucher_id);
      if (!empty($stock_flap)) {
        $voucher_flap_status = "true";
      }
    }
    $fields = $this->getFieldsHelper()->filterForPropertyPath($item->getFields(), NULL, 'voucher_flap_status');
    foreach ($fields as $field) {
      $field->addValue($voucher_flap_status);
    }
  }

}
