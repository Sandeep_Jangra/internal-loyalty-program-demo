<?php

namespace Drupal\allianz_custom\Plugin\search_api\processor;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;
use Drupal\commerce_product\Entity\Product;

/**
 * Adds the item's URL to the indexed data.
 *
 * @SearchApiProcessor(
 *   id = "voucher_stock",
 *   label = @Translation("Voucher stock Field"),
 *   description = @Translation("Custom voucher stock field"),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class VoucherStock extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Voucher Stock Field'),
        'description' => $this->t('Custom voucher stock field'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['voucher_stock'] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    $entity_type = $item->getDatasource()->getEntityTypeId($item->getOriginalObject());
    $entity_id = $item->getDatasource()->getItemId($item->getOriginalObject());
    $voucher_id = explode(":", $entity_id)[0];
    $voucher = Product::load($voucher_id);
    $voucher_stock = 0;
    if ($voucher->bundle() == 'voucher') {
      // Get default variation ids.
      $variations = $voucher->getVariations();
      if (!empty($variations)) {
        foreach ($variations as $key => $variation) {
          $voucher_stock = $voucher_stock + $variation->field_stock->value;
        }
      }
    }
    $fields = $this->getFieldsHelper()->filterForPropertyPath($item->getFields(), NULL, 'voucher_stock');
    foreach ($fields as $field) {
      $field->addValue($voucher_stock);
    }
  }

}
