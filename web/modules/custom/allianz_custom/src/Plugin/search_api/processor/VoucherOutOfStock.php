<?php

namespace Drupal\allianz_custom\Plugin\search_api\processor;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;
use Drupal\commerce_product\Entity\Product;

/**
 * Adds the item's URL to the indexed data.
 *
 * @SearchApiProcessor(
 *   id = "voucher_out_of_stock",
 *   label = @Translation("Voucher Out of stock Field"),
 *   description = @Translation("Custom voucher out of stock field"),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class VoucherOutOfStock extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Voucher Out of Stock Field'),
        'description' => $this->t('Custom voucher out of stock field'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['voucher_out_of_stock'] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    $entity_type = $item->getDatasource()->getEntityTypeId($item->getOriginalObject());
    $entity_id = $item->getDatasource()->getItemId($item->getOriginalObject());
    $voucher_id = explode(":", $entity_id)[0];
    $voucher = Product::load($voucher_id);
    $out_of_stock_status = "false";
    if ($voucher->bundle() == 'voucher') {
      $voucher_id = $voucher->id();
      // Checkout out of stock variation.
      $stock_flap_service = \Drupal::service('allianz_custom.stock_check');
      $out_of_stock = $stock_flap_service->isOutOfStock($voucher_id);
      if (!empty($out_of_stock)) {
        $out_of_stock_status = "true";
      }
    }
    $fields = $this->getFieldsHelper()->filterForPropertyPath($item->getFields(), NULL, 'voucher_out_of_stock');
    foreach ($fields as $field) {
      $field->addValue($out_of_stock_status);
    }
  }

}
