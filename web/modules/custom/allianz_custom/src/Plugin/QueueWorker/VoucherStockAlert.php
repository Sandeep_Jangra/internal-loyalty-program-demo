<?php

namespace Drupal\allianz_custom\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\user\Entity\User;
use Drupal\commerce_product\Entity\Product;
use Drupal\allianz_custom\Entity\StockAlert;

/**
 * Processes Voucher Back in Stock Email Tasks.
 *
 * @QueueWorker(
 *   id = "voucher_stock_alert",
 *   title = @Translation("Task Worker: Send email for voucher back in stock notification."),
 *   cron = {"time" = 10}
 * )
 */
class VoucherStockAlert extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($response) {
    if (!empty($response)) {
      $entity_id = $response->entity_id;
      $voucher_id = $response->field_voucher_id_target_id;
      $voucher = Product::load($voucher_id);
      // Check if this voucher is published or not.
      if (!empty($voucher) && $voucher->isPublished()) {
        $product_name = $voucher->getTitle();
        $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/product/' . $voucher_id);
        $product_url = $_ENV['REDIRECT_URI'] . $alias;
        // Load User with user id.
        $user_id = $response->field_user_id_target_id;
        $user = User::load($user_id);
        $email = $user->getEmail();
        $salutation = $user->field_salutation->value;
        $name = $user->field_last_name->value;
        if (empty($salutation) || empty($name)) {
          $salutation_text = "Sehr geehrte Damen und Herren";
        } elseif ($salutation == "Frau") {
          $salutation_text = "Liebe Frau " . $name;
        } elseif ($salutation == "Herr") {
          $salutation_text = "Lieber Herr " . $name;
        }

        // Stock check service to get stock status.
        $stock_service = \Drupal::service('allianz_custom.stock_check');

        if (!$stock_service->isOutOfStock($voucher_id)) {
          // Trigger Email when a variation is back in stock.
          $warpit = \Drupal::service('warpit.client');
          $warpit_data = [
            "senderToken" => "bae2566d-481a-4d7a-bca4-03df36f899f2",
            "messageType" => "email",
            "messageProviderId" => 1,
            "templateId" => 3,
            "users" => [[
              "email" => $email,
              "product_name" => $product_name,
              "product_url" => $product_url,
              "last_name" => $salutation_text
            ],
            ],
          ];
          $status = $warpit->send($warpit_data);
          \Drupal::logger('voucher_stock_alert_email')->debug('<pre>' . json_encode($email) . '</pre>');
          \Drupal::logger('voucher_stock_alert_log')->debug('<pre>' . json_encode($status) . '</pre>');
          if ($status['code'] && $status['message'] == 'Success') {
            // @todo
            // If email triggered successfully.
            $stock_alert = StockAlert::load($entity_id);
            if (!empty($stock_alert)) {
              $stock_alert->field_status->value = 'inactive';
              $stock_alert->save();
            }
          }
          else {
            // If email trigger is not sent.
            \Drupal::logger('voucher_stock_alert')->error($status['message']);
          }
        }
      }
    }
  }

}
