<?php

namespace Drupal\allianz_custom\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\user\Entity\User;
use Drupal\commerce_product\Entity\ProductVariation;

/**
 * Processes Voucher Low Stock Alert Tasks.
 *
 * @QueueWorker(
 *   id = "send_variation_email_queue",
 *   title = @Translation("Task Worker: Send email for voucher low stock notification."),
 *   cron = {"time" = 10}
 * )
 */
class SendStockEmail extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($response) {
    if (!empty($response)) {
      $voucher_variation = ProductVariation::load($response);
      if (!empty($voucher_variation)) {
        $voucher_product = $voucher_variation->getProduct();
        // Check if this voucher is published or not.
        if ($voucher_product->isPublished()) {
          $voucher_name = $voucher_variation->getTitle();
          $threshold_value = $voucher_variation->field_stock_email_threshold->value;
          $current_stock = $voucher_variation->field_stock->value;
          $supplier_id = $voucher_product->field_supplier_id->target_id;
          $base_url = \Drupal::request()->getSchemeAndHttpHost();
          $product_url = $base_url . "/product/" . $voucher_product->id() . "/edit";
          $variation_price = $voucher_variation->getPrice();
          $price = $variation_price->__toString();
          // Load Supplier with supplier id.
          $supplier = User::load($supplier_id);
          if (!empty($supplier)) {
            $email_values = $supplier->get('field_supplier_email')->getValue();
          }
          $warpit = \Drupal::service('warpit.client');
          // Trigger Email when stock count is less than threshold value.
          foreach ($email_values as $key => $value) {
            $warpit_data = [
              "senderToken" => "bae2566d-481a-4d7a-bca4-03df36f899f2",
              "messageType" => "email",
              "messageProviderId" => 1,
              "templateId" => 2,
              "users" => [[
                "email" => $value['value'],
                "product_name" => $voucher_name,
                "product_url" => $product_url,
                "current_stock" => $current_stock,
                "variation" => $price,
              ],
              ],
            ];
            $status = $warpit->send($warpit_data);
            if ($status['success'] != '1') {
              // @todo
              // If email trigger is not sent.
              \Drupal::logger('voucher_stock_alert_email')->error($status['message']);
            }
          }
        }
      }
    }
  }

}
