(function ($, Drupal) {
	var display_format = jQuery('input[type=radio][name=field_display_format]:checked').val();
	if (display_format == 'without_code') {
		jQuery('div#edit-field-flap > div:eq(2)').css("display", "none");
	  jQuery('div.form-item-field-stock-threshold-0-value').hide();
	}
	jQuery('input[type=radio][name=field_display_format]').change(function() {
	  if (this.value == 'with_code') {
      jQuery('div#edit-field-flap > div:eq(2)').css("display", "block");
      jQuery('div.form-item-field-stock-threshold-0-value').show();
	  }
	  else if (this.value == 'without_code') {
      jQuery('div#edit-field-flap > div:eq(2)').css("display", "none");
      jQuery('div.form-item-field-stock-threshold-0-value').hide();
	  }
	});
}(jQuery, Drupal));