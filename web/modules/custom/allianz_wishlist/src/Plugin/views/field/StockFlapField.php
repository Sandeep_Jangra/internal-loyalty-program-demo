<?php

namespace Drupal\allianz_wishlist\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;

/**
 * A handler to provide a Stock Flap Field.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("custom_stock_flap_field")
 */
class StockFlapField extends FieldPluginBase {
  /**
   * The current display.
   *
   * @var string
   *   The current display of the view.
   */
  protected $currentDisplay;

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->currentDisplay = $view->current_display;
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    // First check whether the field should be hidden if the value(hide_alter_empty = TRUE) /the rewrite is empty (hide_alter_empty = FALSE).
    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $commerce_entity = $values->_entity;
    $attribute_value = [];
    if ($commerce_entity->getEntityTypeId() == 'commerce_wishlist_item') {
      $purchasable_entity = $commerce_entity->getPurchasableEntity();
      if (!empty($purchasable_entity)) {
        $product_id = $purchasable_entity->getProductId();
        // Stock flap service to get stock flap status.
        $stock_flap_service = \Drupal::service('allianz_custom.stock_check');
        $stock_flap = $stock_flap_service->getVoucherStockFlap($product_id);
        $stock_flap_status = "false";
        if (!empty($stock_flap)) {
          $stock_flap_status = "true";
        }
      }
    }
    else {
      $product_id = $commerce_entity->id();
      // Stock flap service to get stock flap status.
      $stock_flap_service = \Drupal::service('allianz_custom.stock_check');
      $stock_flap = $stock_flap_service->getVoucherStockFlap($product_id);
      $stock_flap_status = "false";
      if (!empty($stock_flap)) {
        $stock_flap_status = "true";
      }
    }
    return $stock_flap_status;
  }

}
