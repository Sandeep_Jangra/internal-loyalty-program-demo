<?php

namespace Drupal\allianz_wishlist\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\allianz_voucher_session\Entity\SessionLevelCheckout;

/**
 * A handler to provide a Session level limit Field.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("session_level_limit_field")
 */
class SessionLevelLimit extends FieldPluginBase {
  /**
   * The current display.
   *
   * @var string
   *   The current display of the view.
   */
  protected $currentDisplay;

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->currentDisplay = $view->current_display;
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    // First check whether the field should be hidden if the value(hide_alter_empty = TRUE) /the rewrite is empty (hide_alter_empty = FALSE).
    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $commerce_entity = $values->_entity;
    $session_level_limit = '';
    if ($commerce_entity->getEntityTypeId() == 'commerce_wishlist_item') {
      $purchasable_entity = $commerce_entity->getPurchasableEntity();
      if (!empty($purchasable_entity)) {
        $product_id = $purchasable_entity->getProductId();
        $product = $purchasable_entity->getProduct();
        if (!empty($product->field_session_level_limit) && !empty($product->field_session_level_limit->value)) {
          $session_level_limit = (int) $product->field_session_level_limit->value;
          $request = \Drupal::request();
          $authorization_token = trim(str_replace('Bearer ', '', $request->headers->get('Authorization')));
          // Load jwt transcoder object through services.
          $transcoder = \Drupal::service('jwt.transcoder');
          $token = $transcoder->decode($authorization_token);
          $account_uid = $token->getPayload()->drupal->uid;
          // Select query to get session purchased limit for voucher.
          $query = \Drupal::entityQuery('session_level_checkout');
          $query->condition('field_user_id', $account_uid, '=');
          $query->condition('field_voucher_id', $product_id, '=');
          $query->accessCheck(FALSE);
          $session_limit_result = $query->execute();
          if (!empty($session_limit_result)) {
            $purchased_quantity = 0;
            foreach ($session_limit_result as $session_limit_key => $session_limit_value) {
              $session_limit = SessionLevelCheckout::load($session_limit_value);
              if (!empty($session_limit)) {
                if (!empty($session_limit->field_quantity) && !empty($session_limit->field_quantity->value)) {
                  $purchased_quantity = $purchased_quantity + $session_limit->field_quantity->value;
                }
              }
            }
            $session_level_limit = $session_level_limit - $purchased_quantity;
          }
        }
      }
    }
    return $session_level_limit;
  }

}
