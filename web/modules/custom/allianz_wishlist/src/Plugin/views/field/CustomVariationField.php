<?php

namespace Drupal\allianz_wishlist\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\allianz_product\Controller\AllianzField;

/**
 * A handler to provide a variation attribute field.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("custom_variation_field")
 */
class CustomVariationField extends FieldPluginBase {
  /**
   * The current display.
   *
   * @var string
   *   The current display of the view.
   */
  protected $currentDisplay;

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->currentDisplay = $view->current_display;
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    // First check whether the field should be hidden if the value(hide_alter_empty = TRUE) /the rewrite is empty (hide_alter_empty = FALSE).
    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $wishlist_item = $values->_entity;
    $attribute_value = [];
    $purchasable_entity = $wishlist_item->getPurchasableEntity();
    if (!empty($purchasable_entity) && !empty($purchasable_entity->getAttributeValueIds())) {
      // Create AllianzField class object.
      $allianz_field = new AllianzField();
      foreach ($purchasable_entity->getAttributeValueIds() as $key => $value) {
        $key = explode('attribute_', $key);
        $key = ucfirst($key[1]);
        $attribute_value[$key] = ucwords($allianz_field->getAttributeName($value));
      }
      if (!empty($attribute_value)) {
        $attribute_value = json_encode($attribute_value);
      }
    }
    return $attribute_value;
  }

}
