<?php

namespace Drupal\allianz_wishlist\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ModifiedResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Psr\Log\LoggerInterface;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\jwt\Authentication\Event\JwtAuthGenerateEvent;
use Drupal\jwt\JsonWebToken\JsonWebToken;
use Drupal\jwt\Authentication\Event\JwtAuthEvents;

/**
 * AddWishlistItemRestResource custom rest post api that added item to wishlist for requested user.
 *
 * @RestResource(
 *   id = "product_wishlist_rest_resource",
 *   label = @Translation("Add Wishlist Item rest resource"),
 *   uri_paths = {
 *     "create" = "/api/v1/wishlist/add"
 *   }
 * )
 */
class AddWishlistItemRestResource extends ResourceBase {
  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Responds to POST requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post($data) {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $jwt = $authorization_token;
    $token = $transcoder->decode($jwt);
    $account_uid = $token->getPayload()->drupal->uid;
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if ((!empty($results) && $results['jwt_token'] == $jwt)) {
      if (!empty($data) && !empty($data['product_id']) && !empty($account_uid)) {
        // Load user object through user id.
        $account = User::load($account_uid);
        $wishlist_name = 'wishlist_' . $account_uid;
        // Load product item.
        $purchasable_entity = ProductVariation::load($data['product_id']);
        if (!empty($purchasable_entity)) {
          // Load wishlist manager object through services.
          $wishlist_manager = \Drupal::service('commerce_wishlist.wishlist_manager');
          // Load wishlist provider object through services.
          $wishlist_provider = \Drupal::service('commerce_wishlist.wishlist_provider');
          // Load wishlist resolver object through services.
          $wishlist_type_resolver = \Drupal::service('commerce_wishlist.chain_wishlist_type_resolver');
          try {
            $wishlist_item = $wishlist_manager->createWishlistItem($purchasable_entity, 1);
            $wishlist_type = $wishlist_type_resolver->resolve($wishlist_item);
            $wishlist = $wishlist_provider->getWishlist($wishlist_type);
            if (!$wishlist) {
              $wishlist = $wishlist_provider->createWishlist($wishlist_type, $account, $wishlist_name);
            }
            $wishlist_manager->addWishlistItem($wishlist, $wishlist_item);
            $flag_service = \Drupal::service('flag');
            $flag = $flag_service->getFlagById('wishlist');
            // Flag an entity with a specific flag.
            $flag_service->flag($flag, $purchasable_entity);
            // Generate refresh token for user
            // Load dispatcher object through services.
            $dispatcher = \Drupal::service('event_dispatcher');
            $event = new JwtAuthGenerateEvent(new JsonWebToken());
            $dispatcher->dispatch(JwtAuthEvents::GENERATE, $event);
            // Generate jwt token for requested user.
            $event->addClaim('exp', strtotime('+2000 minutes'));
            $event->addClaim(['drupal', 'uid'], $account_uid);
            $jwt = $event->getToken();
            $refresh_token = $transcoder->encode($jwt);
            // Update refrest token to custom table.
            $token_updated = $connection->update('custom_jwt_token')
              ->fields([
                'jwt_token' => $refresh_token,
              ])
              ->condition('uid', $account_uid, '=')
              ->execute();

            $response['status'] = 'success';
            $response['data']['token'] = $refresh_token;
            $error = 200;
          }
          catch (RequestException $e) {
            $response['status'] = 'failure';
            $response['error'] = 'Something went wrong!';
            $error = 500;
          }
        }
        else {
          // Return 404 if Requested product does not exits.
          $response['status'] = 'failure';
          $response['error'] = 'Requested product does not exits';
          $error = 404;
        }
      }
      else {
        // Return 500 on wrong post request.
        $response['status'] = 'failure';
        $response['error'] = 'Something went wrong!';
        $error = 500;
      }
    }
    else {
      // Return 401 if jwt token does not exist or expired.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }
    $response = new ModifiedResourceResponse($response, $error);

    return $response;
  }

}
