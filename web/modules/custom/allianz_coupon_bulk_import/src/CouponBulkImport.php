<?php

namespace Drupal\allianz_coupon_bulk_import;

use Drupal\commerce_product\Entity\Product;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Provides a Bulk Coupon code Import function.
 */
class CouponBulkImport {

  /**
   * {@inheritdoc}
   */
  public static function bulkimport($count, $getData, $coupon_id, $imported_rows, $total_rows, &$context) {
    $message = 'Importing Coupons Code...';
    $context['results']['count'] = [];
    $coupon = Product::load($coupon_id);
    if (!empty($coupon) && !empty($getData)) {
      $coupon_code = $getData[0];
      if (!empty($coupon->field_coupon_codes)) {
        $array_coupon_code = $coupon->field_coupon_codes->getValue();
      }
      else {
        $array_coupon_code = [];
      }
      $paragraph = Paragraph::create([
        'type' => 'coupon_code',
        'field_coupon_code' => $coupon_code,
        'field_status' => 'active',
      ]);
      $paragraph->save();
      array_push($array_coupon_code, [
        'target_id' => $paragraph->id(),
        'target_revision_id' => $paragraph->getRevisionId(),
      ]);
      $coupon->field_coupon_codes = $array_coupon_code;
      $coupon->save();
      // Call coupon stock update service.
      $coupon_service = \Drupal::service('allianz_coupon_bulk_import.update_coupon_code');
      $coupon_service->updateCouponStock($coupon_id);
    }
    $context['results']['total_csv_rows'] = $total_rows;
    $context['results']['imported_rows'] = $imported_rows;

  }

  /**
   * {@inheritdoc}
   */
  public static function bulkimportFinishedCallback($success, $results, $operations) {
    if ($success) {
      if (!empty($results)) {
        $message = \Drupal::translation()
          ->formatPlural(count($results), '', '@imported_rows rows imported out of @total_rows.', ['@total_rows' => $results['total_csv_rows'], '@imported_rows' => $results['imported_rows']]);
      }
      else {
        $message = \Drupal::translation()
          ->formatPlural(count($results), '', '0 rows imported. Rest were not imported due to coupon code duplicacy.');
      }
    }
    else {
      $message = t('Finished with an error.');
    }
    drupal_set_message($message);
  }

}
