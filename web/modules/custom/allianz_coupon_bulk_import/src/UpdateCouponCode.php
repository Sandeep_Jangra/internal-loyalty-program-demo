<?php

namespace Drupal\allianz_coupon_bulk_import;

/**
 * Class UpdateCouponCode.
 */
class UpdateCouponCode {

  /**
   * Methods to update coupon stock.
   */
  public function updateCouponStock($entity_id) {
    // Database connection.
    $connection = \Drupal::database();
    // Query to count all active coupon code.
    $query = $connection->select('paragraph__field_status', 'ps');
    $query->fields('ps', ['entity_id']);
    $query->innerJoin('commerce_product__field_coupon_codes', 'cp', 'cp.field_coupon_codes_target_id = ps.entity_id');
    $query->condition('cp.bundle', 'coupon', '=');
    $query->condition('cp.entity_id', $entity_id, '=');
    $query->condition('ps.field_status_value', 'active', '=');
    $result = $query->execute()->fetchAll();
    // Updated coupon stock field value based on active coupon code.
    $connection->update('commerce_product__field_stock')
      ->fields([
        'field_stock_value' => count($result),
      ])
      ->condition('entity_id', $entity_id, '=')
      ->execute();
  }

  /**
   * Methods to get total active coupon.
   */
  public function getTotalActiveCoupon($entity_id) {
    // Database connection.
    $connection = \Drupal::database();
    // Query to count all active coupon code.
    $query = $connection->select('paragraph__field_status', 'ps');
    $query->fields('ps', ['entity_id']);
    $query->innerJoin('commerce_product__field_coupon_codes', 'cp', 'cp.field_coupon_codes_target_id = ps.entity_id');
    $query->condition('cp.bundle', 'coupon', '=');
    $query->condition('cp.entity_id', $entity_id, '=');
    $query->condition('ps.field_status_value', 'active', '=');
    $result = $query->execute()->fetchAll();

    return count($result);
  }

}
