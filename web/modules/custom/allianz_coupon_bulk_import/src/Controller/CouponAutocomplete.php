<?php

namespace Drupal\allianz_coupon_bulk_import\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\Element\EntityAutocomplete;

class CouponAutocomplete extends ControllerBase {

  /**
   * Returns response for the autocompletion.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request object containing the search string.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response containing the autocomplete suggestions.
   */

  public function autocomplete(Request $request) {
    $matches = [];
    $string = $request->query->get('q');
    // Get current user role
    $current_user = \Drupal::currentUser();
    $user_id = $current_user->id();
    $roles = $current_user->getRoles();
    if ($string) {
      $query = \Drupal::entityQuery('commerce_product');
      $query->condition('type', 'coupon');
      $query->condition('title', '%'.db_like($string).'%', 'LIKE');
      $query->condition('field_display_format', 'with_code');
      if (in_array('avp_partners', $roles)) {
        $query->condition('field_supplier_id', $user_id);
      }
      $coupon_ids = $query->execute();
      $result = entity_load_multiple('commerce_product', $coupon_ids);
      foreach ($result as $row) {
        $matches[] = ['value' => EntityAutocomplete::getEntityLabels([$row]), 'label' => $row->title->value];
      }
    }
    return new JsonResponse($matches);
  }
}
