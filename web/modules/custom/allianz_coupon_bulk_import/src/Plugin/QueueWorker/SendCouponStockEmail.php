<?php

namespace Drupal\allianz_coupon_bulk_import\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\user\Entity\User;
use Drupal\commerce_product\Entity\Product;

/**
 * Processes Coupon Low Stock Email Alert Tasks.
 *
 * @QueueWorker(
 *   id = "send_coupon_email_queue",
 *   title = @Translation("Task Worker: Send email for coupon low stock notification."),
 *   cron = {"time" = 10}
 * )
 */
class SendCouponStockEmail extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($response) {
    if (!empty($response)) {
      $coupon = Product::load($response);
      // Check if this voucher is published or not.
      if ($coupon->isPublished()) {
        $coupon_name = $coupon->getTitle();
        $threshold_value = $coupon->field_stock_email_threshold->value;
        $current_stock = $coupon->field_stock->value;
        $supplier_id = $coupon->field_supplier_id->target_id;
        $base_url = \Drupal::request()->getSchemeAndHttpHost();
        $product_url = $base_url . "/product/" . $coupon->id() . "/edit";
        // Load Supplier with supplier id.
        $supplier = User::load($supplier_id);
        if (!empty($supplier)) {
          $email_values = $supplier->get('field_supplier_email')->getValue();
        }
        $warpit = \Drupal::service('warpit.client');
        // Trigger Email when stock count is less than threshold value.
        foreach ($email_values as $key => $value) {
          $warpit_data = [
            "senderToken" => "bae2566d-481a-4d7a-bca4-03df36f899f2",
            "messageType" => "email",
            "messageProviderId" => 1,
            "templateId" => 4,
            "users" => [[
              "email" => $value['value'],
              "product_name" => $coupon_name,
              "product_url" => $product_url,
              "current_stock" => $current_stock,
            ],
            ],
          ];
          $status = $warpit->send($warpit_data);
          if ($status['success'] != '1') {
            // @todo
            // If email trigger is not sent.
            \Drupal::logger('send_coupon_email_queue')->error($status['message']);
          }
        }
      }
    }
  }

}
