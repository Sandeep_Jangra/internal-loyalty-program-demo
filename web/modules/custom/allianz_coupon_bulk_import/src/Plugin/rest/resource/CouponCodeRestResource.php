<?php

namespace Drupal\allianz_coupon_bulk_import\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_product\Entity\Product;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\jwt\Authentication\Event\JwtAuthGenerateEvent;
use Drupal\jwt\JsonWebToken\JsonWebToken;
use Drupal\jwt\Authentication\Event\JwtAuthEvents;
use Drupal\allianz_custom\Entity\CouponPurchaseLifetime;

/**
 * Provides a resource to get coupon code for requested coupon.
 *
 * @RestResource(
 *   id = "coupon_code_rest_resource",
 *   label = @Translation("Coupon code rest resource"),
 *   uri_paths = {
 *     "create" = "/api/v1/coupon/code"
 *   }
 * )
 */
class CouponCodeRestResource extends ResourceBase {
  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new CouponCodeRestResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
      array $configuration,
      $plugin_id,
      $plugin_definition,
      array $serializer_formats,
      LoggerInterface $logger,
      AccountProxyInterface $current_user,
      Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->getParameter('serializer.formats'),
          $container->get('logger.factory')->get('allianz_coupon_bulk_import'),
          $container->get('current_user'),
          $container->get('request_stack')->getCurrentRequest()
      );
  }

  /**
   * Responds to POST requests.
   *
   * @param string $payload
   *   Get data object from request.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post($payload) {
    if (!empty($payload) && array_key_exists("coupon_id", $payload)) {
      $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
      // Load jwt transcoder object through services.
      $transcoder = \Drupal::service('jwt.transcoder');
      $jwt = $authorization_token;
      $token = $transcoder->decode($jwt);
      $account_uid = $token->getPayload()->drupal->uid;
      // Get db connection.
      $connection = Database::getConnection();
      // Select query to get requested user token for authentication.
      $query = $connection->select('custom_jwt_token', 'c')
        ->fields('c', ['uid', 'jwt_token'])
        ->condition('c.uid', $account_uid, '=')
        ->execute();
      $results = $query->fetchAssoc();
      if ((!empty($results) && $results['jwt_token'] == $jwt)) {
        // Create session checkout object with attached file.
        $coupon = Product::load($payload['coupon_id']);
        if (!empty($coupon) && $coupon->isPublished() == 1) {
          // Query to validate coupon lifetime purchase limit.
          $purchase_limit = $coupon->field_purchase_limit->value;
          $query = $connection->select('coupon_purchase_lifetime__field_quantity', 'q');
          $query->leftJoin('coupon_purchase_lifetime__field_coupon_id', 'ci', 'ci.entity_id = q.entity_id');
          $query->leftJoin('coupon_purchase_lifetime__field_user_id', 'ui', 'ui.entity_id = q.entity_id');
          $query->fields('q', ['field_quantity_value']);
          $query->condition('ui.field_user_id_target_id', $account_uid, '=');
          $query->condition('ci.field_coupon_id_target_id', $payload['coupon_id'], '=');
          $limit_results = $query->execute()->fetchAll();
          if ($purchase_limit > count($limit_results)) {
            // Query to get active coupon code.
            $query = $connection->select('paragraph__field_status', 'ps');
            $query->fields('ps', ['entity_id']);
            $query->innerJoin('paragraph__field_coupon_code', 'pc', 'pc.entity_id = ps.entity_id');
            $query->innerJoin('commerce_product__field_coupon_codes', 'cp', 'cp.field_coupon_codes_target_id = ps.entity_id');
            $query->fields('pc', ['field_coupon_code_value']);
            $query->condition('cp.bundle', 'coupon', '=');
            $query->condition('cp.entity_id', $payload['coupon_id'], '=');
            $query->condition('ps.field_status_value', 'active', '=');
            $result = $query->execute()->fetchAssoc();
            if (!empty($result)) {
              try {
                $paragraph_id = $result['entity_id'];
                $coupon_code = $result['field_coupon_code_value'];
                $paragraph = Paragraph::load($paragraph_id);
                $paragraph->field_user_id->target_id = $account_uid;
                $paragraph->field_status->value = 'used';
                $paragraph->field_coupon_used_date->value = time() + 16200;
                $paragraph->save();
                // Query to count all active coupon code.
                $query = $connection->select('paragraph__field_status', 'ps');
                $query->fields('ps', ['entity_id']);
                $query->innerJoin('commerce_product__field_coupon_codes', 'cp', 'cp.field_coupon_codes_target_id = ps.entity_id');
                $query->condition('cp.bundle', 'coupon', '=');
                $query->condition('cp.entity_id', $payload['coupon_id'], '=');
                $query->condition('ps.field_status_value', 'active', '=');
                $results = $query->execute()->fetchAll();
                // Updated coupon stock field value based on active coupon code.
                $coupon->field_stock->value = count($results);
                $coupon->save();
                if (!empty($coupon->field_display_format) && $coupon->field_display_format->value == 'without_code') {
                  $end_product_url = $coupon->field_end_product_url->uri;
                }
                else {
                  $end_product_url = NULL;
                }
                // Generate refresh token for user
                // Load dispatcher object through services.
                $dispatcher = \Drupal::service('event_dispatcher');
                $event = new JwtAuthGenerateEvent(new JsonWebToken());
                $dispatcher->dispatch(JwtAuthEvents::GENERATE, $event);
                // Generate jwt token for requested user.
                $event->addClaim('exp', strtotime('+2000 minutes'));
                $event->addClaim(['drupal', 'uid'], $account_uid);
                $jwt = $event->getToken();
                $refresh_token = $transcoder->encode($jwt);
                // Update refrest token to custom table.
                $token_updated = $connection->update('custom_jwt_token')
                  ->fields([
                    'jwt_token' => $refresh_token,
                  ])
                  ->condition('uid', $account_uid, '=')
                  ->execute();
                // Generate lifetime purchase limit entity.
                $stock_alert = CouponPurchaseLifetime::create([
                  'name' => 'Coupon Lifetime Limit - ' . REQUEST_TIME,
                  'user_id' => 1,
                  'created' => REQUEST_TIME,
                  'changed' => REQUEST_TIME,
                  'status' => TRUE,
                  'field_user_id' => [
                    'target_id' => $account_uid,
                  ],
                  'field_coupon_id' => [
                    'target_id' => $payload['coupon_id'],
                  ],
                  'field_quantity' => 1,
                ]);
                $stock_alert->save();
                $response['status'] = 'success';
                $response['data']['code'] = $coupon_code;
                $response['data']['url'] = $end_product_url;
                $response['data']['stock'] = count($results);
                $response['data']['token'] = $refresh_token;
                $error = 200;
              }
              catch (RequestException $e) {
                $response['status'] = 'failure';
                $response['error'] = 'Something went wrong!';
                $error = 500;
              }
            }
            else {
              // Return 404 if requested url does not exists.
              $response['status'] = 'failure';
              $response['error'] = 'Requested coupon does not have enough code.';
              $error = 404;
            }
          }
          else {
            $error = 500;
            $response['status'] = 'failure';
            $response['error'] = 'Something went wrong!';
          }
        }
        else {
          // Return 404 if requested url does not exists.
          $response['status'] = 'failure';
          $response['error'] = 'Requested coupon does not exist or unpublished.';
          $error = 404;
        }
      }
      else {
        // Return 401 if requested token is invalid.
        $response['status'] = 'failure';
        $response['error'] = 'Permission denied';
        $error = 401;
      }
    }
    else {
      // Return 500 if requested params is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Something went wrong!';
      $error = 500;
    }
    $response = new ResourceResponse($response, $error);
    // Disable api cache.
    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);
    $response->addCacheableDependency($disable_cache);
    return $response;
  }

}
