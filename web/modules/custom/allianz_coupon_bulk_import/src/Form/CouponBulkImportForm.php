<?php

namespace Drupal\allianz_coupon_bulk_import\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\Core\Database\Database;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Entity\Element\EntityAutocomplete;

/**
 * Class CouponBulkImportForm.
 */
class CouponBulkImportForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'coupon_bulk_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['coupon_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Select Coupon'),
      '#autocomplete_route_name' => 'allianz_coupon_bulk_import.autocomplete',
      '#description' => $this->t('Select coupon'),
      '#weight' => '0',
      '#required' => TRUE,
    ];
    $form['csv_import'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Upload .csv coupon code file'),
      '#required' => TRUE,
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
      ],
      '#weight' => '1',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#weight' => '2',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $coupon_id = EntityAutocomplete::extractEntityIdFromAutocompleteInput($form_state->getValue('coupon_id'));
    if (!empty($coupon_id)) {
      $coupon = Product::load($coupon_id);
      $display_format = $coupon->field_display_format->value;
      if ($display_format == 'without_code') {
        $form_state->setErrorByName('coupon_id', t('You can not import coupon code in coupon with without code display format.'));
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $coupon_id = EntityAutocomplete::extractEntityIdFromAutocompleteInput($form_state->getValue('coupon_id'));
    $file_id = $form_state->getValue('csv_import')[0];
    $filename = File::load($file_id);
    // Get total no of rows in csv file.
    $file_rows = file($filename->getFileUri());
    $total_rows = count($file_rows) - 1;
    $file = fopen($filename->getFileUri(), "r");
    $count = 0;
    $imported_rows = 1;
    $batch = [
      'title' => t('Importing Coupons Code...'),
      'operations' => [],
      'init_message' => t('Importing'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message'    => t('An error occurred during processing'),
      'finished' => '\Drupal\allianz_coupon_bulk_import\CouponBulkImport::bulkimportFinishedCallback',
    ];
    $duplicate_code = [];
    while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
      if ($count > 0) {
        $coupon_code = $getData[0];
        if (!in_array($coupon_code, $duplicate_code)) {
          $duplicate_code[] = $coupon_code;
          // Get db connection.
          $connection = Database::getConnection();
          // Select query to check duplicate coupon code.
          $query = $connection->select('paragraph__field_coupon_code', 'c')
            ->fields('c', ['entity_id'])
            ->condition('c.field_coupon_code_value', $coupon_code, '=')
            ->execute();
          $results = $query->fetchAssoc();
          if (empty($results)) {
            $batch['operations'][] = [
              '\Drupal\allianz_coupon_bulk_import\CouponBulkImport::bulkimport',
              [$count, $getData, $coupon_id, $imported_rows, $total_rows],
            ];
            $imported_rows++;
          }
          else {
            \Drupal::logger('coupon_bulk_import')->debug('<pre>Duplicate coupon code :' . $coupon_code . '</pre>');
          }
        }
      }
      $count++;
    }
    batch_set($batch);
  }

}
