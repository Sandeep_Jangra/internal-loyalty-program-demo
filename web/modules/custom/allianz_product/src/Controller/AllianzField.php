<?php

namespace Drupal\allianz_product\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductAttributeValue;
use Drupal\file\Entity\File;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\node\Entity\Node;
use Drupal\image\Entity\ImageStyle;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * AllianzField.
 */
class AllianzField extends ControllerBase {


  /**
  * Function used to get field value
  */
  public function getFieldValue($field_key,$entity,$type){
  // Getting field type
  $field_type = $entity->get($field_key)->getFieldDefinition()->getType();
  switch ($field_type) {
    case "image":
      $field_value = $entity->$field_key->getValue();
      foreach ($field_value as $key => $value) {
        $file_id = $value['target_id'];
        $thumbnail_image_style = 'thumbnail_64';
        if($entity->bundle() == 'voucher'){
          $src_image_style = 'voucher';
        }
        else {
          $src_image_style = 'product';
        }
        $output[$key] = $this->get_image_url($file_id, $src_image_style, $thumbnail_image_style);
        $output[$key]['alt'] = $value['alt'];
        $output[$key]['title'] = $value['title'];
      }
      break;

    case "media":
      $field_value = $entity->$field_key->getValue();
      foreach ($field_value as $key => $value) {
        $file_id = $value['target_id'];
        $output[] = $this->get_image_url($file_id, 'none', 'thumbnail');
      }
      break;

    case "file":
      $field_value = $entity->$field_key->getValue();
      foreach ($field_value as $key => $value) {
        $file_id = $value['target_id'];
        $output = $this->get_file_url($file_id);
      }
      break;    

    case "entity_reference":
      $entity_type = $entity->get($field_key)->getFieldDefinition()->getSettings()['target_type'];
      if ($entity_type == 'taxonomy_term') {
        $tid = $entity->$field_key->target_id;
        $output[0]['value'] = $this->get_term_name($tid);
        if ($type == 'variation') {
          // Setting Yes/No, Have you to change variation in front end based on this field or not
          $output[0]['group'] = 'no';
        }
      } 
      elseif ($entity_type == 'user') {
        $tid = $entity->$field_key->target_id;
        $output[0]['value'] = $this->get_user_name($tid);
        if ($type == 'variation') {
          // Setting Yes/No, Have you to change variation in front end based on this field or not
          $output[0]['group'] = 'no';
        }
      } 
      elseif ($entity_type == 'node') {
        $tid = $entity->$field_key->target_id;
        $output[0]['value'] = $this->get_node_name($tid);
        if ($type == 'variation') {
          // Setting Yes/No, Have you to change variation in front end based on this field or not
          $output[0]['group'] = 'no';
        }
      } 
      elseif ($entity_type == 'commerce_product_attribute_value') {
        $tid = $entity->$field_key->target_id;
        $output = $this->getAttributeName($tid);
      } 
      else {
        $output = '';
      }
      break;

    case "entity_reference_revisions":
      $entity_type = $entity->get($field_key)->getFieldDefinition()->getSettings()['target_type'];
      if ($entity_type == 'paragraph') {
        $field_value = $entity->$field_key->getValue();
        if (!empty($field_value)) {
          foreach ($field_value as $key => $value) {
            $tid = $value['target_id'];
            $output[] = $this->get_paragraph_data($tid);
          }
        }
        if ($type == 'variation') {
          // Setting Yes/No, Have you to change variation in front end based on this field or not
          $output[0]['group'] = 'no';
        }
      } 
      else {
        $output = '';
      }
      break; 

    default:
      $output = '';

  }
  return $output;
  }
  /**
   * Function used to get term name based on term id.
   */
  public function getTermName($tid) {
    // Load term.
    $term = Term::load($tid);
    if (!empty($term)) {
      $name = $term->getName();
    }
    else {
      $name = '';
    }
    return $name;
  }

  /**
   * Function used to get username based on user id.
   */
  public function gerUserName($uid) {
    // Load user.
    $account = User::load($uid);
    if (!empty($account)) {
      $username = $account->getUsername();
    }
    else {
      $username = '';
    }
    return $username;
  }

  /**
   * Function used to get node title based on node id.
   */
  public function getNodeName($nid) {
    // Load node.
    $node = Node::load($nid);
    if (!empty($node)) {
      $title = $node->getTitle();
    }
    else {
      $title = '';
    }
    return $title;
  }

  /**
   * Function used to get product name based on product id.
   */
  public function getProductName($pid) {
    // Load product.
    $product = Product::load($pid);
    if (!empty($product)) {
      $title = $product->getTitle();
    }
    else {
      $title = '';
    }
    return $title;
  }

  /**
   * Function used to get product attribute name based on attribute target id.
   */
  public function getAttributeName($aid) {
    // Load product attribute.
    $product_attribute = ProductAttributeValue::load($aid);
    // Checking whether product attribute is empty or not.
    if (!empty($product_attribute)) {
      $title = $product_attribute->getName();
    }
    else {
      $title = '';
    }
    return $title;
  }

  /**
   * Function used to get image url and thumbnail url based on file id.
   */
  public function getImageUrl($file_id, $src_image_style, $thumbnail_image_style) {
    // Load file.
    $file = File::load($file_id);
    if (!empty($file)) {
      $uri = $file->getFileUri();
      $thumbnail_style = ImageStyle::load($thumbnail_image_style);
      $thumbnail_url = $thumbnail_style->buildUrl($uri);
      if ($src_image_style == 'none') {
        $src = Url::fromUri(file_create_url($uri))->toString();
      }
      else {
        $style = ImageStyle::load($src_image_style);
        $src_url = $style->buildUrl($uri);
        $src = Url::fromUri($src_url)->toString();
      }
      $url['src'] = $src;
      $url['thumbnail'] = Url::fromUri($thumbnail_url)->toString();
    }
    else {
      $url = '';
    }
    return $url;
  }

  /**
   * Function used to get image style url based on file id.
   */
  public function getImageStyleUrl($file_id, $image_style) {
    // Load file.
    $file = File::load($file_id);
    if (!empty($file)) {
      $uri = $file->getFileUri();
      // Load thumbnail image style.
      $style = ImageStyle::load($image_style);
      $image_url = $style->buildUrl($uri);
      $url['src'] = Url::fromUri($image_url)->toString();
    }
    else {
      $url = '';
    }
    return $url;
  }

  /**
   * Function used to get file url like pdf, docs etc based on file id.
   */
  public function getFileUrl($file_id) {
    // Load file.
    $file = File::load($file_id);
    if (!empty($file)) {
      $uri = $file->getFileUri();
      $url['url'] = Url::fromUri(file_create_url($uri))->toString();
      $url['name'] = $file->getFilename();
    }
    else {
      $url = '';
    }
    return $url;
  }

  /**
   * Function used to get paragraph detail based on paragraph target id.
   */
  public function getParagraphData($target_id) {
    $paragraph = Paragraph::load($target_id);
    if (!empty($paragraph)) {
      if (!empty($paragraph->field_title)) {
        $output['title'] = $paragraph->field_title->value;
      }
      if (!empty($paragraph->field_description)) {
        $output['description'] = $paragraph->field_description->value;
      }
    }
    else {
      $output = '';
    }
    return $output;
  }

}
