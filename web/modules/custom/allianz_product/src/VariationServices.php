<?php

/**
 * @file providing the service that return variation attribute field data.
 */

namespace Drupal\allianz_product;

use Drupal\allianz_product\Controller\AllianzField;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_product\Entity\ProductAttribute;

/**
 * Class VariationServices.
 */
class VariationServices {

  /**
   * Constructs a new VariationServices object.
   */
  public function __construct() {

  }

  /**
	 * Get default variation data based on variation id.
	 */
  public function getVariation($variation_ids,$variation_id,$product_id,$variatin_parameter) {
  	$default_variation = ProductVariation::load($variation_id); // Load product variation
		$allianz_field = new AllianzField(); // create AllianzField class object 
		// get master field key of default variation
  	foreach ($default_variation->toArray() as $field_key => $variation_value) {
      $entity_type = $default_variation->$field_key->getFieldDefinition()->getSettings()['target_type'];
      if ($entity_type == 'commerce_product_attribute_value') {  
        	$target_id = $default_variation->$field_key->target_id;
        	$attribute_key = $field_key; 
        	break;
      }
    }
    // getVariationMasterField used to get all value of variation master field 
    $output = $this->getVariationMasterField($variation_ids,$attribute_key);
    // Check if product url contains parameters or not
    if ($variatin_parameter != null) {
    	$target_id = $variatin_parameter[$attribute_key]; 
    }
    // Entity query to get default variation field value
    if(!empty($target_id)) {
	    $query = \Drupal::entityQuery('commerce_product_variation');
	    $query->condition('status', 1);
	    $query->condition('product_id', $product_id);
	    if ($variatin_parameter != null) {
         $query->condition($attribute_key, $target_id);
	    }
	    else {
	    	$query->condition($attribute_key, $target_id);
	    }
	    $query->accessCheck(false);
	    $variation_id_array = $query->execute();
	  	$attribut_key_array = [];
	  	foreach ($variation_id_array as $key => $variation_id) {
		  	$variation = ProductVariation::load($variation_id); // Load product variation
		  	// Array contains those field that needs to be skip
			  $field_key_array = ['uuid','langcode','uid','created','changed','default_langcode','stores','variation_id','type','status','product_id','sku','list_price','price','field_points_price','field_delivery_time','field_product_image','field_stock','title'];
			  foreach ($variation->toArray() as $field_key => $variation_value) {
			  	// Check if master field is not equal to other field
			  	if ($field_key != $attribute_key) {
			      if (!in_array($field_key, $field_key_array)) {
			        // Calling $allianz_field->getFieldValue function to get reference field value
			        $entity_type = $variation->get($field_key)->getFieldDefinition()->getSettings()['target_type'];
			        if ($entity_type == 'commerce_product_attribute_value') {
			          $field_label = $variation->$field_key->getFieldDefinition()->getLabel();
			          if (!array_key_exists($field_key,$output['variation'])) {
			          	$output['variation'][$field_key]['label'] = $field_label;
			          	// get attribute id from field key
			        	  $attribute_id = str_replace('attribute_', '', $field_key);
			            $attribute = ProductAttribute::load($attribute_id); // Load product attribute
			        	  $output['variation'][$field_key]['type'] = $attribute->getElementType();
			        	  $output['variation'][$field_key]['selection_type'] = 'slave';
			          }
			          // getFieldValue used to get field value
				        $field_value = $allianz_field->getFieldValue($field_key,$variation,'variation');
				        if (!empty($field_value)) {
				        	$field_value_key = key($field_value);
				        	$tid = $variation->$field_key->target_id;
				        	if (!in_array($tid, $attribut_key_array[$field_key])) {
				        		$stock = $variation->field_stock->value;
				        		if ($stock > 0) {
				        			$disable = false;
				        		}
				        		else {
				        			$disable = true;
				        		}
				            $output['variation'][$field_key]['value'][] = ['tid' => $tid, 'label' => $field_value, 'disable' => $disable];
				            $attribut_key_array[$field_key][] = $tid;
				          }
				        }
			        }
			      }
			    } 
		    }
	    }
    }

	  return $output;
	}
  
  /*
   * getVariationMasterField used to get data for variation master field
   */
  public function getVariationMasterField($variation_ids,$attribute_key) {
  	$allianz_field = new AllianzField(); // create AllianzField class object 
  	foreach ($variation_ids as $key => $variation_id) {
	  	$variation = ProductVariation::load($variation_id); // Load product variation
      foreach ($variation->toArray() as $field_key => $variation_value) {
      	// Chek if field key is equal to master key
		  	if ($field_key == $attribute_key) {
	        $entity_type = $variation->get($field_key)->getFieldDefinition()->getSettings()['target_type'];
	        // Check if field entity type is of attribute
	        if ($entity_type == 'commerce_product_attribute_value') {
	          $field_label = $variation->$field_key->getFieldDefinition()->getLabel();
	          if (!array_key_exists($field_key,$output['variation'])) {
	          	$output['variation'][$field_key]['label'] = $field_label;
	          	// get attribute id from field key
	        	  $attribute_id = str_replace('attribute_', '', $field_key);
	            $attribute = ProductAttribute::load($attribute_id); // Load product attribute
	        	  $output['variation'][$field_key]['type'] = $attribute->getElementType();
	        	  $output['variation'][$field_key]['selection_type'] = 'master';
	          }
		        $field_value = $allianz_field->getFieldValue($field_key,$variation,'variation');
		        if (!empty($field_value)) {
		        	$field_value_key = key($field_value);
		        	$tid = $variation->$field_key->target_id;
		        	if (!in_array($tid, $attribut_key_array[$field_key])) {
		            $output['variation'][$field_key]['value'][] = ['tid' => $tid, 'label' => $field_value];
		            $attribut_key_array[$field_key][] = $tid;
		          }
		        }
	        }
	      }
	    }
    }
    return $output;
  }

}