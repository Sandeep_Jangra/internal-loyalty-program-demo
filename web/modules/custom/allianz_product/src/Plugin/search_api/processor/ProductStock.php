<?php

namespace Drupal\allianz_product\Plugin\search_api\processor;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;
use Drupal\commerce_product\Entity\Product;

/**
 * Adds the item's URL to the indexed data.
 *
 * @SearchApiProcessor(
 *   id = "product_stock",
 *   label = @Translation("Product stock Field"),
 *   description = @Translation("Custom product stock field"),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class ProductStock extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Product Stock Field'),
        'description' => $this->t('Custom product stock field'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['product_stock'] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    $entity_type = $item->getDatasource()->getEntityTypeId($item->getOriginalObject());
    $entity_id = $item->getDatasource()->getItemId($item->getOriginalObject());
    $product_id = explode(":", $entity_id)[0];
    $product = Product::load($product_id);
    $product_stock = 0;
    if ($product->bundle() != 'voucher' || $product->bundle() != 'coupon') {
      // Get default variation ids.
      $variations = $product->getVariations();
      if (!empty($variations)) {
        foreach ($variations as $key => $variation) {
          $product_stock = $product_stock + $variation->field_stock->value;
        }
      }
    }
    $fields = $this->getFieldsHelper()->filterForPropertyPath($item->getFields(), NULL, 'product_stock');
    foreach ($fields as $field) {
      $field->addValue($product_stock);
    }
  }

}
