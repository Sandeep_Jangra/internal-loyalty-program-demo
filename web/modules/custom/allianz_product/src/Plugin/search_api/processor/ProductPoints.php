<?php

namespace Drupal\allianz_product\Plugin\search_api\processor;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;
use Drupal\commerce_product\Entity\Product;

/**
 * Adds the item's URL to the indexed data.
 *
 * @SearchApiProcessor(
 *   id = "product_points",
 *   label = @Translation("Product points Field"),
 *   description = @Translation("Custom product points field"),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class ProductPoints extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Product Points Field'),
        'description' => $this->t('Custom product points field'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['product_points'] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    $entity_type = $item->getDatasource()->getEntityTypeId($item->getOriginalObject());
    $entity_id = $item->getDatasource()->getItemId($item->getOriginalObject());
    $product_id = explode(":", $entity_id)[0];
    $product = Product::load($product_id);
    $product_points = 0;
    if ($product->bundle() != 'voucher' || $product->bundle() != 'coupon') {
      // Get default variation ids.
      $variations = $product->getVariations();
      if (!empty($variations)) {
        $product_points = round($variations[0]->getPrice()->getNumber());
      }
    }
    $fields = $this->getFieldsHelper()->filterForPropertyPath($item->getFields(), NULL, 'product_points');
    foreach ($fields as $field) {
      $field->addValue($product_points);
    }
  }

}
