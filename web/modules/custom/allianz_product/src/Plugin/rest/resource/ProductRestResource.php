<?php

namespace Drupal\allianz_product\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\allianz_product\Controller\AllianzField;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;

/**
* Provides a resource to get view modes by entity and bundle.
*
* @RestResource(
*   id = "product_rest_resource",
*   label = @Translation("Product rest resource"),
*   uri_paths = {
*     "canonical" = "/api/v1/product/{product_title}"
*   }
* )
*/
class ProductRestResource extends ResourceBase {

  /**
  * A current user instance.
  *
  * @var \Drupal\Core\Session\AccountProxyInterface
  */
  protected $currentUser;

  /**
  * Constructs a new ProductRestResource object.
  *
  * @param array $configuration
  *   A configuration array containing information about the plugin instance.
  * @param string $plugin_id
  *   The plugin_id for the plugin instance.
  * @param mixed $plugin_definition
  *   The plugin implementation definition.
  * @param array $serializer_formats
  *   The available serialization formats.
  * @param \Psr\Log\LoggerInterface $logger
  *   A logger instance.
  * @param \Drupal\Core\Session\AccountProxyInterface $current_user
  *   A current user instance.
  */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
  * {@inheritdoc}
  */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_product'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
      );
  }

  /**
  * Responds to GET requests.
  *
  * @param string $product_title
  *
  * @return \Drupal\rest\ResourceResponse
  *   The HTTP response object.
  *
  * @throws \Symfony\Component\HttpKernel\Exception\HttpException
  *   Throws exception expected.
  */
  public function get($product_title) {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $jwt = $authorization_token;
    $token = $transcoder->decode($jwt);
    $account_uid = $token->getPayload()->drupal->uid;
    $connection = Database::getConnection(); // get db connection
    // select query to get requested user token for authentication
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', array('uid','jwt_token'))
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if ((!empty($results) && $results['jwt_token'] == $jwt)) {
      // Getting product id from product url alias
      $path = \Drupal::service('path.alias_manager')->getPathByAlias('/product/'.$product_title);

      if (preg_match('/product\/(\d+)/', $path, $matches)) {
        // $field_key_array contains list of field, that needs to be skip for this endpoint
        $product_id = $matches[1]; 
        $product = Product::load($product_id); // Load product
        if (!empty($product) && $product->isPublished() == 1) {
          try {
            if ($product->bundle() != 'coupon' && $product->bundle() != 'voucher') { // Checking if product bundle is not from coupon and voucher
              $allianz_field = new AllianzField(); // create AllianzField class object 
              $response['status'] = 'success';
              $response['data']['product']['title'] = $product->getTitle();
              $response['data']['product']['slug'] = '/product/'.$product_title;
              $response['data']['product']['summary'] = $product->field_summary->value;
              $product_descriptions = $product->field_description->getValue();
              if (!empty($product_descriptions)) {
                foreach ($product_descriptions as $key => $paragraph_id) {
                  $product_description = $allianz_field->getParagraphData($paragraph_id['target_id']);
                  $response['data']['product']['description'][] = $product_description;
                }
              }
              $response['data']['product']['flap'] = $product->field_flap->value;
              if ($product->field_flap->value == 'availability') {
                $response['data']['product']['availability_date_from'] = strtotime($product->field_availability_date->value);
                $response['data']['product']['availability_date_to'] = strtotime($product->field_availability_date->end_value);
              }
              // Get all product varations ids
              $variation_ids = $product->getVariationIds();
              $variatin_parameter = null;
              if (!empty($variation_ids)) { // Check if variations id not empty
                if (!empty($_GET)) { // Check if url contains parameter or not
                  $variatin_parameter = $_GET;
                  // Get variation id if url contains parameters
                  $query = \Drupal::entityQuery('commerce_product_variation');
                  $query->condition('status', 1);
                  $query->condition('product_id', $product_id);
                  foreach ($_GET as $field_key => $field_value) {
                    $query->condition($field_key, $field_value);
                  }
                  $query->accessCheck(false);
                  $variation_id = $query->execute();
                  $variation_id = reset($variation_id);
                } 
                else {
                  $variation_id = $variation_ids[0];
                }
                // Call service to get current product and default variation data
                $default_variation_service = \Drupal::service('allianz_product.default_variation');
                $default_variation_data = $default_variation_service->getVariationData($variation_id, $account_uid);
                if (!empty($default_variation_data)) {
                  $response['data']['product'] = array_merge($response['data']['product'],$default_variation_data);
                }
                // Call service to get variation field data
                $variation_service = \Drupal::service('allianz_product.variations_data');
                $variation_data = $variation_service->getVariation($variation_ids,$variation_id,$product_id,$variatin_parameter);
                if (!empty($variation_data)) {
                  $response['data'] = array_merge($response['data'],$variation_data);
                } 
              }
              $response['data']['discount'] = $product->field_discount->value;
              $error = 200;
            }
          }
          catch (RequestException $e) {
            $response['status'] = 'failure';
            $response['error'] = 'Something went wrong!';
            $error = 500;
          } 
        }
        else {
          // Return 404 if requested product is unpublished
          $response['status'] = 'failure';
          $response['error'] = 'Requested product is unpublished.';
          $error = 500;
        }   
      } 
      else {
        // Return 404 if requested product url does not exists
        $response['status'] = 'failure';
        $response['error'] = 'Requested product does not exist.';
        $error = 404;
      }
    }
    else {
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }     
    $response = new ResourceResponse($response, $error);
    // disable api cache
    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);
    $response->addCacheableDependency($disable_cache);

    return $response;
  }

}