<?php

namespace Drupal\allianz_product\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\views\Views;

/**
 * Returns voucher category for filter.
 *
 * @RestResource(
 *   id = "punkte_einlosen_filter",
 *   label = @Translation("Punkte Einlosen Filter"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/punkte-einlosen/filter"
 *   }
 * )
 */
class PunkteEinlosenFilter extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new PunkteEinlosenFilter object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_custom'),
      $container->get('current_user')
      );
  }

  /**
   * Responds to GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get() {
    try {
      $args[] = 'all';
      // Get total physical product category wise.
      $product_view = Views::getView('physical_product_listing');
      $product_render_array = $product_view->buildRenderable('rest_export_1', $args);
      $product_rendered = \Drupal::service('renderer')->renderRoot($product_render_array);
      $product_json_string = $product_rendered->jsonSerialize();
      $product_json_array = json_decode($product_rendered, TRUE);
      foreach ($product_json_array as $product_key => $product_value) {
        $category_id = $product_value['category_id'];
        if ($product_value['display'] == 1) {
          if (array_key_exists($category_id, $category_count)) {
            $category_count[$category_id] = $category_count[$category_id] + 1;
          }
          else {
            $category_count[$category_id] = 1;
          }
        }
      }
      // Database connection.
      $connection = \Drupal::database();
      // Quesry to get all used voucher category ids.
      $query = $connection->select('commerce_product__field_category', 'pc');
      $query->fields('pc', ['field_category_target_id']);
      $query->distinct();
      $query->innerJoin('commerce_product_field_data', 'pd', 'pd.product_id = pc.entity_id');
      $query->condition('pd.status', 1, '=');
      $result = $query->execute()->fetchAll();
      if (!empty($result)) {
        $response['status'] = 'success';
        $count = 0;
        foreach ($result as $key => $value) {
          $term_id = $value->field_category_target_id;
          // Load taxonomy term.
          $term = Term::load($term_id);
          // Check if vocabulary id is digital category.
          if (!empty($term) && $term->getVocabularyId() == 'physical_category') {
            if (array_key_exists($term_id, $category_count)) {
              $cat_count = $category_count[$term_id];
            }
            else {
              $cat_count = 0;
            }
            $response['data']['category'][$count]['id'] = $term_id;
            $response['data']['category'][$count]['label'] = $term->getName();
            $response['data']['category'][$count]['count'] = $cat_count;
            $count++;
          }
        }
        $error = 200;
      }
      else {
        // Return 404 if category does not exist for voucher.
        $response['status'] = 'failure';
        $response['error'] = 'Something went wrong!';
        $error = 404;
      }
    }
    catch (RequestException $e) {
      $response['status'] = 'failure';
      $response['error'] = 'Something went wrong!';
      $error = 500;
    }
    $response = new ResourceResponse($response, $error);
    // Disable api cache.
    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);
    $response->addCacheableDependency($disable_cache);

    return $response;
  }

}
