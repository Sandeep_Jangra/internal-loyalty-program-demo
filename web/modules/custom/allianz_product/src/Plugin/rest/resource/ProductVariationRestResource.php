<?php

namespace Drupal\allianz_product\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\allianz_product\Controller\AllianzField;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "product_variation_rest_resource",
 *   label = @Translation("Product Variation Rest Resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/product-variation/{product_url}"
 *   }
 * )
 */
class ProductVariationRestResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new ProductVariationRestResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_product'),
      $container->get('current_user')
      );
  }

  /**
   * Responds to GET requests.
   *
   * @param string $payload
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get($product_url) { 
    try {
      // Getting product id from product url alias
      $path = \Drupal::service('path.alias_manager')->getPathByAlias('/product/'.$product_url);
      if(preg_match('/product\/(\d+)/', $path, $matches)) {
        // $field_key_array contains list of field, that needs to be skip for this endpoint
        $product_id = $matches[1]; 
        $product = Product::load($product_id); // Load product
        if(!empty($product)){
          if($product->bundle() != 'coupon' && $product->bundle() != 'voucher'){ // Checking if product bundle is not from coupon and voucher
            $allianz_field = new AllianzField(); // create AllianzField class object 
            $response['status'] = 'success';
            $response['data']['product']['product_id'] = $product_id;
            $response['data']['product']['title'] = $product->getTitle();
            $response['data']['product']['slug'] = '/product/'.$product_url;
            $response['data']['product']['summary'] = $product->field_summary->value;
            $product_descriptions = $product->field_description->getValue();
            if(!empty($product_descriptions)){
              foreach ($product_descriptions as $key => $paragraph_id) {
                $product_description = $allianz_field->get_paragraph_data($paragraph_id['target_id']);
                $response['data']['product']['description'][] = $product_description;
              }
            }

            $variation_ids = $product->getVariationIds();
            if(!empty($variation_ids)){
              $query = \Drupal::entityQuery('commerce_product_variation');
              $query->condition('status', 1);
              $query->condition('product_id', $product_id);
              if(!empty($_GET)){
                foreach ($_GET as $field_key => $field_value) {
                  $query->condition($field_key, $field_value);
                }
              }
              $query->accessCheck(false);
              $variation_id = $query->execute();
              if(!empty($variation_id)){
                $variation_id = reset($variation_id);
                $default_variation_service = \Drupal::service('allianz_product.default_variation');
                $default_variation_data = $default_variation_service->getVariationData($variation_id);
                if(!empty($default_variation_data)){
                  $response['data']['product'] = array_merge($response['data']['product'],$default_variation_data);
                }
                
                $variation_service = \Drupal::service('allianz_product.variations_data');
                $variation_data = $variation_service->getVariation($variation_ids);
                if(!empty($variation_data)){
                  $response['data'] = array_merge($response['data'],$variation_data);
                }
              } 
            }
            $error = 200;
          } 
        }   
      } else {
        // Return 404 if requested product url does not exists
        $response['status'] = 'failure';
        $response['error'] = 'Requested product does not exist.';
        $error = 404;
      }
    }
    catch (RequestException $e) {
      $response['status'] = 'failure';
      $response['error'] = 'Something went wrong!';
      $error = 500;
    }

    return new ResourceResponse($response, $error);
  }

}
