<?php

namespace Drupal\allianz_product\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\commerce_product\Entity\Product;

/**
 * Processes Product Expiry Unpublish Tasks.
 *
 * @QueueWorker(
 *   id = "product_unpublish_queue_processor",
 *   title = @Translation("Task Worker: Product Expiry Unpublish"),
 *   cron = {"time" = 10}
 * )
 */

class ProductUnpublish extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($response) {
    if (!empty($response)) {
      $product = Product::load($response);
      $product->setUnpublished(TRUE);
      $product->save();
    }
  }
  
}