<?php

namespace Drupal\allianz_product\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\commerce_product\Entity\Product;

/**
 * Processes Product start date publish Tasks.
 *
 * @QueueWorker(
 *   id = "product_publish_queue_processor",
 *   title = @Translation("Task Worker: Product Start date Publish"),
 *   cron = {"time" = 10}
 * )
 */

class ProductPublish extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($response) {
    if (!empty($response)) {
      $product = Product::load($response);
      $product->setPublished(TRUE);
      $product->save();
    }
  }
  
}