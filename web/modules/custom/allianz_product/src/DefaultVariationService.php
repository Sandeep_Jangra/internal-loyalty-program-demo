<?php

/**
 * @file providing the service that return default physical product vaiation data.
 */

namespace Drupal\allianz_product;

use Drupal\allianz_product\Controller\AllianzField;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Database\Database;

/**
 * Class DefaultVariationService.
 */
class DefaultVariationService
{

	/**
	 * Constructs a new DefaultVariationService object.
	 */
	public function __construct()
	{

	}

	/**
	 * Get default variation data based on variation id.
	 *
	 * @param string $variation_id
	 *   Default variation id
	 * @return array $output                       
	 *   return array response
	 */
	public function getVariationData($variation_id, $account_uid)
	{
    $variation = ProductVariation::load($variation_id); // Load default variation
    $allianz_field = new AllianzField(); // create AllianzField class object 
    $variation_images = $variation->field_product_image->getValue();
    if (!empty($variation_images)) {
    	foreach ($variation_images as $key => $value) {
    		$file_id = $value['target_id'];
    		$output['product_image'][$key] = $allianz_field->getImageUrl($file_id, 'voucher', 'thumbnail_64');
    		$output['product_image'][$key]['alt'] = $value['alt'];
    		$output['product_image'][$key]['title'] = $value['title'];
    	}
    }
    $output['price']['points_price'] = round($variation->getPrice()->getNumber(), 2);
    $output['price']['currency'] = $variation->getPrice()->getCurrencyCode();
    $output['delivery_time'] = $variation->field_delivery_time->value;
    $output['stock'] = $variation->field_stock->value;
    // select query to get item is flagged or not by requested user
    $connection = Database::getConnection(); // get db connection
    $query = $connection->select('flagging', 'c')
      ->fields('c', array('id'))
      ->condition('c.entity_type', 'commerce_product_variation', '=')
      ->condition('c.entity_id', $variation_id, '=')
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if (!empty($results)) {
      $output['flagged'] = "True";
    }
    else {
      $output['flagged'] = "False";
    }
    $output['product_id'] = $variation_id;
    $field_key_array = ['uuid','langcode','uid','created','changed','default_langcode','stores','variation_id','type','status','product_id','sku','list_price','price','field_points_price','field_delivery_time','field_product_image','field_stock','title'];
    // get default variation extra info field value except attributes field 
    foreach ($variation->toArray() as $field_key => $variation_value) {
    	if (!in_array($field_key, $field_key_array)) {
    		$entity_type = $variation->get($field_key)->getFieldDefinition()->getSettings()['target_type'];
    		if ($entity_type != 'commerce_product_attribute_value') {
    			$field_value = $allianz_field->getFieldValue($field_key, $variation, 'variation');
    			if (!empty($field_value)) {
    				$output['extra_info'][$field_key] = $field_value;
    				$output['extra_info'][$field_key]['type'] = $entity_type;
    			} 
    			else {
    				$field_type = $variation->get($field_key)->getFieldDefinition()->getType();
    				$output['extra_info'][$field_key] = $variation_value[0];
    				$output['extra_info'][$field_key]['type'] = $field_type;
    			}
    		}
    	}
    }
    return $output;
  }

}