<?php

/**
 * @file
 * Contains wirecard_errors.page.inc.
 *
 * Page callback for Wirecard Error Logs entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Wirecard Error Logs templates.
 *
 * Default template: wirecard_errors.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_wirecard_errors(array &$variables) {
  // Fetch WirecardErrors Entity Object.
  $wirecard_errors = $variables['elements']['#wirecard_errors'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
