<?php

/**
 * @file
 * Contains allianz_checkout.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\Core\Database\Database;

/**
 * Implements hook_help().
 */
function allianz_checkout_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the allianz_checkout module.
    case 'help.page.allianz_checkout':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Custom module for checkout functionality') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_views_query_alter().
 */
function allianz_checkout_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {
  // Alter wishlist iew to get item for specific user using JWT token.
  if ($view->id() == 'activities' && $view->getDisplay()->display['id'] == 'rest_export_1') {
    // Traverse through the 'where' part of the query.
    foreach ($query->where as &$condition_group) {
      foreach ($condition_group['conditions'] as &$condition) {
        // Chang the condition to filter on wishlist customer id.
        if ($condition['field'] == 'commerce_order.uid = :commerce_order_uid') {
          $request = Drupal::request();
          $authorization_token = trim(str_replace('Bearer ', '', $request->headers->get('Authorization')));
          // Load jwt transcoder object through services.
          $transcoder = \Drupal::service('jwt.transcoder');
          $token = $transcoder->decode($authorization_token);
          $account_uid = $token->getPayload()->drupal->uid;
          $condition['value'][':commerce_order_uid'] = $account_uid;
        }
      }
    }
  }

  // Get current user role
  $current_user = \Drupal::currentUser();
  $user_id = $current_user->id();
  $roles = $current_user->getRoles();
  if (in_array('avp_partners', $roles)) {
    if ($view->id() == 'commerce_orders' && $view->getDisplay()->display['id'] == 'page_1') {
      // Store filter counts.
      $count = count($query->where[1]['conditions']);
      $query->where[1]['conditions'][$count]['field'] = 'commerce_order.uid';
      $query->where[1]['conditions'][$count]['value'] = $user_id;
    }
    if ($view->id() == 'commerce_carts' && $view->getDisplay()->display['id'] == 'page_1') {
      // Store filter counts.
      $count = count($query->where[1]['conditions']);
      $query->where[1]['conditions'][$count]['field'] = 'commerce_order.uid';
      $query->where[1]['conditions'][$count]['value'] = $user_id;
    }
    // Back in stock view.
    if ($view->id() == 'voucher_stock_alert' && $view->getDisplay()->display['id'] == 'page_2') {
      // Store filter counts.
      $count = count($query->where[1]['conditions']);
      $query->where[1]['conditions'][$count]['field'] = 'stock_alert__field_user_id.field_user_id_target_id';
      $query->where[1]['conditions'][$count]['value'] = $user_id;
    }
    // Coupon Purchase Lifetime Limit view.
    if ($view->id() == 'coupon_purchase_lifetime_limit' && $view->getDisplay()->display['id'] == 'page_2') {
      // Store filter counts.
      $count = count($query->where[1]['conditions']);
      $query->where[1]['conditions'][$count]['field'] = 'coupon_purchase_lifetime__field_user_id.field_user_id_target_id';
      $query->where[1]['conditions'][$count]['value'] = $user_id;
    }
    // Session Level Checkout view.
    if ($view->id() == 'session_level_checkout' && $view->getDisplay()->display['id'] == 'page_2') {
      // Store filter counts.
      $count = count($query->where[1]['conditions']);
      $query->where[1]['conditions'][$count]['field'] = 'session_level_checkout__field_user_id.field_user_id_target_id';
      $query->where[1]['conditions'][$count]['value'] = $user_id;
    }
    // Order Dashboard view.
    if ($view->id() == 'order_dashboard' && $view->getDisplay()->display['id'] == 'page_1') {
      // Store filter counts.
      $count = count($query->where[1]['conditions']);
      $query->where[1]['conditions'][$count]['field'] = 'commerce_order.uid';
      $query->where[1]['conditions'][$count]['value'] = $user_id;
    }
    // Wirecard Transactions view.
    if ($view->id() == 'wirecard_transactions' && $view->getDisplay()->display['id'] == 'page_1') {
      // Store filter counts.
      $count = count($query->where[1]['conditions']);
      $query->where[1]['conditions'][$count]['field'] = 'wirecard_errors.user_id';
      $query->where[1]['conditions'][$count]['value'] = $user_id;
    }
  }
}

