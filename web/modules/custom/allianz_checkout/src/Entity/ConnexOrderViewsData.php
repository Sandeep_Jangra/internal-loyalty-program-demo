<?php

namespace Drupal\allianz_checkout\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Connex order entities.
 */
class ConnexOrderViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
