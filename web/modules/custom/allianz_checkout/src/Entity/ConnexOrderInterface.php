<?php

namespace Drupal\allianz_checkout\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Connex order entities.
 *
 * @ingroup allianz_checkout
 */
interface ConnexOrderInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Connex order name.
   *
   * @return string
   *   Name of the Connex order.
   */
  public function getName();

  /**
   * Sets the Connex order name.
   *
   * @param string $name
   *   The Connex order name.
   *
   * @return \Drupal\allianz_checkout\Entity\ConnexOrderInterface
   *   The called Connex order entity.
   */
  public function setName($name);

  /**
   * Gets the Connex order creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Connex order.
   */
  public function getCreatedTime();

  /**
   * Sets the Connex order creation timestamp.
   *
   * @param int $timestamp
   *   The Connex order creation timestamp.
   *
   * @return \Drupal\allianz_checkout\Entity\ConnexOrderInterface
   *   The called Connex order entity.
   */
  public function setCreatedTime($timestamp);

}
