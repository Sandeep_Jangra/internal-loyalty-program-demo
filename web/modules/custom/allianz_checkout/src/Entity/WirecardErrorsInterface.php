<?php

namespace Drupal\allianz_checkout\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Wirecard Error Logs entities.
 *
 * @ingroup allianz_checkout
 */
interface WirecardErrorsInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Wirecard Error Logs name.
   *
   * @return string
   *   Name of the Wirecard Error Logs.
   */
  public function getName();

  /**
   * Sets the Wirecard Error Logs name.
   *
   * @param string $name
   *   The Wirecard Error Logs name.
   *
   * @return \Drupal\allianz_checkout\Entity\WirecardErrorsInterface
   *   The called Wirecard Error Logs entity.
   */
  public function setName($name);

  /**
   * Gets the Wirecard Error Logs creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Wirecard Error Logs.
   */
  public function getCreatedTime();

  /**
   * Sets the Wirecard Error Logs creation timestamp.
   *
   * @param int $timestamp
   *   The Wirecard Error Logs creation timestamp.
   *
   * @return \Drupal\allianz_checkout\Entity\WirecardErrorsInterface
   *   The called Wirecard Error Logs entity.
   */
  public function setCreatedTime($timestamp);

}
