<?php

namespace Drupal\allianz_checkout;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Connex order entity.
 *
 * @see \Drupal\allianz_checkout\Entity\ConnexOrder.
 */
class ConnexOrderAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\allianz_checkout\Entity\ConnexOrderInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished connex order entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published connex order entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit connex order entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete connex order entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add connex order entities');
  }

}
