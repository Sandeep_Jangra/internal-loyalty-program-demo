<?php

namespace Drupal\allianz_checkout\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\profile\Entity\Profile;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\commerce_wishlist\Entity\WishlistItem;

/**
 * Store the user delivery and billing information.
 *
 * @RestResource(
 *   id = "delivery_information",
 *   label = @Translation("Add lieferanschrift"),
 *   uri_paths = {
 *     "create" = "/api/v1/lieferanschrift"
 *   }
 * )
 */
class DeliveryInformation extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new DeliveryInformation object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_checkout'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
      );
  }

  /**
   * Responds to POST requests.
   *
   * @param string $data
   *   Get data object on request.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post($data) {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $jwt = $authorization_token;
    $token = $transcoder->decode($jwt);
    $account_uid = $token->getPayload()->drupal->uid;
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if ((!empty($results) && $results['jwt_token'] == $jwt)) {
      if (!empty($data)) {
        $order = Order::load($data['order_id']);
        if (!empty($order) && $order->state->value != 'completed') {
          $billing_address = [
            'country_code' => 'DE',
            'given_name' => $data['first_name'],
            'family_name' => $data['last_name'],
            'address_line1' => $data['address'],
            'postal_code' => $data['postal_code'],
            'locality' => $data['locality'],
          ];
          $profile = Profile::create([
            'type' => 'customer',
            'uid' => $account_uid,
            'address' => $billing_address,
          ]);
          $shipping_address = [
            'country_code' => 'DE',
            'given_name' => $data['shipping_first_name'],
            'family_name' => $data['shipping_last_name'],
            'address_line1' => $data['shipping_address'],
            'postal_code' => $data['shipping_postal_code'],
            'locality' => $data['shipping_locality'],
          ];
          try {
            $profile->save();
            $account = User::load($account_uid);
            $member_id = $account->field_pc_member_id->value;
            $description['order_id'] = $data['order_id'];
            $description['billing_profile'] = $billing_address;
            $description['shipping_address'] = $shipping_address;
            $pc_service = \Drupal::service('allianz_prime_cloud.triggers');
            $date = date('Y-m-d');
            $pc_data['date'] = $date . 'T00:00:00.000Z';
            $pc_data['receiptId'] = $member_id;
            $pc_data['amount'] = round($order->getTotalPrice()->getNumber());
            $order_item_id = $order->getItems()[0]->id();
            $order_item = OrderItem::load($order_item_id);
            if (!empty($order_item)) {
              $variation = $order_item->getPurchasedEntity();
              $product_title = $variation->getTitle();
              $lp_desc['message'] = "Produkt gekauft " . $product_title;
              $lp_desc['order_detail'] = $description;
              $pc_data['description'] = json_encode($lp_desc);
              $query_params = ['type' => 'bonus'];
              $redeem_points = $pc_service->redeemPoints($member_id, $query_params, $pc_data);
              if ($redeem_points['error'] < 400) {
                $profile = Profile::load($profile->id());

                // Set customer billing profile.
                $order->setBillingProfile($profile);
                $order->set('field_shipping_address', $shipping_address);
                $order->set('state', 'completed');
                $order->save();
                // Remove product from wishlist
                // Load wishlist manager object through services.
                $wishlist_manager = \Drupal::service('commerce_wishlist.wishlist_manager');
                // Select query to get request user wishlist item count.
                $query = $connection->select('commerce_wishlist_item', 'wt');
                $query->fields('wt');
                $query->leftJoin('commerce_wishlist', 'w', 'w.wishlist_id = wt.wishlist_id');
                $query->condition('w.uid', $account_uid);
                $query->condition('wt.purchasable_entity', $variation->id());
                $results = $query->execute()->fetchAssoc();
                if (!empty($results)) {
                  $wishlist_item_id = $results['wishlist_item_id'];
                  $wishlist_item = WishlistItem::load($wishlist_item_id);
                  if (!empty($wishlist_item)) {
                    $wishlist = $wishlist_item->getWishlist();
                    $wishlist_manager->removeWishlistItem($wishlist, $wishlist_item);
                    $purchasable_entity = $wishlist_item->getPurchasableEntity();
                    $flag_service = \Drupal::service('flag');
                    $flag = $flag_service->getFlagById('wishlist');
                    // Flag an entity with a specific flag.
                    $flag_service->unflag($flag, $purchasable_entity);
                  }
                }
                // Order confirmation email to member.
                $warpit = \Drupal::service('warpit.client');
                $warpit_data = [
                  "senderToken" => "bae2566d-481a-4d7a-bca4-03df36f899f2",
                  "messageType" => "email",
                  "messageProviderId" => 1,
                  "templateId" => 1,
                  "users" => [[
                    "email" => $account->getEmail(),
                  ],
                  ],
                ];
                $status = $warpit->send($warpit_data);
                if ($status['succes'] != '1') {
                  // @todo
                  // If email trigger is not sent.
                }

                $response['status'] = 'success';
                $response['data']['order_id'] = $data['order_id'];
                $error = 200;
              }
              else {
                // Return 400 if member key does not exist on prime cloud.
                $response['status'] = 'failure';
                $response['error'] = 'Invalid member key!';
                $error = 400;
              }
            }
            else {
              // Return 500 if requested order item does not exist.
              $response['status'] = 'failure';
              $response['error'] = 'Something went wrong';
              $error = 500;
            }
          }
          catch (RequestException $e) {
            $response['status'] = 'failure';
            $response['error'] = 'Something went wrong!';
            $error = 500;
          }
        }
        else {
          // Return 500 if requested order does not exist.
          $response['status'] = 'failure';
          $response['error'] = 'Something went wrong!';
          $error = 500;
        }
      }
      else {
        // Return 500 if requested request is invalid.
        $response['status'] = 'failure';
        $response['error'] = 'Something went wrong!';
        $error = 500;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }

    $response = new ModifiedResourceResponse($response, $error);
    return $response;
  }

}
