<?php

namespace Drupal\allianz_checkout\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\user\Entity\User;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\allianz_product\Controller\AllianzField;
use Drupal\allianz_voucher_session\Entity\SessionLevelCheckout;
use Drupal\allianz_checkout\Entity\ConnexOrder;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\commerce_price\Price;
use Drupal\profile\Entity\Profile;
use Drupal\allianz_checkout\Entity\WirecardErrors;
use Drupal\commerce_order\Adjustment;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "checkout_success",
 *   label = @Translation("Checkout Success"),
 *   uri_paths = {
 *     "create" = "/api/v1/checkout/success"
 *   }
 * )
 */
class CheckoutSuccess extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new CheckoutSuccess object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_checkout'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param string $data
   *   Get data object on request.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function post($data) {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $jwt = $authorization_token;
    $token = $transcoder->decode($jwt);
    $account_uid = $token->getPayload()->drupal->uid;
    $account = User::load($account_uid);
    // Empty member_id case to be handled..
    $member_id = $account->field_pc_member_id->value;
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if ((!empty($results) && $results['jwt_token'] == $jwt)) {
      if (!empty($data) && !empty($data['payment_method']) && !empty($data['request_id'])) {
        $request_id = $data['request_id'];
        $payment_info = $payment_method_german = "";
        // Get transaction detail using request id.
        if ($data['payment_method'] == 'creditcard') {
          $merchant_id = $_ENV['WIRECARD_CCARD_MID'];
          $wirecard_username = $_ENV['WIRECARD_USERNAME_CCARD'];
          $wirecard_password = $_ENV['WIRECARD_PASSWORD_CCARD'];
          $payment_gateway_method = 'wirecard_credit_debit_card';
          $payment_info = "Ihre Kreditkarte wird mit dem oben genannte Rechnungsbetrag in den nächsten Tagen belastet.";
          $payment_method_german = "Kreditkarte";
        }
        elseif ($data['payment_method'] = 'sepadirectdebit') {
          $merchant_id = $_ENV['WIRECARD_SEPA_MID'];
          $wirecard_username = $_ENV['WIRECARD_USERNAME_SEPA'];
          $wirecard_password = $_ENV['WIRECARD_PASSWORD_SEPA'];
          $payment_gateway_method = 'wirecard_sepa_debit_card';
          $payment_info = "Der oben genannte Rechnungsbetrag wird in den nächsten Tagen von Ihrem angegebenen Bankkonto eingezogen.";
          $payment_method_german = "SEPA Lastschrift";
        }
        $wirecard_url = $_ENV['WIRECARD_API_URL'];
        $request_url = $wirecard_url . '/engine/rest/merchants/' . $merchant_id . '/payments/search';
        $wirecard_auth = $wirecard_username . ':' . $wirecard_password;
        $basic_auth = 'Basic ' . base64_encode($wirecard_auth);
        $headers = [
          'Authorization' => $basic_auth,
          'Accept'  => 'application/json',
        ];
        $query_params = ['payment.request-id' => $request_id];
        // Call drupal httpClient service.
        $client = \Drupal::httpClient();
        $request = $client->request('GET', $request_url, [
          'headers' => $headers,
          'http_errors' => FALSE,
          'query' => $query_params,
        ]);
        if ($request->getStatusCode() < 400) {
          $transaction = json_decode($request->getBody(), TRUE);
          $payment_amount = (string) $transaction['payment']['requested-amount']['value'];
          $payment_currency = $transaction['payment']['requested-amount']['currency'];
          // When this is requested from cart page.
          if (!empty($transaction) && $transaction['payment']['descriptor'] == 'cart' && !empty($transaction['payment']['order-number']) && !empty($account)) {
            $order_id = $transaction['payment']['order-number'];
            $order = Order::load($order_id);
            if (!empty($order) && $order->getCustomerId() == $account_uid) {
              $recalculated_price = $order->recalculateTotalPrice();
              $price_obj = $order->getTotalPrice();
              $total_amount = round($price_obj->getNumber(), 2);
              $currency_code = $price_obj->getCurrencyCode();
              // Create list of cart items.
              $items = $order->getItems();
              $count = 0;
              $order_details = [];
              foreach ($items as $item_key => $item) {
                $order_item = OrderItem::load($item->id());
                $quantity = (int) $order_item->get('quantity')->getValue()[0]['value'];
                $variation = $order_item->getPurchasedEntity();
                $voucher = $variation->getProduct();
                $voucher_id = $voucher->id();
                $actual_price = round($variation->price->getValue()[0]['number'], 2);
                $discount_percentage = $voucher->field_discount->value;
                // Calculating discount price.
                if (!empty($variation->price)) {
                  $discount = round(($actual_price * $discount_percentage) / 100, 2);
                  $purchase_price = round($actual_price - $discount, 2);
                }
                // Create session level entry to manage session purchase limit.
                $query = \Drupal::entityQuery('session_level_checkout');
                $query->condition('field_user_id', $account->id(), '=');
                $query->condition('field_voucher_id', $voucher_id, '=');
                $query->condition('field_variation_id', $variation->id(), '=');
                $query->condition('field_request_id', $request_id, '=');
                $query->accessCheck(FALSE);
                $session_limit_result = $query->execute();
                if (empty($session_limit_result)) {
                  $this->createSessionLevelEntity($account, $voucher, $variation, $quantity, $request_id);
                  $varation_stock = $variation->field_stock->value;
                  if ($varation_stock >= $quantity) {
                    $varation_stock_left = $varation_stock - $quantity;
                    $variation->field_stock->value = $varation_stock_left;
                    $variation->save();
                  }
                  // Create order details array for order complete email.
                  $order_details[] = [
                    'product_name' => ucfirst($voucher->getTitle()),
                    'variation' => $actual_price,
                    'purchase_price' => $purchase_price,
                    'quantity' => $quantity,
                  ];
                  // Create payment for order.
                  if ($count == 0) {
                    $commerceEntity = \Drupal::entityTypeManager()->getStorage('commerce_payment');
                    $payment = $commerceEntity->create([
                      'payment_gateway' => $payment_gateway_method,
                      'order_id' => $order_id,
                      'amount' => new Price($payment_amount, $payment_currency),
                    ]);
                    $paymentGateway = PaymentGateway::load($payment_gateway_method);
                    $paymentGateway->getPlugin()->createPayment($payment, TRUE);
                    $error_message = $transaction['payment']['statuses']['status'][0]['description'];
                    $wirecard_error = WirecardErrors::create([
                      'name' => 'Wirecard Transaction - ' . $request_id,
                      'created' => REQUEST_TIME,
                      'changed' => REQUEST_TIME,
                      'field_request_id' => $request_id,
                      'field_type' => $data['payment_method'],
                      'field_status' => 'success',
                      'field_error_message' => $error_message,
                    ]);
                    $wirecard_error->save();
                  }
                }
                $count++;

                // Get voucher alias with voucher id.
                $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/product/' . $voucher_id);
                $response['status'] = 'success';
                $response['redemption_data'] = $redeem_points['response_data'];
                $response['product'][$item_key]['item_id'] = $item->id();
                $response['product'][$item_key]['variation_id'] = $variation->variation_id->value;
                $response['product'][$item_key]['type'] = $voucher->bundle();
                $response['product'][$item_key]['title'] = ucfirst($voucher->getTitle());
                $response['product'][$item_key]['quantity'] = $quantity;
                $response['product'][$item_key]['product_url'] = $alias;
                // Adding price to response data.
                if (!empty($variation->price)) {
                  $response['product'][$item_key]['actual_price'] = $actual_price;
                  $response['product'][$item_key]['purchase_price'] = $purchase_price;
                  $response['product'][$item_key]['discount'] = $discount;
                  $response['product'][$item_key]['discount_per'] = $discount_percentage;
                  $response['product'][$item_key]['currency'] = $currency_code;
                }
                // Storing flap related data.
                $flap = $voucher->field_flap->value;
                $availibility_to = NULL;
                $stock = (int) $variation->get('field_stock')->getValue()[0]['value'];
                $response['product'][$item_key]['flap_details']['flap'] = $flap;
                if ($flap == 'availability') {
                  $end_date = new DrupalDateTime($voucher->field_availability_date->end_value, 'UTC');
                  $end_date->setTimezone(timezone_open(date_default_timezone_get()));
                  $availibility_to = $end_date->format('Y-m-d\TH:i:s');
                }
                $response['product'][$item_key]['flap_details']['stock'] = $stock;
                $response['product'][$item_key]['flap_details']['availability_date_to'] = $availibility_to;
                // Stock flap service to get stock flap status.
                $stock_flap_service = \Drupal::service('allianz_custom.stock_check');
                $flap_status = $stock_flap_service->getVoucherStockFlap($voucher_id);
                $out_of_stock = $stock_flap_service->isOutOfStock($voucher_id);
                $response['product'][$item_key]['flap_details']['flap_status'] = $flap_status;
                $response['product'][$item_key]['flap_details']['out_of_stock'] = $out_of_stock;
                // Create AllianzField class object.
                $allianz_field = new AllianzField();
                $voucher_image = reset($voucher->field_voucher_image->getValue());
                $response['product'][$item_key]['voucher_image'] = $allianz_field->getImageUrl($voucher_image['target_id'], 'checkout', 'checkout_mobile');
                $response['product'][$item_key]['voucher_image']['alt'] = $voucher_image['alt'];
                $response['product'][$item_key]['voucher_image']['title'] = $voucher_image['title'];
                // Voucher session service to get session checkout limit.
                $voucher_service = \Drupal::service('allianz_voucher_session.session_level_checkout');
                $session_level_limit = $voucher_service->getSessionCheckoutLimit($account_uid, $voucher);
                $response['product'][$item_key]['session_level_limit'] = $session_level_limit;
              }
              // Get delivery info from cart settings form.
              $config = \Drupal::config('allianz_checkout.cartsettings');
              // Static Text Info.
              $static_text = [
                'shipping_info' => $config->get('shipping_info'),
                'email_info' => $config->get('email_info'),
                'delivery_info' => $config->get('green_text'),
                'information_list' => $config->get('delivery_info'),
              ];
              $response['shipping_info'] = $static_text;
              $error = 200;

              // Change order status
              $order->set('state', 'completed');
              $order->field_request_id->value = $request_id;
              $order->save();
            }
            else {
              // Return 404 if requested order does not exist.
              $response['status'] = 'failure';
              $response['error'] = 'Order not found';
              $error = 404;
            }
          }
          // When this is requested from direct checkout page.
          elseif (!empty($transaction) && $transaction['payment']['descriptor'] == 'direct_checkout' && !empty($transaction['payment']['order-number']) && !empty($data['quantity']) && !empty($account)) {
            $quantity = $data['quantity'];
            $variation_id = $transaction['payment']['order-number'];
            $variation = ProductVariation::load($variation_id);
            if (!empty($variation)) {
              $varation_stock = $variation->field_stock->value;
              $varation_stock_left = $varation_stock - $data['quantity'];
              $voucher = $variation->getProduct();
              $voucher_id = $voucher->id();
              $actual_price = round($variation->price->getValue()[0]['number'], 2);
              $currency_code = $variation->getPrice()->getCurrencyCode();
              $discount_percentage = $voucher->field_discount->value;
              // Calculating discount price and adding to response data.
              if (!empty($variation->price)) {
                $discount = round(($actual_price * $discount_percentage) / 100, 2);
                $purchase_price = round($actual_price - $discount, 2);
              }
              
              // Create order item.
              // Check if order already exits or not.
              $query = \Drupal::entityQuery('commerce_order');
              $query->condition('field_request_id', $request_id, '=');
              $query->accessCheck(FALSE);
              $commerce_order_result = $query->execute();
              if (empty($commerce_order_result)) {
                $entityManager = \Drupal::entityManager();
                $order_item = $entityManager->getStorage('commerce_order_item')->create([
                  'type' => 'default',
                  'title' => $variation->getTitle(),
                  'purchased_entity' => (string) $variation_id,
                  'quantity' => $quantity,
                  'unit_price' => $variation->getPrice(),
                ]);
                $order_item->save();
                // Create order.
                $direct_order = Order::create([
                  'type' => 'default',
                  'state' => 'completed',
                  'uid' => $account_uid,
                  'store_id' => 1,
                  'order_items' => [$order_item],
                  'placed' => time(),
                  'field_request_id' => $request_id,
                ]);
                $direct_order->save();
                $product_price = $order_item->getUnitPrice();
                $product_unit_price = $product_price->getNumber();
                $discounted_price = $quantity * round((($product_unit_price * $discount_percentage) / 100), 2);
                $new_adjustment = $discounted_price;
                $adjustments = $order_item->getAdjustments();
                // Apply custom adjustment.
                $adjustments[] = new Adjustment([
                  'type' => 'custom_discount_adjustment',
                  'label' => 'Discount',
                  'amount' => new Price('-' . $new_adjustment, 'INR'),
                ]);
                $order_item->setAdjustments($adjustments);
                $order_item->save();

                $prime_cloud = \Drupal::service('allianz_prime_cloud.triggers');
                $member_id = $account->field_pc_member_id->value;
                $member_details = $prime_cloud->memberDetails($member_id);
                if (!empty($member_details)) {
                  $member_response_data = $member_details['response_data']['data']['owner']['properties'];
                  $billing_address = [
                    'country_code' => 'DE',
                    'given_name' => $member_response_data['FirstName'],
                    'family_name' => $member_response_data['LastName'],
                    'address_line1' => $member_response_data['Street'],
                    'postal_code' => $member_response_data['PostalCode'],
                    'locality' => $member_response_data['Street'],
                  ];
                  $profile = Profile::create([
                    'type' => 'customer',
                    'uid' => $account_uid,
                    'address' => $billing_address,
                  ]);
                  $direct_order->setBillingProfile($profile);
                }
                $direct_order->setOrderNumber($direct_order->id());
                $direct_order->save();
                // Create session level entry to manage session purchase limit.
                $query = \Drupal::entityQuery('session_level_checkout');
                $query->condition('field_user_id', $account->id(), '=');
                $query->condition('field_voucher_id', $voucher_id, '=');
                $query->condition('field_variation_id', $variation->id(), '=');
                $query->condition('field_request_id', $request_id, '=');
                $query->accessCheck(FALSE);
                $session_limit_result = $query->execute();
                $order_details = [];
                if (empty($session_limit_result)) {
                  $this->createSessionLevelEntity($account, $voucher, $variation, $quantity, $request_id);
                  if ($varation_stock >= $data['quantity']) {
                    $variation->field_stock->value = $varation_stock_left;
                    $variation->save();
                  }
                  // Create payment for order.
                  $commerceEntity = \Drupal::entityTypeManager()->getStorage('commerce_payment');
                  $payment = $commerceEntity->create([
                    'payment_gateway' => $payment_gateway_method,
                    'order_id' => $direct_order->id(),
                    'amount' => new Price($payment_amount, $payment_currency),
                  ]);
                  $paymentGateway = PaymentGateway::load($payment_gateway_method);
                  $paymentGateway->getPlugin()->createPayment($payment, TRUE);
                  $error_message = $transaction['payment']['statuses']['status'][0]['description'];
                  $wirecard_error = WirecardErrors::create([
                    'name' => 'Wirecard Transaction - ' . $request_id,
                    'created' => REQUEST_TIME,
                    'changed' => REQUEST_TIME,
                    'field_request_id' => $request_id,
                    'field_type' => $data['payment_method'],
                    'field_status' => 'success',
                    'field_error_message' => $error_message,
                  ]);
                  $wirecard_error->save();
                  // Create order details array for order complete email.
                  $order_details[] = [
                    'product_name' => ucfirst($voucher->getTitle()),
                    'variation' => $actual_price,
                    'purchase_price' => $purchase_price,
                    'quantity' => $quantity,
                  ];
                }
              }

              // Get voucher alias with voucher id.
              $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/product/' . $voucher_id);
              $response['status'] = 'success';
              $res['variation_id'] = $variation_id;
              $res['type'] = $voucher->bundle();
              $res['title'] = ucfirst($voucher->getTitle());
              $res['quantity'] = $quantity;
              $res['product_url'] = $alias;
              // Calculating discount price and adding to response data.
              if (!empty($variation->price)) {
                $discount = round(($actual_price * $discount_percentage) / 100, 2);
                $purchase_price = round($actual_price - $discount, 2);
                $res['actual_price'] = $actual_price;
                $res['purchase_price'] = $purchase_price;
                $res['discount'] = $discount;
                $res['discount_per'] = $discount_percentage;
                $res['currency'] = $currency_code;
              }
              // Storing flap related data.
              $flap = $voucher->field_flap->value;
              $availibility_to = NULL;
              $stock = (int) $variation->field_stock->value;
              $resd['flap_details']['flap'] = $flap;
              if ($flap == 'availability') {
                $end_date = new DrupalDateTime($voucher->field_availability_date->end_value, 'UTC');
                $end_date->setTimezone(timezone_open(date_default_timezone_get()));
                $availibility_to = $end_date->format('Y-m-d\TH:i:s');
              }
              $res['flap_details']['stock'] = $stock;
              $res['flap_details']['availability_date_to'] = $availibility_to;
              // Stock flap service to get stock flap status.
              $stock_flap_service = \Drupal::service('allianz_custom.stock_check');
              $flap_status = $stock_flap_service->getVoucherStockFlap($voucher_id);
              $out_of_stock = $stock_flap_service->isOutOfStock($voucher_id);
              $res['flap_details']['flap_status'] = $flap_status;
              $res['flap_details']['out_of_stock'] = $out_of_stock;
              // Create AllianzField class object.
              $allianz_field = new AllianzField();
              $voucher_image = reset($voucher->field_voucher_image->getValue());
              $res['voucher_image'] = $allianz_field->getImageUrl($voucher_image['target_id'], 'checkout', 'checkout_mobile');
              $res['voucher_image']['alt'] = $voucher_image['alt'];
              $res['voucher_image']['title'] = $voucher_image['title'];
              // Using voucher session service to get session checkout limit.
              $voucher_service = \Drupal::service('allianz_voucher_session.session_level_checkout');
              $session_level_limit = $voucher_service->getSessionCheckoutLimit($account_uid, $voucher);
              $res['session_level_limit'] = $session_level_limit;
              $response['product'][] = $res;
              // Get delivery info from cart settings form.
              $config = \Drupal::config('allianz_checkout.cartsettings');
              // Static Text Info.
              $static_text = [
                'shipping_info' => $config->get('shipping_info'),
                'email_info' => $config->get('email_info'),
                'delivery_info' => $config->get('green_text'),
                'information_list' => $config->get('delivery_info'),
              ];
              $response['static_text'] = $static_text;
              $error = 200;

              // Trigger order success email.
              if ($direct_order) {
                // Change order status.
                $direct_order->set('state', 'completed');
                $direct_order->save();
              }
            }
            else {
              $response['status'] = 'failure';
              $response['error'] = 'Variation with this id does not exist.';
              $error = 200;
            }
          }
          else {
            // Return 500 if requested request is invalid.
            $response['status'] = 'failure';
            $response['error'] = 'Something went wrong!';
            $error = 500;
          }
        }
        else {
          $response = json_decode($request->getBody(), TRUE);
          $error = $request->getStatusCode();
        }
      }
      else {
        // Return 500 if requested request is invalid.
        $response['status'] = 'failure';
        $response['error'] = 'Something went wrong!';
        $error = 500;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }
    return new ModifiedResourceResponse($response, $error);
  }

  /**
   * Create session level entity.
   */
  public function createSessionLevelEntity($account, $voucher, $variation, $quantity, $request_id) {
    $session_level_limit = $voucher->field_session_level_limit->value;
    $purchased_quantity = 0;
    // Select query to get session purchased limit for voucher.
    $query = \Drupal::entityQuery('session_level_checkout');
    $query->condition('field_user_id', $account->id(), '=');
    $query->condition('field_voucher_id', $voucher->id(), '=');
    $query->accessCheck(FALSE);
    $session_limit_result = $query->execute();
    if (!empty($session_limit_result)) {
      foreach ($session_limit_result as $session_limit_key => $session_limit_value) {
        $session_limit = SessionLevelCheckout::load($session_limit_value);
        if (!empty($session_limit)) {
          if (!empty($session_limit->field_quantity) && !empty($session_limit->field_quantity->value)) {
            $purchased_quantity = $purchased_quantity + $session_limit->field_quantity->value;
          }
        }
      }
    }
    $session_level_limit = $session_level_limit - $purchased_quantity;
    if (($quantity <= $session_level_limit) || empty($session_level_limit)) {
      $session_checkout = SessionLevelCheckout::create([
        'name'        => 'session_limit_' . $account->id(),
        'field_user_id'        => $account,
        'field_voucher_id'     => $voucher,
        'field_variation_id'     => $variation,
        'field_quantity'     => $quantity,
        'field_request_id' => $request_id,
      ]);
      $session_checkout->save();
    }
  }

}
