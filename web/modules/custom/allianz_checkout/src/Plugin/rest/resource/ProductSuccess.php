<?php

namespace Drupal\allianz_checkout\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\user\Entity\User;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\allianz_product\Controller\AllianzField;
use Drupal\allianz_voucher_session\Entity\SessionLevelCheckout;
use Drupal\allianz_checkout\Entity\ConnexOrder;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\commerce_price\Price;
use Drupal\profile\Entity\Profile;
use Drupal\allianz_checkout\Entity\WirecardErrors;
use Drupal\commerce_order\Adjustment;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "product_success",
 *   label = @Translation("Product Success"),
 *   uri_paths = {
 *     "create" = "/api/v1/product/success"
 *   }
 * )
 */
class ProductSuccess extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new ProductSuccess object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_checkout'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param string $data
   *   Get data object on request.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function post($data) {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $jwt = $authorization_token;
    $token = $transcoder->decode($jwt);
    $account_uid = $token->getPayload()->drupal->uid;
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if ((!empty($results) && $results['jwt_token'] == $jwt)) {
      if (!empty($data)) {
        $order = Order::load($data['order_id']);
        if (!empty($order)) {
          $currency_code = $order->getTotalPrice()->getCurrencyCode();
          if ($order->state->value != 'completed') {
            try {
              $account = User::load($account_uid);
              $member_id = $account->field_pc_member_id->value;
              $description['order_id'] = $data['order_id'];
              $pc_service = \Drupal::service('allianz_prime_cloud.triggers');
              $date = date('Y-m-d');
              $pc_data['date'] = $date . 'T00:00:00.000Z';
              $pc_data['receiptId'] = $member_id;
              $items = $order->getItems();
              if (!empty($items)) {
                foreach ($items as $item_key => $item) {
                  $orderItem = OrderItem::load($item->id());
                  $quantity = (int) $orderItem->get('quantity')->getValue()[0]['value'];
                  $variation = $orderItem->getPurchasedEntity();
                  $voucher = $variation->getProduct();
                  $voucher_id = $voucher->id();
                  $actual_price = round($variation->price->getValue()[0]['number'], 2);
                  $discount_percentage = $voucher->field_discount->value;
                  $varation_stock = $variation->field_stock->value;
                  if ($varation_stock >= $quantity) {
                    $varation_stock_left = $varation_stock - $quantity;
                    $variation->field_stock->value = $varation_stock_left;
                    $variation->save();
                  }
                  // Calculating discount price.
                  if (!empty($variation->price)) {
                    $discount = round(($actual_price * $discount_percentage) / 100, 2);
                    $purchase_price = round($actual_price - $discount, 2);
                  }
                  $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/product/' . $voucher_id);
                  $response['product'][$item_key]['item_id'] = $item->id();
                  $response['product'][$item_key]['variation_id'] = $variation->variation_id->value;
                  $response['product'][$item_key]['type'] = $voucher->bundle();
                  $response['product'][$item_key]['title'] = ucfirst($voucher->getTitle());
                  $response['product'][$item_key]['quantity'] = $quantity;
                  $response['product'][$item_key]['product_url'] = $alias;
                  // Adding price to response data.
                  if (!empty($variation->price)) {
                    $response['product'][$item_key]['actual_price'] = $actual_price;
                    $response['product'][$item_key]['purchase_price'] = $purchase_price;
                    $response['product'][$item_key]['discount'] = $discount;
                    $response['product'][$item_key]['discount_per'] = $discount_percentage;
                    $response['product'][$item_key]['currency'] = $currency_code;
                  }
                  // Storing flap related data.
                  $flap = $voucher->field_flap->value;
                  $availibility_to = NULL;
                  $stock = (int) $variation->get('field_stock')->getValue()[0]['value'];
                  $response['product'][$item_key]['flap_details']['flap'] = $flap;
                  if ($flap == 'availability') {
                    $end_date = new DrupalDateTime($voucher->field_availability_date->end_value, 'UTC');
                    $end_date->setTimezone(timezone_open(date_default_timezone_get()));
                    $availibility_to = $end_date->format('Y-m-d\TH:i:s');
                  }
                  $response['product'][$item_key]['flap_details']['stock'] = $stock;
                  $response['product'][$item_key]['flap_details']['availability_date_to'] = $availibility_to;
                  // Stock flap service to get stock flap status.
                  $stock_flap_service = \Drupal::service('allianz_custom.stock_check');
                  $flap_status = $stock_flap_service->getVoucherStockFlap($voucher_id);
                  $out_of_stock = $stock_flap_service->isOutOfStock($voucher_id);
                  $response['product'][$item_key]['flap_details']['flap_status'] = $flap_status;
                  $response['product'][$item_key]['flap_details']['out_of_stock'] = $out_of_stock;
                  // Create AllianzField class object.
                  $allianz_field = new AllianzField();
                  $product_image = reset($variation->field_product_image->getValue());
                  $response['product'][$item_key]['voucher_image'] = $allianz_field->getImageUrl($product_image['target_id'], 'checkout', 'checkout_mobile');
                  $response['product'][$item_key]['voucher_image']['alt'] = $product_image['alt'];
                  $response['product'][$item_key]['voucher_image']['title'] = $product_image['title'];
                }
                $orderPrice = round($order->getTotalPrice()->getNumber());
                $pc_data['amount'] = $orderPrice;
                $order_item_id = $order->getItems()[0]->id();
                $order_item = OrderItem::load($order_item_id);
                $variations = $order_item->getPurchasedEntity();
                $product_title = $variations->getTitle();
                $lp_desc['message'] = "Produkt gekauft " . $product_title;
                $lp_desc['order_detail'] = $description;
                $pc_data['description'] = json_encode($lp_desc);
                $query_params = ['type' => 'bonus'];
                $redeem_points = $pc_service->redeemPoints($member_id, $query_params, $pc_data);
                if ($redeem_points['error'] < 400) {
                  $response['redemption_data'] = $redeem_points['response_data'];
                  // Get delivery info from cart settings form.
                  $config = \Drupal::config('allianz_checkout.cartsettings');
                  // Static Text Info.
                  $static_text = [
                    'shipping_info' => $config->get('shipping_info'),
                    'email_info' => $config->get('email_info'),
                    'delivery_info' => $config->get('green_text'),
                    'information_list' => $config->get('delivery_info'),
                  ];
                  $response['shipping_info'] = $static_text;  
                  // Set customer billing profile.
                  $prime_cloud = \Drupal::service('allianz_prime_cloud.triggers');
                  $member_id = $account->field_pc_member_id->value;
                  $member_details = $prime_cloud->memberDetails($member_id);
                  if (!empty($member_details)) {
                    $member_response_data = $member_details['response_data']['data']['owner']['properties'];
                    $billing_address = [
                      'country_code' => 'DE',
                      'given_name' => $member_response_data['FirstName'],
                      'family_name' => $member_response_data['LastName'],
                      'address_line1' => $member_response_data['Street'],
                      'postal_code' => $member_response_data['PostalCode'],
                      'locality' => $member_response_data['Street'],
                    ];
                    $profile = Profile::create([
                      'type' => 'customer',
                      'uid' => $account_uid,
                      'address' => $billing_address,
                    ]);
                    $order->setBillingProfile($profile);
                  }
                  $order->set('state', 'completed');
                  $order->save();
                  // Remove product from wishlist

                  $response['status'] = 'success';
                  $response['data']['order_id'] = $data['order_id'];
                  $error = 200;
                }
                else {
                  // Return 400 if member key does not exist on prime cloud.
                  $response['status'] = 'failure';
                  $response['error'] = 'Invalid member key!';
                  $error = 400;
                }
              }
              else {
                // Return 500 if requested order item does not exist.
                $response['status'] = 'failure';
                $response['error'] = 'Somethings went wrong';
                $error = 500;
              }
            }
            catch (RequestException $e) {
              $response['status'] = 'failure';
              $response['error'] = 'Something swent wrong!';
              $error = 500;
            }
          }
          else {
            $items = $order->getItems();
            if (!empty($items)) {  
              $order_item_id = $order->getItems()[0]->id();
              $order_item = OrderItem::load($order_item_id);
              foreach ($items as $item_key => $item) {
                $orderItem = OrderItem::load($item->id());
                $quantity = (int) $orderItem->get('quantity')->getValue()[0]['value'];
                $variation = $orderItem->getPurchasedEntity();
                $voucher = $variation->getProduct();
                $voucher_id = $voucher->id();
                $actual_price = round($variation->price->getValue()[0]['number'], 2);
                $discount_percentage = $voucher->field_discount->value;
                // Calculating discount price.
                if (!empty($variation->price)) {
                  $discount = round(($actual_price * $discount_percentage) / 100, 2);
                  $purchase_price = round($actual_price - $discount, 2);
                }
                $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/product/' . $voucher_id);
                $response['redemption_data'] = $redeem_points['response_data'];
                $response['product'][$item_key]['item_id'] = $item->id();
                $response['product'][$item_key]['variation_id'] = $variation->variation_id->value;
                $response['product'][$item_key]['type'] = $voucher->bundle();
                $response['product'][$item_key]['title'] = ucfirst($voucher->getTitle());
                $response['product'][$item_key]['quantity'] = $quantity;
                $response['product'][$item_key]['product_url'] = $alias;
                // Adding price to response data.
                if (!empty($variation->price)) {
                  $response['product'][$item_key]['actual_price'] = $actual_price;
                  $response['product'][$item_key]['purchase_price'] = $purchase_price;
                  $response['product'][$item_key]['discount'] = $discount;
                  $response['product'][$item_key]['discount_per'] = $discount_percentage;
                  $response['product'][$item_key]['currency'] = $currency_code;
                }
                // Storing flap related data.
                $flap = $voucher->field_flap->value;
                $availibility_to = NULL;
                $stock = (int) $variation->get('field_stock')->getValue()[0]['value'];
                $response['product'][$item_key]['flap_details']['flap'] = $flap;
                if ($flap == 'availability') {
                  $end_date = new DrupalDateTime($voucher->field_availability_date->end_value, 'UTC');
                  $end_date->setTimezone(timezone_open(date_default_timezone_get()));
                  $availibility_to = $end_date->format('Y-m-d\TH:i:s');
                }
                $response['product'][$item_key]['flap_details']['stock'] = $stock;
                $response['product'][$item_key]['flap_details']['availability_date_to'] = $availibility_to;
                // Stock flap service to get stock flap status.
                $stock_flap_service = \Drupal::service('allianz_custom.stock_check');
                $flap_status = $stock_flap_service->getVoucherStockFlap($voucher_id);
                $out_of_stock = $stock_flap_service->isOutOfStock($voucher_id);
                $response['product'][$item_key]['flap_details']['flap_status'] = $flap_status;
                $response['product'][$item_key]['flap_details']['out_of_stock'] = $out_of_stock;
                // Create AllianzField class object.
                $allianz_field = new AllianzField();
                $product_image = reset($variation->field_product_image->getValue());
                $response['product'][$item_key]['voucher_image'] = $allianz_field->getImageUrl($product_image['target_id'], 'checkout', 'checkout_mobile');
                $response['product'][$item_key]['voucher_image']['alt'] = $product_image['alt'];
                $response['product'][$item_key]['voucher_image']['title'] = $product_image['title'];
              }

              // Get delivery info from cart settings form.
              $config = \Drupal::config('allianz_checkout.cartsettings');
              // Static Text Info.
              $static_text = [
                'shipping_info' => $config->get('shipping_info'),
                'email_info' => $config->get('email_info'),
                'delivery_info' => $config->get('green_text'),
                'information_list' => $config->get('delivery_info'),
              ];
              $response['shipping_info'] = $static_text;  
              $response['status'] = 'success';
              $response['data']['order_id'] = $data['order_id'];
              $error = 200;
            }
            else {
              // Return 500 if requested order does not exist.
              $response['status'] = 'failure';
              $response['error'] = 'Somethingd went wrong!';
              $error = 500;
            }
          }
        }
        else {
          // Return 500 if requested order does not exist.
          $response['status'] = 'failure';
          $response['error'] = 'Somethingd went wrong!';
          $error = 500;
        }
      }
      else {
        // Return 500 if requested request is invalid.
        $response['status'] = 'failure';
        $response['error'] = 'Somethingss went wrong!';
        $error = 500;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }

    $response = new ModifiedResourceResponse($response, $error);
    return $response;
  }

}
