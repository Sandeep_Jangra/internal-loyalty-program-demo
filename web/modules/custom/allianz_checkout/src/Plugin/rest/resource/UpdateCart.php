<?php

namespace Drupal\allianz_checkout\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\jwt\Authentication\Event\JwtAuthGenerateEvent;
use Drupal\jwt\JsonWebToken\JsonWebToken;
use Drupal\jwt\Authentication\Event\JwtAuthEvents;

/**
 * UpdateCart custom rest post request used to update cart item quantity.
 *
 * @RestResource(
 *   id = "update_cart",
 *   label = @Translation("Update Cart"),
 *   uri_paths = {
 *     "create" = "/api/v1/cart/update"
 *   }
 * )
 */
class UpdateCart extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new UpdateCart object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_checkout'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param string $data
   *   Get data object on request.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post($data) {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $jwt = $authorization_token;
    $token = $transcoder->decode($jwt);
    $account_uid = $token->getPayload()->drupal->uid;
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if ((!empty($results) && $results['jwt_token'] == $jwt)) {
      if (!empty($data) && !empty($data['order_id']) && !empty($data['item_id']) && !empty($data['quantity'])) {
        try {
          $order = Order::load($data['order_id']);
          if (!empty($order) && $order->state->value == 'draft' && $order->getCustomerId() == $account_uid) {
            $account = User::load($order->getCustomerId());
            $item_id = $data['item_id'];
            $order_items = $order->order_items;
            $item_exists = FALSE;
            foreach ($order_items as $key => $value) {
              if (!empty($order_items) && $value->target_id == $item_id) {
                $item_exists = TRUE;
                break;
              }
            }
            // Proceed only if item exist in the cart.
            if ($item_exists) {
              if (!empty($account)) {
                $order_item = OrderItem::load($item_id);
                $purchased_product = $order_item->getPurchasedEntity();
                $product_stock = $purchased_product->field_stock->value;
                $voucher = $purchased_product->getProduct();
                $added_quantity = $order_item->getQuantity() ? $order_item->getQuantity() : 0;
                $voucher_id = $purchased_product->getProductId();
                $variation_id = $purchased_product->id();

                // Using voucher session service to get session checkout limit.
                $voucher_service = \Drupal::service('allianz_voucher_session.session_level_checkout');
                $session_level_limit = $voucher_service->getSessionCheckoutLimit($account_uid, $voucher);
                // Reset session limit if session limit does not exist.
                if ($session_level_limit != 'not_exist') {
                  $session_level_limit += $added_quantity;
                }
                if ($data['quantity'] > 0) {
                  if ($product_stock > 0 && $product_stock >= $data['quantity']) {
                    if ($session_level_limit == 'not_exist' || ($session_level_limit > 0 && $session_level_limit >= $data['quantity'])) {
                      $order_item->setQuantity($data['quantity']);
                      $order_item->save();
                      $order->save();
                      // Update variation stock.
                      $product_stock -= $data['quantity'];
                      // Update session level limit.
                      if ($session_level_limit != 'not_exist') {
                        $session_level_limit -= $data['quantity'];
                      }
                      // Generate refresh token for user
                      // Load dispatcher object through services.
                      $dispatcher = \Drupal::service('event_dispatcher');
                      $event = new JwtAuthGenerateEvent(new JsonWebToken());
                      $dispatcher->dispatch(JwtAuthEvents::GENERATE, $event);
                      // Generate jwt token for requested user.
                      $event->addClaim('exp', strtotime('+2000 minutes'));
                      $event->addClaim(['drupal', 'uid'], $account_uid);
                      $jwt = $event->getToken();
                      $refresh_token = $transcoder->encode($jwt);
                      // Update refrest token to custom table.
                      $token_updated = $connection->update('custom_jwt_token')
                        ->fields([
                          'jwt_token' => $refresh_token,
                        ])
                        ->condition('uid', $account_uid, '=')
                        ->execute();

                      $response['status'] = 'success';
                      $response['data']['token'] = $refresh_token;
                      $response['data']['stock'] = $product_stock;
                      $response['data']['variation_id'] = $variation_id;
                      $response['data']['voucher_id'] = $voucher_id;
                      $response['data']['session_level_limit'] = $session_level_limit;
                      $error = 200;
                    }
                    else {
                      // Return 200 but status failure.
                      // If session level limit exceeds.
                      $response['status'] = 'failure';
                      $response['data']['error'] = 'Session level limit exceeds.';
                      $response['data']['session_level_limit'] = $session_level_limit;
                      $error = 200;
                    }
                  }
                  else {
                    // Return 200 but status failure.
                    // If requested product does not enough stock.
                    $response['status'] = 'failure';
                    $response['data']['error'] = 'Only ' . $product_stock . ' left in stock';
                    $response['data']['stock'] = $product_stock;
                    $response['data']['quantity'] = $data['quantity'];
                    $error = 200;
                  }
                }
                else {
                  // Return 500 if requested quantity is 0.
                  $response['status'] = 'failure';
                  $response['data']['error'] = 'Quantity should be greater than 0';
                  $error = 200;
                }
              }
              else {
                // Return 500 if requested request is invalid.
                $response['status'] = 'failure';
                $response['error'] = 'Something went wrong!';
                $error = 500;
              }
            }
            else {
              // Return 500 if requested request is invalid.
              $response['status'] = 'failure';
              $response['error'] = 'Item not present in the cart!';
              $error = 200;
            }
          }
          else {
            // Return 500 if requested order is invalid for requested user.
            $response['status'] = 'failure';
            $response['error'] = 'Something went wrong!';
            $error = 500;
          }
        }
        catch (RequestException $e) {
          $response['status'] = 'failure';
          $response['error'] = 'Something went wrong!';
          $error = 500;
        }
      }
      else {
        // Return 500 if requested request is invalid.
        $response['status'] = 'failure';
        $response['error'] = 'Something went wrong!';
        $error = 500;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }

    $response = new ModifiedResourceResponse($response, $error);
    return $response;
  }

}
