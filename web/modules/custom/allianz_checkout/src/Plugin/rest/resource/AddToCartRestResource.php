<?php

namespace Drupal\allianz_checkout\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\jwt\Authentication\Event\JwtAuthGenerateEvent;
use Drupal\jwt\JsonWebToken\JsonWebToken;
use Drupal\jwt\Authentication\Event\JwtAuthEvents;

/**
 * Used to add requested product to cart.
 *
 * @RestResource(
 *   id = "add_to_cart_rest_resource",
 *   label = @Translation("Add to cart rest resource"),
 *   uri_paths = {
 *     "create" = "/api/v1/cart/add"
 *   }
 * )
 */
class AddToCartRestResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new AddToCartRestResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_checkout'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param string $data
   *   Get data object on request.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post($data) {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $jwt = $authorization_token;
    $token = $transcoder->decode($jwt);
    $account_uid = $token->getPayload()->drupal->uid;
    $account = User::load($account_uid);
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if ((!empty($results) && $results['jwt_token'] == $jwt)) {
      if (!empty($data) && !empty($data['variation_id']) && !empty($account_uid)) {
        $storeId = 1;
        $entityManager = \Drupal::entityManager();
        $cartManager = \Drupal::service('commerce_cart.cart_manager');
        $cartProvider = \Drupal::service('commerce_cart.cart_provider');
        $store = \Drupal::entityTypeManager()
          ->getStorage('commerce_store')
          ->load($storeId);
        $cart = $cartProvider->getCart('default', $store, $account);
        try {
          if (!$cart) {
            // Create cart and add item in the cart.
            $variationobj = \Drupal::entityTypeManager()
              ->getStorage('commerce_product_variation')
              ->load($data['variation_id']);
            $voucher = $variationobj->getProduct();
            $product_stock = $variationobj->field_stock->value;
            if ($data['quantity'] > 0) {
              if ($product_stock > 0 && $product_stock >= $data['quantity']) {
                if ($voucher->bundle() == 'voucher') {
                  // Using voucher session service to get session checkout limit.
                  $voucher_service = \Drupal::service('allianz_voucher_session.session_level_checkout');
                  $session_level_limit = $voucher_service->getSessionCheckoutLimit($account_uid, $voucher);
                  if ($session_level_limit == 'not_exist' || ($session_level_limit > 0 && $session_level_limit >= $data['quantity'])) {
                    $cart = $cartProvider->createCart('default', $store, $account);
                    // Process to place order programatically.
                    $order_item = $entityManager->getStorage('commerce_order_item')->create([
                      'type' => 'default',
                      'purchased_entity' => (string) $data['variation_id'],
                      // Amount or quantity to be added to the cart.
                      'quantity' => $data['quantity'],
                      'unit_price' => $variationobj->getPrice(),
                    ]);
                    $order_item->save();
                    $cartManager->addOrderItem($cart, $order_item);
                    $stock_left = $product_stock - $data['quantity'];
                    if ($session_level_limit != 'not_exist') {
                      $session_level_limit -= $data['quantity'];
                    }
                    // Generate refresh token for user
                    // Load dispatcher object through services.
                    $dispatcher = \Drupal::service('event_dispatcher');
                    $event = new JwtAuthGenerateEvent(new JsonWebToken());
                    $dispatcher->dispatch(JwtAuthEvents::GENERATE, $event);
                    // Generate jwt token for requested user.
                    $event->addClaim('exp', strtotime('+2000 minutes'));
                    $event->addClaim(['drupal', 'uid'], $account_uid);
                    $jwt = $event->getToken();
                    $refresh_token = $transcoder->encode($jwt);
                    // Update refrest token to custom table.
                    $token_updated = $connection->update('custom_jwt_token')
                      ->fields([
                        'jwt_token' => $refresh_token,
                      ])
                      ->condition('uid', $account_uid, '=')
                      ->execute();

                    $response['status'] = 'success';
                    $response['data']['token'] = $refresh_token;
                    $response['data']['order_id'] = $cart->id();
                    $response['data']['stock'] = $stock_left;
                    $response['data']['session_level_limit'] = $session_level_limit;
                    $error = 200;
                  }
                  else {
                    // Return 200 but status failure.
                    // If session level limit exceeds.
                    $response['status'] = 'failure';
                    $response['data']['error'] = 'Session level limit exceeds.';
                    $response['data']['session_level_limit'] = $session_level_limit;
                    $error = 200;
                  }
                }
                else {
                  // Physicall product cart 
                  $cart = $cartProvider->createCart('default', $store, $account);
                  // Process to place order programatically.
                  $order_item = $entityManager->getStorage('commerce_order_item')->create([
                    'type' => 'default',
                    'purchased_entity' => (string) $data['variation_id'],
                    // Amount or quantity to be added to the cart.
                    'quantity' => $data['quantity'],
                    'unit_price' => $variationobj->getPrice(),
                  ]);
                  $order_item->save();
                  $cartManager->addOrderItem($cart, $order_item);
                  $stock_left = $product_stock - $data['quantity'];
                  // Generate refresh token for user
                  // Load dispatcher object through services.
                  $dispatcher = \Drupal::service('event_dispatcher');
                  $event = new JwtAuthGenerateEvent(new JsonWebToken());
                  $dispatcher->dispatch(JwtAuthEvents::GENERATE, $event);
                  // Generate jwt token for requested user.
                  $event->addClaim('exp', strtotime('+2000 minutes'));
                  $event->addClaim(['drupal', 'uid'], $account_uid);
                  $jwt = $event->getToken();
                  $refresh_token = $transcoder->encode($jwt);
                  // Update refrest token to custom table.
                  $token_updated = $connection->update('custom_jwt_token')
                    ->fields([
                      'jwt_token' => $refresh_token,
                    ])
                    ->condition('uid', $account_uid, '=')
                    ->execute();

                  $response['status'] = 'success';
                  $response['data']['token'] = $refresh_token;
                  $response['data']['order_id'] = $cart->id();
                  $response['data']['stock'] = $stock_left;
                  $error = 200;
                }
              }
              else {
                $response['status'] = 'failure';
                $response['data']['error'] = 'Only ' . $product_stock . ' left in stock';
                $response['data']['stock'] = $product_stock;
                $error = 200;
              }
            }
            else {
              $response['status'] = 'failure';
              $response['data']['error'] = 'Quantity should be greater than 0';
              $error = 200;
            }
          }
          else {
            // If items are already present in the cart add new item.
            $variationobj = \Drupal::entityTypeManager()
              ->getStorage('commerce_product_variation')
              ->load($data['variation_id']);
            $voucher = $variationobj->getProduct();
            $product_stock = $variationobj->field_stock->value;
            $cart_items = $cart->getItems();
            $added_quantity = 0;
            foreach ($cart_items as $key => $cart_item) {
              $added_variation_id = $cart_item->getPurchasedEntityId();
              if ($added_variation_id == $data['variation_id']) {
                $added_quantity = $cart_item->getQuantity();
                break;
              }
            }
            $stock_left = $product_stock - $added_quantity;
            if ($data['quantity'] > 0) {
              if ($stock_left > 0 && $stock_left >= $data['quantity']) {
                if ($voucher->bundle() == 'voucher') {
                  // Using voucher session service to get session checkout limit.
                  $voucher_service = \Drupal::service('allianz_voucher_session.session_level_checkout');
                  $session_level_limit = $voucher_service->getSessionCheckoutLimit($account_uid, $voucher);
                  //check voucher session limit
                  if ($session_level_limit == 'not_exist' || ($session_level_limit > 0 && $session_level_limit >= $data['quantity'])) {
                    // Process to place order programatically.
                    $order_item = $entityManager->getStorage('commerce_order_item')->create([
                      'type' => 'default',
                      'purchased_entity' => (string) $data['variation_id'],
                      // Amount or quantity to be added to the cart.
                      'quantity' => $data['quantity'],
                      'unit_price' => $variationobj->getPrice(),
                    ]);

                    $order_item->save();
                    $cartManager->addOrderItem($cart, $order_item);
                    $stock_left -= $data['quantity'];
                    if ($session_level_limit != 'not_exist') {
                      $session_level_limit -= $data['quantity'];
                    }
                    // Generate refresh token for user
                    // Load dispatcher object through services.
                    $dispatcher = \Drupal::service('event_dispatcher');
                    $event = new JwtAuthGenerateEvent(new JsonWebToken());
                    $dispatcher->dispatch(JwtAuthEvents::GENERATE, $event);
                    // Generate jwt token for requested user.
                    $event->addClaim('exp', strtotime('+2000 minutes'));
                    $event->addClaim(['drupal', 'uid'], $account_uid);
                    $jwt = $event->getToken();
                    $refresh_token = $transcoder->encode($jwt);
                    // Update refrest token to custom table.
                    $token_updated = $connection->update('custom_jwt_token')
                      ->fields([
                        'jwt_token' => $refresh_token,
                      ])
                      ->condition('uid', $account_uid, '=')
                      ->execute();

                    $response['status'] = 'success';
                    $response['data']['token'] = $refresh_token;
                    $response['data']['order_id'] = $cart->id();
                    $response['data']['stock'] = $stock_left;
                    $response['data']['session_level_limit'] = $session_level_limit;
                    $error = 200;
                  }
                  else {
                    // Return 200 but status failure.
                    // If session level limit exceeds.
                    $response['status'] = 'failure';
                    $response['data']['error'] = 'Session level limit exceeds.';
                    $response['data']['session_level_limit'] = $session_level_limit;
                    $error = 200;
                  }
                }
                else {
                  // Pgysicall product cart 
                  // Process to place order programatically.
                  $order_item = $entityManager->getStorage('commerce_order_item')->create([
                    'type' => 'default',
                    'purchased_entity' => (string) $data['variation_id'],
                    // Amount or quantity to be added to the cart.
                    'quantity' => $data['quantity'],
                    'unit_price' => $variationobj->getPrice(),
                  ]);

                  $order_item->save();
                  $cartManager->addOrderItem($cart, $order_item);
                  $stock_left -= $data['quantity'];
                  // Generate refresh token for user
                  // Load dispatcher object through services.
                  $dispatcher = \Drupal::service('event_dispatcher');
                  $event = new JwtAuthGenerateEvent(new JsonWebToken());
                  $dispatcher->dispatch(JwtAuthEvents::GENERATE, $event);
                  // Generate jwt token for requested user.
                  $event->addClaim('exp', strtotime('+2000 minutes'));
                  $event->addClaim(['drupal', 'uid'], $account_uid);
                  $jwt = $event->getToken();
                  $refresh_token = $transcoder->encode($jwt);
                  // Update refrest token to custom table.
                  $token_updated = $connection->update('custom_jwt_token')
                    ->fields([
                      'jwt_token' => $refresh_token,
                    ])
                    ->condition('uid', $account_uid, '=')
                    ->execute();

                  $response['status'] = 'success';
                  $response['data']['token'] = $refresh_token;
                  $response['data']['order_id'] = $cart->id();
                  $response['data']['stock'] = $stock_left;
                  $error = 200;
                }
              }
              else {
                // Return 200 but status failure.
                // If product does not have enough stock.
                $response['status'] = 'failure';
                $response['data']['error'] = 'Only ' . $stock_left . ' left in stock';
                $response['data']['stock'] = $stock_left;
                $error = 200;
              }
            }
            else {
              $response['status'] = 'failure';
              $response['data']['error'] = 'Quantity should be greater than 0';
              $error = 200;
            }
          }
        }
        catch (RequestException $e) {
          $response['status'] = 'failure';
          $response['error'] = 'Something went wrong!';
          $error = 500;
        }
      }
      else {
        // Return 500 if requested request is invalid.
        $response['status'] = 'failure';
        $response['error'] = 'Something went wrong!';
        $error = 500;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }

    $response = new ModifiedResourceResponse($response, $error);
    return $response;
  }

}
