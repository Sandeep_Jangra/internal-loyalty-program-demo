<?php

namespace Drupal\allianz_checkout\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\user\Entity\User;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_product\Entity\ProductVariation;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "wirecard_payment",
 *   label = @Translation("Wirecard payment"),
 *   uri_paths = {
 *     "create" = "/api/v1/wirecard/payment"
 *   }
 * )
 */
class WirecardPayment extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new WirecardPayment object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_checkout'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param string $payload
   *   Get data payload on request.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post($payload) {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $jwt = $authorization_token;
    $token = $transcoder->decode($jwt);
    $account_uid = $token->getPayload()->drupal->uid;
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if ((!empty($results) && $results['jwt_token'] == $jwt)) {
      if (!empty($payload['payment_method']) && !empty($payload['total_price']) && !empty($payload['checkout_type']) && !empty($payload['checkout_id'])) {
        $variation_stock = TRUE;
        // Check purchase product status.
        if ($payload['checkout_type'] == 'cart' && !empty($payload['checkout_id'])) {
          $order_id = $payload['checkout_id'];
          $order = Order::load($order_id);
          if (!empty($order)) {
            $items = $order->getItems();
            foreach ($items as $item_key => $item) {
              $order_item = OrderItem::load($item->id());
              $quantity = (int) $order_item->get('quantity')->getValue()[0]['value'];
              $variation = $order_item->getPurchasedEntity();
              $voucher = $variation->getProduct();
              $voucher_id = $voucher->id();
              if ($variation->field_stock->value < $quantity) {
                $variation_stock = FALSE;
              }
            }
          }
        }
        elseif ($payload['checkout_type'] == 'direct_checkout' && !empty($payload['checkout_id']) && !empty($payload['quantity'])) {
          $quantity = $payload['quantity'];
          $variation_id = $payload['checkout_id'];
          $variation = ProductVariation::load($variation_id);
          $stock = (int) $variation->field_stock->value;
          if ($stock < $quantity) {
            $variation_stock = FALSE;
          }
        }
        if ($variation_stock) {
          try {
            $account = User::load($account_uid);
            $wirecard_url = $_ENV['WIRECARD_URL'];
            $request_url = $wirecard_url . '/api/payment/register';
            $request_id = time();
            $merchant_id = '';
            $transaction_type = '';
            if ($payload['payment_method'] == 'creditcard') {
              $merchant_id = $_ENV['WIRECARD_CCARD_MID'];
              $transaction_type = $_ENV['WIRECARD_TXTYPE_CCARD'];
              $wirecard_username = $_ENV['WIRECARD_USERNAME_CCARD'];
              $wirecard_password = $_ENV['WIRECARD_PASSWORD_CCARD'];
              $request_id_q_params = time();
            }
            elseif ($payload['payment_method'] = 'sepadirectdebit') {
              $merchant_id = $_ENV['WIRECARD_SEPA_MID'];
              $transaction_type = $_ENV['WIRECARD_TXTYPE_SEPA'];
              $wirecard_username = $_ENV['WIRECARD_USERNAME_SEPA'];
              $wirecard_password = $_ENV['WIRECARD_PASSWORD_SEPA'];
              $request_id_q_params = time() . '-pending-debit';
            }
            $wirecard_auth = $wirecard_username . ':' . $wirecard_password;
            $basic_auth = 'Basic ' . base64_encode($wirecard_auth);
            $headers = [
              'Authorization' => $basic_auth,
              'Content-Type'  => 'application/json',
            ];
            $quantity = '';
            if (!empty($payload['quantity'])) {
              $quantity = "&quantity=" . $payload['quantity'];
            }
            $success_q_params = "?request_id=" . $request_id_q_params . "&payment_method=" . $payload['payment_method'] . $quantity;
            if ($payload['checkout_type'] == 'cart') {
              $error_param = "?error=true&type=cart&order_id=" . $payload['checkout_id'] . "&request_id=" . $request_id_q_params . "&payment_method=" . $payload['payment_method'];
            }
            else {
              $error_param = "?error=true&type=direct_checkout&order_id=" . $payload['checkout_id'] . $quantity . "&request_id=" . $request_id_q_params . "&payment_method=" . $payload['payment_method'];
            }
            $error_q_params = "?error=true&type=" . $payload['checkout_type'] . "&order_id=511";
            $data['payment']['merchant-account-id']['value'] = $merchant_id;
            $data['payment']['request-id'] = $request_id;
            $data['payment']['transaction-type'] = $transaction_type;
            $data['payment']['descriptor'] = $payload['checkout_type'];
            $data['payment']['order-number'] = $payload['checkout_id'];
            $data['payment']['requested-amount']['value'] = $payload['total_price'];
            $data['payment']['requested-amount']['currency'] = "INR";
            $data['payment']['account-holder']['first-name'] = $account->field_first_name->value;
            $data['payment']['account-holder']['last-name'] = $account->field_last_name->value;
            $data['payment']['payment-methods']['payment-method'] = [["name" => $payload['payment_method']]];
            $data['payment']['three-d']['attempt-three-d'] = true;
            $data['payment']['three-d']['version'] = "2.1";
            $data['payment']['mandate']['mandate-id'] = 12345678;
            $data['payment']['creditor-id'] = "DE98ZZZ09999999999";
            $data['payment']['success-redirect-url'] = $_ENV['WIRECARD_SUCCESS'] . $success_q_params;
            $data['payment']['fail-redirect-url'] = $_ENV['WIRECARD_FAIL'] . $error_param;
            $data['payment']['cancel-redirect-url'] = $_ENV['WIRECARD_FAIL'] . $error_param;
            $data['payment']['locale'] = 'en';
            $data['options']['mode'] = "embedded";
            $data['options']['frame-ancestor'] = $_ENV['REDIRECT_URI'];
            // Call drupal httpClient service.
            $client = \Drupal::httpClient();
            $request = $client->request('POST', $request_url, [
              'headers' => $headers,
              'http_errors' => FALSE,
              'json' => $data,
            ]);
            if ($request->getStatusCode() < 400) {
              $response['status'] = 'success';
              $response['data'] = json_decode($request->getBody(), TRUE);
              $error = $request->getStatusCode();
            }
            else {
              $response = json_decode($request->getBody(), TRUE);
              $error = $request->getStatusCode();
            }
          }
          catch (RequestException $e) {
            $response['status'] = 'failure';
            $response['error'] = 'Something went wrong!';
            $error = 500;
          }
        }
        else {
          $response = [];
          $response['status'] = 'failure';
          $response['error'] = TRUE;
          $error = 200;
        }
      }
      else {
        $response['status'] = 'failure';
        $response['error'] = 'Something went wrong!';
        $error = 500;
      }
    }
    else {
      // Return 401 if jwt token does not exist or expired.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }
    $response = new ResourceResponse($response, $error);
    // Disable api cache.
    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);
    $response->addCacheableDependency($disable_cache);

    return $response;
  }

}
