<?php

namespace Drupal\allianz_checkout\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\allianz_checkout\Entity\WirecardErrors;
use Drupal\user\Entity\User;
use Drupal\Core\Database\Database;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "wirecard_error_logs",
 *   label = @Translation("Wirecard Error Logs"),
 *   uri_paths = {
 *     "create" = "/api/v1/wirecard/error"
 *   }
 * )
 */
class WirecardErrorLogs extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new WirecardErrorLogs object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_checkout'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param string $data
   *   Get data object on request.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post($data) {

    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $jwt = $authorization_token;
    $token = $transcoder->decode($jwt);
    $account_uid = $token->getPayload()->drupal->uid;
    $account = User::load($account_uid);
    // Empty member_id case to be handled..
    $member_id = $account->field_pc_member_id->value;
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if ((!empty($results) && $results['jwt_token'] == $jwt)) {
      if (!empty($data) && !empty($data['request_id']) && !empty($data['payment_method'])) {
        $request_id = $data['request_id'];
        $query = \Drupal::entityQuery('wirecard_errors');
        $query->condition('field_status', 'error', '=');
        $query->condition('field_type', $data['payment_method'], '=');
        $query->condition('field_request_id', $request_id, '=');
        $query->accessCheck(FALSE);
        $wirecard = $query->execute();
        if (empty($wirecard)) {
          // Get transaction detail using request id.
          if ($data['payment_method'] == 'creditcard') {
            $merchant_id = $_ENV['WIRECARD_CCARD_MID'];
            $wirecard_username = $_ENV['WIRECARD_USERNAME_CCARD'];
            $wirecard_password = $_ENV['WIRECARD_PASSWORD_CCARD'];
          }
          elseif ($data['payment_method'] = 'sepadirectdebit') {
            $merchant_id = $_ENV['WIRECARD_SEPA_MID'];
            $wirecard_username = $_ENV['WIRECARD_USERNAME_SEPA'];
            $wirecard_password = $_ENV['WIRECARD_PASSWORD_SEPA'];
          }
          $wirecard_url = $_ENV['WIRECARD_API_URL'];
          $request_url = $wirecard_url . '/engine/rest/merchants/' . $merchant_id . '/payments/search';
          $wirecard_auth = $wirecard_username . ':' . $wirecard_password;
          $basic_auth = 'Basic ' . base64_encode($wirecard_auth);
          $headers = [
            'Authorization' => $basic_auth,
            'Accept'  => 'application/json',
          ];
          $query_params = ['payment.request-id' => $request_id];
          // Call drupal httpClient service.
          $client = \Drupal::httpClient();
          $request = $client->request('GET', $request_url, [
            'headers' => $headers,
            'http_errors' => FALSE,
            'query' => $query_params,
          ]);
          if ($request->getStatusCode() < 400) {
            // Check if wirecard transaction entry already exist.
            $transaction = json_decode($request->getBody(), TRUE);
            $error_message = $transaction['payment']['statuses']['status'][0]['description'];
            $wirecard_error = WirecardErrors::create([
              'name' => 'Wirecard Transaction - ' . $request_id,
              'created' => REQUEST_TIME,
              'changed' => REQUEST_TIME,
              'field_request_id' => $request_id,
              'field_type' => $data['payment_method'],
              'field_status' => 'error',
              'field_error_message' => $error_message,
            ]);
            $wirecard_error->save();
            $response['status'] = 'success';
            $error = 200;
          }
          else {
            // Return 500 if requested request is invalid.
            $response['status'] = 'failure';
            $response['error'] = 'Something went wrong!';
            $error = 500;
          }
        }
        else {
          // Return 500 if requested request is invalid.
          $response['status'] = 'failure';
          $response['error'] = 'Something went wrong!';
          $error = 500;
        }
      }
      else {
        // Return 500 if requested request is invalid.
        $response['status'] = 'failure';
        $response['error'] = 'Something went wrong!';
        $error = 500;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }
    return new ModifiedResourceResponse($response, $error);
  }

}
