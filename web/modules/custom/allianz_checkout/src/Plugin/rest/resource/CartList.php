<?php

namespace Drupal\allianz_checkout\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\allianz_product\Controller\AllianzField;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "cart_list",
 *   label = @Translation("Cart List"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/cart/list"
 *   }
 * )
 */
class CartList extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new CartList object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_checkout'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Responds to GET requests.
   *
   * @param string $data
   *   Get data on request.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get($data) {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $jwt = $authorization_token;
    $token = $transcoder->decode($jwt);
    $account_uid = $token->getPayload()->drupal->uid;
    $account = User::load($account_uid);
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if ((!empty($results) && $results['jwt_token'] == $jwt)) {
      try {
        $storeId = 1;
        $entityManager = \Drupal::entityManager();
        $cartManager = \Drupal::service('commerce_cart.cart_manager');
        $cartProvider = \Drupal::service('commerce_cart.cart_provider');
        $store = \Drupal::entityTypeManager()
          ->getStorage('commerce_store')
          ->load($storeId);
        // Load the cart for the user.
        $cart = $cartProvider->getCart('default', $store, $account);
        if (!empty($cart)) {
          $cart->save();
          $response['status'] = 'success';
          // Get order id.
          $response['order_id'] = $cart->order_id->value;
          // Stock flap service to get stock flap status.
          $stock_flap_service = \Drupal::service('allianz_custom.stock_check');
          $order_items = [];
          // Store all order items added in the Cart.
          $order_items = $cart->order_items;
          if (!empty($order_items)) {
            // Load all order items.
            foreach ($order_items as $item_key => $item_id) {
              $order_item = OrderItem::load($item_id->target_id);
              $quantity = (int) $order_item->get('quantity')->getValue()[0]['value'];
              $variation = $order_item->getPurchasedEntity();
              $voucher = $variation->getProduct();
              $voucher_id = $voucher->id();
              // Get voucher alias with voucher id.
              $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/product/' . $voucher_id);
              $response['product'][$item_key]['item_id'] = $item_id->target_id;
              $response['product'][$item_key]['variation_id'] = $variation->variation_id->value;
              $response['product'][$item_key]['voucher_id'] = $voucher_id;
              $response['product'][$item_key]['type'] = $voucher->bundle();
              $response['product'][$item_key]['title'] = ucfirst($voucher->getTitle());
              $response['product'][$item_key]['quantity'] = $quantity;
              $response['product'][$item_key]['product_url'] = $alias;
              $actual_price = round($variation->price->getValue()[0]['number'], 2);
              $currency_code = $variation->getPrice()->getCurrencyCode();
              $discount_percentage = $voucher->field_discount->value;
              // Calculating discount price and adding to response data.
              if (!empty($variation->price)) {
                $discount = round(($actual_price * $discount_percentage) / 100, 2);
                $purchase_price = round($actual_price - $discount, 2);
                $response['product'][$item_key]['actual_price'] = $actual_price;
                $response['product'][$item_key]['purchase_price'] = $purchase_price;
                $response['product'][$item_key]['discount'] = $discount;
                $response['product'][$item_key]['discount_per'] = $discount_percentage;
                $response['product'][$item_key]['currency'] = $currency_code;
              }
              // Storing flap related data.
              $flap = $voucher->field_flap->value;
              $availibility_to = NULL;
              $stock = (int) $variation->field_stock->value;
              $response['product'][$item_key]['flap_details']['flap'] = $flap;
              if ($flap == 'availability') {
                $end_date = new DrupalDateTime($voucher->field_availability_date->end_value, 'UTC');
                $end_date->setTimezone(timezone_open(date_default_timezone_get()));
                $availibility_to = $end_date->format('Y-m-d\TH:i:s');
              }
              $response['product'][$item_key]['flap_details']['stock'] = $stock;
              $response['product'][$item_key]['flap_details']['availability_date_to'] = $availibility_to;
              $flap_status = $stock_flap_service->getVoucherStockFlap($voucher_id);
              $out_of_stock = $stock_flap_service->isOutOfStock($voucher_id);
              $response['product'][$item_key]['flap_details']['flap_status'] = $flap_status;
              $response['product'][$item_key]['flap_details']['out_of_stock'] = $out_of_stock;
              // Create AllianzField class object.
              $allianz_field = new AllianzField();
              if ($voucher->bundle() == 'voucher') {
                $voucher_image = reset($voucher->field_voucher_image->getValue());
                $response['product'][$item_key]['voucher_image'] = $allianz_field->getImageUrl($voucher_image['target_id'], 'checkout', 'checkout_mobile');
                $response['product'][$item_key]['voucher_image']['alt'] = $voucher_image['alt'];
                $response['product'][$item_key]['voucher_image']['title'] = $voucher_image['title'];
              }
              else {
                $product_image = reset($variation->field_product_image->getValue());
                $response['product'][$item_key]['voucher_image'] = $allianz_field->getImageUrl($product_image['target_id'], 'checkout', 'checkout_mobile');
                $response['product'][$item_key]['voucher_image']['alt'] = $product_image['alt'];
                $response['product'][$item_key]['voucher_image']['title'] = $product_image['title'];
              }
              // Using voucher session service to get session checkout limit.
              $voucher_service = \Drupal::service('allianz_voucher_session.session_level_checkout');
              $session_level_limit = $voucher_service->getCartListSessionLimit($account_uid, $voucher);
              $response['product'][$item_key]['session_level_limit'] = $session_level_limit;
            }
            // Get delivery info from cart settings form.
            $config = \Drupal::config('allianz_checkout.cartsettings');
            // Static Text Info.
            $static_text = [
              'shipping_info' => $config->get('shipping_info'),
              'email_info' => $config->get('email_info'),
              'delivery_info' => $config->get('green_text'),
              'information_list' => $config->get('delivery_info'),
            ];
            $response['static_text'] = $static_text;
            $error = 200;
          }
          else {
            $response['status'] = 'failure';
            $response['error'] = 'Something went wrong!';
            $error = 500;
          }
        }
        else {
          $response['status'] = 'failure';
          $response['error'] = 'Cart is empty!';
          $error = 200;
        }
      }
      catch (RequestException $e) {
        $response['status'] = 'failure';
        $response['error'] = 'Something went wrong!';
        $error = 500;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }

    $response = new ResourceResponse($response, $error);
    // Disable api cache.
    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);
    $response->addCacheableDependency($disable_cache);

    return $response;
  }

}
