<?php

namespace Drupal\allianz_checkout\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "wirecard_transaction",
 *   label = @Translation("Wirecard transaction"),
 *   uri_paths = {
 *     "create" = "/api/v1/wirecard/transaction"
 *   }
 * )
 */
class WirecardTransaction extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new WirecardTransaction object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_checkout'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param string $payload
   *   Get data payload on request.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post($payload) {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $jwt = $authorization_token;
    $token = $transcoder->decode($jwt);
    $account_uid = $token->getPayload()->drupal->uid;
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if ((!empty($results) && $results['jwt_token'] == $jwt)) {
      if (!empty($payload['payment_method']) && !empty($payload['request_id'])) {
        if ($payload['payment_method'] == 'creditcard') {
          $merchant_id = $_ENV['WIRECARD_CCARD_MID'];
        }
        elseif ($payload['payment_method'] = 'sepadirectdebit') {
          $merchant_id = $_ENV['WIRECARD_SEPA_MID'];
        }
        $wirecard_url = $_ENV['WIRECARD_API_URL'];
        $request_url = $wirecard_url . '/engine/rest/merchants/' . $merchant_id . '/payments/search';
        $wirecard_username = $_ENV['WIRECARD_USERNAME'];
        $wirecard_password = $_ENV['WIRECARD_PASSWORD'];
        $wirecard_auth = $wirecard_username . ':' . $wirecard_password;
        $basic_auth = 'Basic ' . base64_encode($wirecard_auth);
        $headers = [
          'Authorization' => $basic_auth,
          'Accept'  => 'application/json',
        ];
        $data = ['payment.request-id' => $payload['request_id']];
        try {
          // Call drupal httpClient service.
          $client = \Drupal::httpClient();
          $request = $client->request('GET', $request_url, [
            'headers' => $headers,
            'http_errors' => FALSE,
            'query' => $data,
          ]);
          if ($request->getStatusCode() < 400) {
            $response['status'] = 'success';
            $response['data'] = json_decode($request->getBody(), TRUE);
            $error = $request->getStatusCode();
          }
          else {
            $response = json_decode($request->getBody(), TRUE);
            $error = $request->getStatusCode();
          }
        }
        catch (RequestException $e) {
          $response['status'] = 'failure';
          $response['error'] = 'Something went wrong!';
          $error = 500;
        }
      }
      else {
        $response['status'] = 'failure';
        $response['error'] = 'Something went wrong!';
        $error = 500;
      }
    }
    else {
      // Return 401 if jwt token does not exist or expired.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }
    $response = new ResourceResponse($response, $error);
    // Disable api cache.
    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);
    $response->addCacheableDependency($disable_cache);

    return $response;
  }

}
