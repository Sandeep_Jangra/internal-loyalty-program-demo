<?php

namespace Drupal\allianz_checkout\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\allianz_product\Controller\AllianzField;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\commerce_wishlist\Entity\WishlistItem;

/**
 * OrderComplete custom rest get api that retrun order detail after completion.
 *
 * @RestResource(
 *   id = "order_complete",
 *   label = @Translation("Order Complete"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/order/{order_id}/complete"
 *   }
 * )
 */
class OrderComplete extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new PunkteEinlosenFilter object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_custom'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
      );
  }

  /**
   * Responds to GET requests.
   *
   * @param string $order_id
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get($order_id) {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $jwt = $authorization_token;
    $token = $transcoder->decode($jwt);
    $account_uid = $token->getPayload()->drupal->uid;
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if ((!empty($results) && $results['jwt_token'] == $jwt)) {
      $order = Order::load($order_id);
      if (!empty($order) && $order->getCustomerId() == $account_uid) {
        try {
          $account = User::load($order->getCustomerId());
          if (!empty($account)) {
            // Load points statment object through services.
            $points_statement = \Drupal::service('allianz_prime_cloud.triggers');
            $data = ['type' => 'bonus'];
            $order_item_id = $order->getItems()[0]->id();
            $order_item = OrderItem::load($order_item_id);
            if (!empty($order_item)) {
              $member_id = $account->field_pc_member_id->value;
              $points = $points_statement->pointsStatement($member_id, $data);
              $variation = $order_item->getPurchasedEntity();
              $product = $variation->getProduct();
              $profile = $order->getBillingProfile();
              $allianz_field = new AllianzField();
              $product_url = \Drupal::service('path.alias_manager')->getAliasByPath('/product/' . $product->id());
              if ($product->bundle() == 'voucher') {
                $product_image = $allianz_field->getImageStyleUrl($product->field_voucher_image->target_id, 'default');
              }
              else {
                $product_image = $allianz_field->getImageStyleUrl($variation->field_product_image->target_id, 'default');
              }
              // Select query to get request user wishlist item count.
              $query = $connection->select('commerce_wishlist', 'w');
              $query->fields('w');
              $query->rightJoin('commerce_wishlist_item', 'wt', 'w.wishlist_id = wt.wishlist_id');
              $query->rightJoin('commerce_product_variation_field_data', 'cv', 'cv.variation_id = wt.purchasable_entity');
              $query->rightJoin('commerce_product_field_data', 'cp', 'cv.product_id = cp.product_id');
              $query->condition('w.uid', $account_uid);
              $query->condition('cp.status', 1);
              $results = $query->execute()->fetchAll();
              if ($order->state->value == 'completed') {
                $response['status'] = 'success';
                $response['data']['title'] = $product->getTitle();
                $response['data']['points_price'] = $variation->field_points_price->value;
                $response['data']['product_image'] = $product_image;
                $response['data']['delivery_time'] = $variation->field_delivery_time->value;
                // $response['data']['address']['first_name'] = $profile->address->given_name;
                // $response['data']['address']['last_name'] = $profile->address->family_name;
                // $response['data']['address']['street_address'] = $profile->address->address_line1;
                // $response['data']['address']['postal_code'] = $profile->address->postal_code;
                // $response['data']['address']['locality'] = $profile->address->locality;
                $response['data']['balance'] = $points['response_data']['data']['balance'];
                $response['data']['product_url'] = $product_url;
                $response['data']['total_wishlist_item'] = count($results);
                $error = 200;
              }
              else {
                // $pc_service = \Drupal::service('allianz_prime_cloud.triggers');
                // $date = date('Y-m-d');
                // $pc_data['date'] = $date . 'T00:00:00.000Z';
                // $pc_data['receiptId'] = $member_id;
                // $pc_data['amount'] = 1;
                // $product_title = $product->getTitle();
                // $description['order_id'] = $order_id;
                // $lp_desc['message'] = "Produkt gekauft " . $product_title;
                // $lp_desc['order_detail'] = $description;
                // $pc_data['description'] = json_encode($lp_desc);
                // $query_params = ['type' => 'bonus'];
                // $redeem_points = $pc_service->redeemPoints($member_id, $query_params, $pc_data);
                // if ($redeem_points['error'] < 400) {
                // Set order state completed.
                $order->set('state', 'completed');
                $order->save();
                // Remove product from wishlist
                // Load wishlist manager object through services.
                $wishlist_manager = \Drupal::service('commerce_wishlist.wishlist_manager');
                // Select query to get request user wishlist item count.
                $query = $connection->select('commerce_wishlist_item', 'wt');
                $query->fields('wt');
                $query->leftJoin('commerce_wishlist', 'w', 'w.wishlist_id = wt.wishlist_id');
                $query->condition('w.uid', $account_uid);
                $query->condition('wt.purchasable_entity', $variation->id());
                $results = $query->execute()->fetchAssoc();
                if (!empty($results)) {
                  $wishlist_item_id = $results['wishlist_item_id'];
                  $wishlist_item = WishlistItem::load($wishlist_item_id);
                  if (!empty($wishlist_item)) {
                    $wishlist = $wishlist_item->getWishlist();
                    $wishlist_manager->removeWishlistItem($wishlist, $wishlist_item);
                    $purchasable_entity = $wishlist_item->getPurchasableEntity();
                    $flag_service = \Drupal::service('flag');
                    $flag = $flag_service->getFlagById('wishlist');
                    // Flag an entity with a specific flag.
                    $flag_service->unflag($flag, $purchasable_entity);
                  }
                }
                // Order confirmation email to member.
                $warpit = \Drupal::service('warpit.client');
                $warpit_data = [
                  "senderToken" => "bae2566d-481a-4d7a-bca4-03df36f899f2",
                  "messageType" => "email",
                  "messageProviderId" => 1,
                  "templateId" => 1,
                  "users" => [[
                    "email" => $account->getEmail(),
                  ],
                  ],
                ];
                $status = $warpit->send($warpit_data);
                if ($status['succes'] != '1') {
                  // @todo
                  // If email trigger is not sent.
                }
                $response['status'] = 'success';
                $response['data']['title'] = $product->getTitle();
                $response['data']['points_price'] = $variation->field_points_price->value;
                $response['data']['product_image'] = $product_image;
                $response['data']['delivery_time'] = $variation->field_delivery_time->value;
                // $response['data']['address']['first_name'] = $profile->address->given_name;
                // $response['data']['address']['last_name'] = $profile->address->family_name;
                // $response['data']['address']['street_address'] = $profile->address->address_line1;
                // $response['data']['address']['postal_code'] = $profile->address->postal_code;
                // $response['data']['address']['locality'] = $profile->address->locality;
                $response['data']['balance'] = $points['response_data']['data']['balance'];
                $response['data']['product_url'] = $product_url;
                $response['data']['total_wishlist_item'] = count($results);
                $error = 200;
                // }
                // else {
                //   // Return 400 if member key does not exist on prime cloud.
                //   $response = [];
                //   $response['status'] = 'failure';
                //   $response['error'] = 'Invalid member key!';
                //   $error = 400;
                // }
              }
            }
            else {
              // Return 500 if requested order item does not exist.
              $response = [];
              $response['status'] = 'failure';
              $response['error'] = 'Something went wrong';
              $error = 500;
            }
          }
          else {
            // Return 500 if requested order user does not exist.
            $response['status'] = 'failure';
            $response['error'] = 'Something went wrong!';
            $error = 500;
          }
        }
        catch (RequestException $e) {
          $response['status'] = 'failure';
          $response['error'] = 'Something went wrong!';
          $error = 500;
        }
      }
      else {
        // Return 404 if requested order does not exist.
        $response['status'] = 'failure';
        $response['error'] = 'Order not found';
        $error = 404;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }
    $response = new ResourceResponse($response, $error);
    // Disable api cache.
    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);
    $response->addCacheableDependency($disable_cache);

    return $response;
  }

}
