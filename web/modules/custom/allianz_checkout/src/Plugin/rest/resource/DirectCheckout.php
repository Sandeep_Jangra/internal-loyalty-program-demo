<?php

namespace Drupal\allianz_checkout\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\allianz_product\Controller\AllianzField;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "direct_checkout",
 *   label = @Translation("Direct Checkout"),
 *   uri_paths = {
 *     "create" = "/api/v1/cart/direct-checkout"
 *   }
 * )
 */
class DirectCheckout extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new DirectCheckout object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_checkout'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param string $data
   *   Get data object on request.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function post($data) {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $jwt = $authorization_token;
    $token = $transcoder->decode($jwt);
    $account_uid = $token->getPayload()->drupal->uid;
    $account = User::load($account_uid);
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if ((!empty($results) && $results['jwt_token'] == $jwt)) {
      if (!empty($data) && !empty($data['variation_id']) && !empty($data['quantity'])) {
        try {
          $quantity = $data['quantity'];
          $variation_id = $data['variation_id'];
          $variation = ProductVariation::load($variation_id);
          $stock = (int) $variation->field_stock->value;
          // Get current time.
          $current_date = new DrupalDateTime();
          $current_date = $current_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
          if (!empty($variation) && $stock > 0) {
            $voucher = $variation->getProduct();
            $voucher_id = $voucher->id();
            $flap = $voucher->field_flap->value;
            $availibility_to = NULL;
            if ($flap == 'availability') {
              $end_date = new DrupalDateTime($voucher->field_availability_date->end_value, 'UTC');
              $end_date->setTimezone(timezone_open(date_default_timezone_get()));
              $availibility_to = $end_date->format('Y-m-d\TH:i:s');
            }
            if ($voucher->isPublished() && ($flap != 'availability' || ($flap == 'availability' && $availibility_to > $current_date))) {
              // Get voucher alias with voucher id.
              $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/product/' . $voucher_id);
              $response['status'] = 'success';
              $response['product']['variation_id'] = $variation_id;
              $response['product']['type'] = $voucher->bundle();
              $response['product']['title'] = ucfirst($voucher->getTitle());
              $response['product']['quantity'] = $quantity;
              $response['product']['product_url'] = $alias;
              $actual_price = round($variation->price->getValue()[0]['number'], 2);
              $currency_code = $variation->getPrice()->getCurrencyCode();
              $discount_percentage = $voucher->field_discount->value;
              // Calculating discount price and adding to response data.
              if (!empty($variation->price)) {
                $discount = round(($actual_price * $discount_percentage) / 100, 2);
                $purchase_price = round($actual_price - $discount, 2);
                $response['product']['actual_price'] = $actual_price;
                $response['product']['purchase_price'] = $purchase_price;
                $response['product']['discount'] = $discount;
                $response['product']['discount_per'] = $discount_percentage;
                $response['product']['currency'] = $currency_code;
              }
              // Storing flap related data.
              $response['product']['flap_details']['flap'] = $flap;
              $response['product']['flap_details']['stock'] = $stock;
              $response['product']['flap_details']['availability_date_to'] = $availibility_to;
              // Stock flap service to get stock flap status.
              $stock_flap_service = \Drupal::service('allianz_custom.stock_check');
              $flap_status = $stock_flap_service->getVoucherStockFlap($voucher_id);
              $out_of_stock = $stock_flap_service->isOutOfStock($voucher_id);
              $response['product']['flap_details']['flap_status'] = $flap_status;
              $response['product']['flap_details']['out_of_stock'] = $out_of_stock;
              // Create AllianzField class object.
              $allianz_field = new AllianzField();
              $voucher_image = reset($voucher->field_voucher_image->getValue());
              $response['product']['voucher_image'] = $allianz_field->getImageUrl($voucher_image['target_id'], 'checkout', 'checkout_mobile');
              $response['product']['voucher_image']['alt'] = $voucher_image['alt'];
              $response['product']['voucher_image']['title'] = $voucher_image['title'];
              // Using voucher session service to get session checkout limit.
              $voucher_service = \Drupal::service('allianz_voucher_session.session_level_checkout');
              $session_level_limit = $voucher_service->getSessionCheckoutLimit($account_uid, $voucher);
              $response['product']['session_level_limit'] = $session_level_limit;
              // Get delivery info from cart settings form.
              $config = \Drupal::config('allianz_checkout.cartsettings');
              // Static Text Info.
              $static_text = [
                'shipping_info' => $config->get('shipping_info'),
                'email_info' => $config->get('email_info'),
                'delivery_info' => $config->get('green_text'),
                'information_list' => $config->get('delivery_info'),
              ];
              $response['static_text'] = $static_text;
              $error = 200;
            }
            else {
              $response['status'] = 'failure';
              $response['error'] = 'Something went wrong!';
              $error = 500;
            }
          }
          else {
            $response['status'] = 'failure';
            $response['error'] = 'Something went wrong!';
            $error = 500;
          }
        }
        catch (RequestException $e) {
          $response['status'] = 'failure';
          $response['error'] = 'Something went wrong!';
          $error = 500;
        }
      }
      else {
        // Return 500 if requested request is invalid.
        $response['status'] = 'failure';
        $response['error'] = 'Something went wrong!';
        $error = 500;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }

    return new ModifiedResourceResponse($response, $error);
  }

}
