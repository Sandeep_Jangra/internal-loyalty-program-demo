<?php

namespace Drupal\allianz_checkout\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\jwt\Authentication\Event\JwtAuthGenerateEvent;
use Drupal\jwt\JsonWebToken\JsonWebToken;
use Drupal\jwt\Authentication\Event\JwtAuthEvents;

/**
 * Used to remove item from cart for requested user.
 *
 * @RestResource(
 *   id = "remove_from_cart_rest_resource",
 *   label = @Translation("Remove Item from cart rest resource"),
 *   uri_paths = {
 *     "create" = "/api/v1/cart/remove"
 *   }
 * )
 */
class RemoveFromCartRestResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new RemoveFromCartRestResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_checkout'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
      );
  }

  /**
   * Responds to POST requests.
   *
   * @param string $data
   *   Get data object on request.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post($data) {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $jwt = $authorization_token;
    $token = $transcoder->decode($jwt);
    $account_uid = $token->getPayload()->drupal->uid;
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if ((!empty($results) && $results['jwt_token'] == $jwt)) {
      if (!empty($data) && !empty($data['order_id']) && !empty($data['item_id'])) {
        try {
          $order = Order::load($data['order_id']);
          $account = User::load($account_uid);
          $item_id = $data['item_id'];
          $order_items = $order->order_items;
          $item_exists = FALSE;
          foreach ($order_items as $key => $value) {
            if (!empty($order_items) && $value->target_id == $item_id) {
              $item_exists = TRUE;
              break;
            }
          }
          // Proceed only if item exist in the cart.
          if ($item_exists) {
            if (!empty($account) && !empty($order)) {
              $order_item = OrderItem::load($item_id);
              $order->removeItem($order_item);
              $order_item->delete();
              $order->save();
              // Generate refresh token for user
              // Load dispatcher object through services.
              $dispatcher = \Drupal::service('event_dispatcher');
              $event = new JwtAuthGenerateEvent(new JsonWebToken());
              $dispatcher->dispatch(JwtAuthEvents::GENERATE, $event);
              // Generate jwt token for requested user.
              $event->addClaim('exp', strtotime('+2000 minutes'));
              $event->addClaim(['drupal', 'uid'], $account_uid);
              $jwt = $event->getToken();
              $refresh_token = $transcoder->encode($jwt);
              // Update refrest token to custom table.
              $token_updated = $connection->update('custom_jwt_token')
                ->fields([
                  'jwt_token' => $refresh_token,
                ])
                ->condition('uid', $account_uid, '=')
                ->execute();

              $response['status'] = 'success';
              $response['data']['token'] = $refresh_token;
              $error = 200;
            }
            else {
              // Return 500 if requested order item id is invalid.
              $response['status'] = 'success';
              $response['error'] = 'Something went wrong!';
              $error = 500;
            }
          }
          else {
            // Return 500 if requested request is invalid.
            $response['status'] = 'failure';
            $response['error'] = 'Item not present in the cart!';
            $error = 200;
          }
        }
        catch (RequestException $e) {
          $response['status'] = 'failure';
          $response['error'] = 'Something went wrong!';
          $error = 500;
        }
      }
      else {
        // Return 500 if requested request is invalid.
        $response['status'] = 'failure';
        $response['error'] = 'Something went wrong!';
        $error = 500;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }

    $response = new ModifiedResourceResponse($response, $error);
    return $response;
  }

}
