<?php

namespace Drupal\allianz_checkout;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Connex order entities.
 *
 * @ingroup allianz_checkout
 */
class ConnexOrderListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Connex order ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\allianz_checkout\Entity\ConnexOrder $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.connex_order.edit_form',
      ['connex_order' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
