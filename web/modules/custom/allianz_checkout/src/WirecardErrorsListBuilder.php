<?php

namespace Drupal\allianz_checkout;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Wirecard Error Logs entities.
 *
 * @ingroup allianz_checkout
 */
class WirecardErrorsListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Wirecard Error Logs ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\allianz_checkout\Entity\WirecardErrors $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.wirecard_errors.edit_form',
      ['wirecard_errors' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
