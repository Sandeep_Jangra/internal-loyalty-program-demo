<?php

namespace Drupal\allianz_checkout;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\OrderProcessorInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_order\Adjustment;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * CustomOrderProcessor that add the discount for voucher product.
 */
class CustomOrderProcessor implements OrderProcessorInterface {
  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new CartOrderProcessor object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function process(OrderInterface $order) {
    foreach ($order->getItems() as $order_item) {
      // SetAdjustment to empty initially.
      $order_item->setAdjustments([]);
      $product_variation = $order_item->getPurchasedEntity();
      if (!empty($product_variation) && $product_variation->bundle() != 'coupon_variation') {
        $product_id = $product_variation->getProductId();
        $product = Product::load($product_id);
        // Get current time.
        $current_date = new DrupalDateTime();
        $current_date = $current_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
        $flap = $product->field_flap->value;
        $availibility_to = NULL;
        if ($flap == 'availability') {
          $end_date = new DrupalDateTime($product->field_availability_date->end_value, 'UTC');
          $end_date->setTimezone(timezone_open(date_default_timezone_get()));
          $availibility_to = $end_date->format('Y-m-d\TH:i:s');
        }
        // Removed item from card if product is unpublished or expired.
        if (!$product->isPublished() || ($flap == 'availability' && $availibility_to < $current_date)) {
          $title = $order_item->getTitle();
          $order->removeItem($order_item);
          $order_item->delete();
          $this->messenger->addWarning(
            $this->t('Voucher @title is not available for purchase and was removed from your cart.',
              ['@title' => $title],
              ['context' => 'custom_checkout']
            )
          );
        }
        $product_type = $product->get('type')->getValue()[0]['target_id'];
        if ($product_type != 'coupon') {
          if ($product_type == 'voucher') {
            $currency = "INR";
          }
          else {
            $currency = "PTS";
          }
          $product_price = $order_item->getUnitPrice();
          $product_unit_price = $product_price->getNumber();
          $quantity = $order_item->getQuantity();
          if (!empty($product->field_discount->value)) {
            $variation_discount = $product->field_discount->value;
            $discounted_price = $quantity * round((($product_unit_price * $variation_discount) / 100), 2);
            $new_adjustment = $discounted_price;
            $adjustments = $order_item->getAdjustments();
            // Apply custom adjustment.
            $adjustments[] = new Adjustment([
              'type' => 'custom_discount_adjustment',
              'label' => 'Discount',
              'amount' => new Price('-' . $new_adjustment, $currency),
            ]);
            $order_item->setAdjustments($adjustments);
            $order_item->save();
          }
        }
      }
      else {
        // Removed item from card product is unavailable.
        if (!$product_variation instanceof ProductVariationInterface) {
          $title = $order_item->getTitle();
          $order->removeItem($order_item);
          $order_item->delete();
          $this->messenger->addWarning(
            $this->t('Voucher @title is not available for purchase and was removed from your cart.',
              ['@title' => $title],
              ['context' => 'custom_checkout']
            )
          );
        }
      }
    }
  }

}
