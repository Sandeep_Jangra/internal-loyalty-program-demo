<?php

namespace Drupal\allianz_checkout\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Connex order entities.
 *
 * @ingroup allianz_checkout
 */
class ConnexOrderDeleteForm extends ContentEntityDeleteForm {


}
