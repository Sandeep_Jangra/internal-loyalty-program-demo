<?php

namespace Drupal\allianz_checkout\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Wirecard Error Logs entities.
 *
 * @ingroup allianz_checkout
 */
class WirecardErrorsDeleteForm extends ContentEntityDeleteForm {


}
