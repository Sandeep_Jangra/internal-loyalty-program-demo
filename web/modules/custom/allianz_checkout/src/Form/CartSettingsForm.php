<?php

namespace Drupal\allianz_checkout\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CartSettingsForm.
 */
class CartSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'allianz_checkout.cartsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cart_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('allianz_checkout.cartsettings');
    $form['shipping_info'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Shipping Info'),
      '#description' => $this->t('Shipping Info Text'),
      '#default_value' => $config->get('shipping_info'),
    ];
    $form['email_info'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email Info'),
      '#description' => $this->t('Email Info'),
      '#default_value' => $config->get('email_info'),
    ];
    $form['green_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Delivery Info (Green Text)'),
      '#description' => $this->t('Additional information highlighted by green colour.'),
      '#default_value' => $config->get('green_text'),

    ];
    // Update no of fields as per saved configuration.
    if (!empty($config->get('delivery_info'))) {
      $num_fields = count($config->get('delivery_info'));
    }
    $num_fields = $form_state->get('num_fields');
    $form['#tree'] = TRUE;
    $form['text_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Add delivery information'),
      '#prefix' => '<div id="fieldset-wrapper">',
      '#suffix' => '</div>',
    ];
    if (!empty($config->get('delivery_info')) && empty($num_fields)) {
      $count = count($config->get('delivery_info'));
      $form_state->set('num_fields', $count);
      $num_fields = $count;
    }
    if (empty($num_fields)) {
      $num_fields = $form_state->set('num_fields', 1);
    }
    // Create multiple fields.
    for ($i = 0; $i < $num_fields; $i++) {
      $form['text_fieldset']['name'][$i] = [
        '#type' => 'textfield',
        '#title' => t('Enter text'),
        '#default_value' => $config->get('delivery_info')[$i],
      ];
    }
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['text_fieldset']['actions']['add_name'] = [
      '#type' => 'submit',
      '#value' => t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'fieldset-wrapper',
      ],
    ];
    if ($num_fields > 1) {
      $form['text_fieldset']['actions']['remove_name'] = [
        '#type' => 'submit',
        '#value' => t('Remove one'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'fieldset-wrapper',
        ],
      ];
    }
    $form_state->setCached(FALSE);
    return parent::buildForm($form, $form_state);
  }

  /**
   * Increase total field count by one.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $num_fields = $form_state->get('num_fields');
    $add_button = $num_fields + 1;
    $form_state->set('num_fields', $add_button);
    $form_state->setRebuild();
  }

  /**
   * Creates new field.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    $num_fields = $form_state->get('num_fields');
    return $form['text_fieldset'];
  }

  /**
   * Removes one field.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $num_fields = $form_state->get('num_fields');
    if ($num_fields > 1) {
      $remove_button = $num_fields - 1;
      $form_state->set('num_fields', $remove_button);
    }
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('allianz_checkout.cartsettings')
      ->set('shipping_info', $form_state->getValue('shipping_info'))
      ->set('email_info', $form_state->getValue('email_info'))
      ->set('green_text', $form_state->getValue('green_text'))
      ->set('delivery_info', $form_state->getValue('text_fieldset')['name'])
      ->save();
  }

}
