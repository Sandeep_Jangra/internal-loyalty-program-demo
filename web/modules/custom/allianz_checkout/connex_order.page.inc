<?php

/**
 * @file
 * Contains connex_order.page.inc.
 *
 * Page callback for Connex order entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Connex order templates.
 *
 * Default template: connex_order.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_connex_order(array &$variables) {
  // Fetch ConnexOrder Entity Object.
  $connex_order = $variables['elements']['#connex_order'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
