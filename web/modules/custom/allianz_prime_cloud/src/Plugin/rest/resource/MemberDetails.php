<?php

namespace Drupal\allianz_prime_cloud\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\user\Entity\User;

/**
 * MemberDetails that return full member details for requested user.
 *
 * @RestResource(
 *   id = "member_details",
 *   label = @Translation("Member Details"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/member/details"
 *   }
 * )
 */
class MemberDetails extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new MemberDetails object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_custom'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
      );
  }

  /**
   * Responds to GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get() {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $jwt = $authorization_token;
    $token = $transcoder->decode($jwt);
    $account_uid = $token->getPayload()->drupal->uid;
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if ((!empty($results) && $results['jwt_token'] == $jwt)) {
      try {
        $prime_cloud = \Drupal::service('allianz_prime_cloud.triggers');
        $account = User::load($account_uid);
        if (!empty($account)) {
          $member_id = $account->field_pc_member_id->value;
          $member_details = $prime_cloud->memberDetails($member_id);
          if ($member_details['error'] < 400) {
            $response['status'] = 'success';
            $response['data']['title'] = $member_details['response_data']['data']['owner']['properties']['Title'];
            $response['data']['salutation'] = $member_details['response_data']['data']['owner']['properties']['Salutation'];
            $response['data']['email'] = $member_details['response_data']['data']['owner']['properties']['Email'];
            $response['data']['first_name'] = $member_details['response_data']['data']['owner']['properties']['FirstName'];
            $response['data']['last_name'] = $member_details['response_data']['data']['owner']['properties']['LastName'];
            $response['data']['address'] = $member_details['response_data']['data']['owner']['address'];
            $response['data']['street_address'] = $member_details['response_data']['data']['owner']['properties']['Street'];
            $response['data']['house_number'] = $member_details['response_data']['data']['owner']['properties']['Number'];
            $response['data']['postal_code'] = $member_details['response_data']['data']['owner']['properties']['PostalCode'];
            $response['data']['place'] = $member_details['response_data']['data']['owner']['properties']['City'];
            $response['data']['locality'] = $member_details['response_data']['data']['owner']['properties']['Street'];
            $response['data']['country'] = $member_details['response_data']['data']['owner']['properties']['Country'];

            $error = 200;
          }
          else {
            // Return 400 if requested member key does not exist on prime cloud.
            $response['status'] = 'failure';
            $response['error'] = 'Invalid member key!';
            $error = 400;
          }
        }
        else {
          // Return 404 if requested user does not exist.
          $response['status'] = 'failure';
          $response['error'] = 'User does not exits.';
          $error = 404;
        }
      }
      catch (RequestException $e) {
        $response['status'] = 'failure';
        $response['error'] = 'Something went wrong!';
        $error = 500;
      }
    }
    else {
      // Return 401 if jwt token does not exist or expired.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }
    $response = new ResourceResponse($response, $error);
    // Disable api cache.
    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);
    $response->addCacheableDependency($disable_cache);

    return $response;
  }

}
