<?php

namespace Drupal\allianz_prime_cloud\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\Client;

/**
 * Class PrimeCloudAPI used to get data from prime cloud api.
 *
 * @package \Drupal\allianz_prime_cloud\Services
 */
class PrimeCloudAPI {

  /**
   * Config Factory Interface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Guzzle HTTP Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * PrimeCloudAPI constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Config Factory Interface.
   * @param \GuzzleHttp\Client $client
   *   Guzzle HTTP Client.
   */
  public function __construct(LoggerChannelFactoryInterface $logger, ConfigFactoryInterface $config, Client $client) {
    $this->logger = $logger->get('allianz_prime_cloud');
    $this->client = $client;
  }

  /**
   * Retrieves the API key.
   */
  protected function getApiKey() {
    return $_ENV['PC_BASE_API_KEY'];
  }

  /**
   * Retrieves the API server URL.
   */
  protected function getApiServerUrl() {
    return $_ENV['PC_BASE_URL'];
  }

  /**
   * Redeem member points.
   */
  public function redeemPoints($id, $query_params, $data) {
    $request_url = $this->getApiServerUrl() . '/api/v10/deposits/' . $id . '/redeem';
    $headers = [
      'Authorization' => $this->getApiKey(),
      'Content-Type'  => 'application/json',
    ];

    try {
      $request = $this->client->request('POST', $request_url, [
        'headers' => $headers,
        'http_errors' => FALSE,
        'query' => $query_params,
        'json' => $data,
      ]);
      if ($request->getStatusCode() < 400) {
        $response['response_data'] = json_decode($request->getBody(), TRUE);
        $response['error'] = $request->getStatusCode();
      }
      else {
        $response['error'] = $request->getStatusCode();
      }
    }
    catch (RequestException $exception) {
      $this->logger->error($exception->getMessage());
      $response['error'] = 500;
    }

    return $response;
  }

  /**
   * Get member points statement.
   */
  public function pointsStatement($id, $data) {
    $request_url = $this->getApiServerUrl() . '/api/v10/deposits/' . $id . '/statement';
    $headers = [
      'Authorization' => $this->getApiKey(),
      'Content-Type'  => 'application/json',
    ];
    try {
      $request = $this->client->request('GET', $request_url, [
        'headers' => $headers,
        'http_errors' => FALSE,
        'query' => $data,
      ]);
      if ($request->getStatusCode() < 400) {
        $response['response_data'] = json_decode($request->getBody(), TRUE);
        $response['error'] = $request->getStatusCode();
      }
      else {
        $response['error'] = $request->getStatusCode();
      }
    }
    catch (RequestException $exception) {
      $this->logger->error($exception->getMessage());
      $response['error'] = 500;
    }

    return $response;
  }

  /**
   * Get member points statement.
   */
  public function memberDetails($id) {
    $request_url = $this->getApiServerUrl() . '/api/v10/memberships/' . $id . '/details';
    $headers = [
      'Authorization' => $this->getApiKey(),
      'Content-Type'  => 'application/json',
    ];
    try {
      $request = $this->client->request('GET', $request_url, ['headers' => $headers, 'http_errors' => FALSE]);
      if ($request->getStatusCode() < 400) {
        $response['response_data'] = json_decode($request->getBody(), TRUE);
        $response['error'] = $request->getStatusCode();
      }
      else {
        $response['error'] = $request->getStatusCode();
      }
    }
    catch (RequestException $exception) {
      $this->logger->error($exception->getMessage());
      $response['error'] = 500;
    }

    return $response;
  }

}
