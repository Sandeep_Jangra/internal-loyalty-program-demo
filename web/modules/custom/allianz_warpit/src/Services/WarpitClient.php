<?php

namespace Drupal\allianz_warpit\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\allianz_warpit\Model\Source;
use Drupal\allianz_warpit\Model\SourceTable;
use Drupal\allianz_warpit\Model\Table;
use Drupal\allianz_warpit\Model\TriggerData;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class WarpitClient.
 *
 * @package \Drupal\warpit\Services
 */
class WarpitClient {

  /**
   * Config Factory Interface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Guzzle HTTP Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Warpit constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Config Factory Interface.
   * @param \GuzzleHttp\Client $client
   *   Guzzle HTTP Client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   A logger instance.
   */
  public function __construct(ConfigFactoryInterface $config, Client $client, LoggerChannelFactoryInterface $logger) {
    $this->client = $client;
    $this->config = $config->get('allianz_warpit.settings');
    $this->logger = $logger->get('allianz_warpit');
  }

  /**
   * Retrieves the base URI of Warpit.
   */
  protected function getBaseUri() {
    $base_uri = $this->config->get('base_uri');
    if ($base_uri) {
      $base_uri = str_replace('{{instance-url}}', $this->config->get('instance_url'), $base_uri);
    }
    return $base_uri;
  }

  /**
   * Retrieves the authentication credentials of Warpit.
   */
  protected function getAuthCredentials() {
    return [
      $this->config->get('auth.username'),
      $this->config->get('auth.password'),
    ];
  }

  /**
   * Gets a source.
   *
   * @param string $id
   *   Source ID.
   *
   * @return \Drupal\warpit\Model\Source|false
   *   Source table instance.
   */
  public function getSource($id) {
    $uri = $this->getBaseUri() . str_replace('{sourceId}', $id, $this->config->get('endpoints.source.individual.url'));
    try {
      $response = $this->client->get($uri, [
        'auth' => $this->getAuthCredentials(),
      ]);
      if ($response->getStatusCode() === Response::HTTP_OK) {
        $data = json_decode($response->getBody(), TRUE);
        return new Source($data);
      }
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }
    return FALSE;
  }

  /**
   * Creates a new source.
   *
   * @param string $sourceName
   *   Source Name.
   *
   * @return \Drupal\warpit\Model\Source|false
   *   Source table instance.
   */
  public function createSource($sourceName) {
    $uri = $this->getBaseUri() . $this->config->get('endpoints.source.base.url');
    try {
      $response = $this->client->post($uri, [
        'auth' => $this->getAuthCredentials(),
        'json' => ['name' => $sourceName],
      ]);
      if ($response->getStatusCode() === Response::HTTP_OK) {
        $data = json_decode($response->getBody(), TRUE);
        return new Source($data);
      }
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }
    return FALSE;
  }

  /**
   * Creates a source table in Warpit.
   *
   * @param int $sourceId
   *   Source ID.
   * @param \Drupal\warpit\Model\Table $table
   *   Source table instance.
   *
   * @return \Drupal\warpit\Model\SourceTable|false
   *   Source table instance.
   */
  public function createTable($sourceId, Table $table) {
    $uri = $this->getBaseUri() . str_replace('{sourceId}', $sourceId, $this->config->get('endpoints.source.table_base.url'));
    try {
      $response = $this->client->post($uri, [
        'auth' => $this->getAuthCredentials(),
        'json' => (array) $table,
      ]);
      if ($response->getStatusCode() === Response::HTTP_OK) {
        $data = json_decode($response->getBody(), TRUE);
        return new SourceTable($data);
      }
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }
    return FALSE;
  }

  /**
   * Add columns to a table.
   *
   * @param int $tableId
   *   Table ID.
   * @param array $columns
   *   Table columns.
   *
   * @return bool
   *   Whether the operation was successful or not.
   */
  public function addColumns($tableId, array $columns) {
    $uri = $this->getBaseUri() . str_replace('{tableId}', $tableId, $this->config->get('endpoints.table.columns.url'));
    try {
      $response = $this->client->post($uri, [
        'auth' => $this->getAuthCredentials(),
        'json' => $columns,
      ]);
      if ($response->getStatusCode() === Response::HTTP_OK) {
        $data = json_decode($response->getBody(), TRUE);
        return isset($data['success']) && $data['success'];
      }
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }
    return FALSE;
  }

  /**
   * Uploads a DSV of users to Warpit.
   *
   * @param int $table_id
   *   Table ID.
   * @param string $file_name
   *   File name.
   *
   * @return bool|false
   *   Whether the operation was successful or not.
   */
  public function uploadUsers($table_id, $file_name) {
    $uri = $this->getBaseUri() . str_replace('{tableId}', $table_id, $this->config->get('endpoints.table.upload.url'));
    try {
      $response = $this->client->put($uri, [
        'auth' => $this->getAuthCredentials(),
        'body' => fopen($file_name, "r"),
        'headers' => ['Content-Type' => 'application/binary'],
      ]);
      if ($response->getStatusCode() === Response::HTTP_OK) {
        $data = json_decode($response->getBody(), TRUE);
        return isset($data['success']) && $data['success'];
      }
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }
    return FALSE;
  }

  /**
   * Creates single user to Warpit.
   *
   * @param int $table_id
   *   Table ID.
   * @param array $user_data
   *   File name.
   *
   * @return bool|false
   *   Whether the operation was successful or not.
   */
  public function createUser($table_id, array &$user_data) {
    $uri = $this->getBaseUri() . str_replace('{tableId}', $table_id, $this->config->get('endpoints.table.user.url'));
    try {
      $response = $this->client->put($uri, [
        'auth' => $this->getAuthCredentials(),
        'body' => json_encode($user_data),
        'headers' => ['Content-Type' => 'application/json'],
      ]);
      if ($response->getStatusCode() === Response::HTTP_OK) {
        $data = json_decode($response->getBody(), TRUE);
        return isset($data['success']) && $data['success'];
      }
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }
    return FALSE;
  }

  /**
   * Send instant message from campaign.
   *
   * @param \Drupal\warpit\Model\TriggerData $data
   *   Data.
   *
   * @return bool|false
   *   Whether the operation was successful or not.
   */
  public function instantTrigger(TriggerData $data) {
    $uri = $this->getBaseUri() . $this->config->get('endpoints.instant_trigger.base.url');
    try {
      $response = $this->client->post($uri, [
        'auth' => $this->getAuthCredentials(),
        'json' => (array) $data,
      ]);
      if ($response->getStatusCode() === Response::HTTP_OK) {
        $data = json_decode($response->getBody(), TRUE);
        return isset($data['success']) && $data['success'];
      }
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }
    return FALSE;
  }

  /**
   * Send instant message from campaign.
   *
   * @param \Drupal\warpit\Model\TriggerData $data
   *   Data.
   *
   * @return bool|false
   *   Whether the operation was successful or not.
   */
  public function send($data) {
    $uri = $this->getBaseUri() . '/send';
    try {
      $response = $this->client->post($uri, [
        'auth' => $this->getAuthCredentials(),
        'json' => $data,
      ]);
      if ($response->getStatusCode() === Response::HTTP_OK) {
        $data = json_decode($response->getBody(), TRUE);
        return $data;
      }
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }
    return FALSE;
  }

}
