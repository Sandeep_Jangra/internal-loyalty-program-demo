<?php

namespace Drupal\allianz_warpit\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class WarpitConfigForm.
 *
 * @package Drupal\warpit\Form
 */
class WarpitConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'warpit_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'allianz_warpit.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $config = $this->config('allianz_warpit.settings');
    $form['auth']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $config->get('auth.username'),
      '#description' => $this->t('Basic Authentication Username'),
      '#required' => TRUE,
    ];
    $form['auth']['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#default_value' => $config->get('auth.password'),
      '#description' => $this->t('Basic Authentication Password'),
      '#required' => TRUE,
    ];
    $form['instance_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Instance URL'),
      '#default_value' => $config->get('instance_url'),
      '#description' => $this->t('Warpit Instance URL'),
      '#required' => TRUE,
    ];
    $form['file_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('File Path'),
      '#default_value' => $config->get('file_path'),
      '#description' => $this->t('Path of the users file'),
      '#required' => FALSE,
    ];
    $form['user_table_id'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('User Table ID'),
      '#default_value' => $config->get('user_table_id'),
      '#description' => $this->t('User Table ID'),
      '#required' => FALSE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('allianz_warpit.settings')
      ->set('auth.username', $form_state->getValue('username'))
      ->set('auth.password', $form_state->getValue('password'))
      ->set('instance_url', $form_state->getValue('instance_url'))
      ->set('file_path', $form_state->getValue('file_path'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
