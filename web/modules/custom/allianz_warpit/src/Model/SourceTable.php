<?php

namespace Drupal\allianz_warpit\Model;

/**
 * Class SourceTable.
 *
 * @package \Drupal\allianz_warpit\Model
 */
class SourceTable extends Table {

  /**
   * Display Name.
   *
   * @var string
   */
  public $id;

  /**
   * Display Name.
   *
   * @var string
   */
  public $display_name;

  /**
   * Display Name.
   *
   * @var string
   */
  public $fk_data_table_category;

  /**
   * Display Name.
   *
   * @var string
   */
  public $fk_data_source;

  /**
   * Class constructor.
   *
   * @param array $data
   *   Data.
   */
  public function __construct(array $data) {
    parent::__construct($data['name'], $data['type']);
    $this->id = $data['id'];
    $this->display_name = $data['display_name'];
    $this->fk_data_source = $data['fk_data_source'];
    $this->fk_data_table_category = $data['fk_data_table_category'];
  }

}
