<?php

namespace Drupal\allianz_warpit\Model;

/**
 * Class TableColumn.
 *
 * @package \Drupal\allianz_warpit\Model
 */
class TableColumn {

  /**
   * Column name ID.
   *
   * @var string
   */
  public $column;

  /**
   * Type Identifier.
   *
   * @var string
   */
  public $type;

  /**
   * Column display name ID.
   *
   * @var string
   */
  public $displayName;

  /**
   * Is personalized.
   *
   * @var bool
   */
  public $isPersonalized;

  /**
   * Is visible.
   *
   * @var bool
   */
  public $isVisible;

  /**
   * Class constructor.
   *
   * @param array $data
   *   Column data.
   */
  public function __construct(array $data) {
    $this->column = $data['column'];
    $this->type = $data['type'];
    $this->displayName = $data['displayName'];
    $this->isPersonalized = $data['isPersonalized'];
    $this->isVisible = $data['isVisible'];
  }

}
