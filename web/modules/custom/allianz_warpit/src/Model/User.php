<?php

namespace Drupal\allianz_warpit\Model;

/**
 * Class User.
 *
 * @package \Drupal\allianz_warpit\Model
 */
class User {

  /**
   * User ID.
   *
   * @var string
   */
  public $id;

  /**
   * First Name.
   *
   * @var string
   */
  public $firstName;

  /**
   * Last Name.
   *
   * @var string
   */
  public $lastName;

  /**
   * Email.
   *
   * @var string
   */
  public $email;

  /**
   * Class constructor.
   *
   * @param array $data
   *   Data.
   */
  public function __construct(array $data) {
    $this->id = $data['id'];
    $this->firstName = $data['firstName'];
    $this->lastName = $data['lastName'];
    $this->email = $data['email'];
  }

}
