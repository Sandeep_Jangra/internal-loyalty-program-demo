<?php

namespace Drupal\allianz_warpit\Model;

/**
 * Class TriggerData.
 *
 * @package \Drupal\allianz_warpit\Model
 */
class TriggerData {

  /**
   * Campaign ID.
   *
   * @var int
   */
  public $campaignId;

  /**
   * Campaign Type.
   *
   * @var string
   */
  public $campaignType;

  /**
   * User IDs.
   *
   * @var array
   */
  public $userIds;

  /**
   * Users.
   *
   * @var array
   */
  public $users;

  /**
   * Table machine name.
   *
   * @var string
   */
  public $table;

  /**
   * Class constructor.
   *
   * @param array $data
   *   Data.
   */
  public function __construct(array $data) {
    $this->campaignId = $data['campaignId'];
    $this->campaignType = $data['campaignType'];
    $this->userIds = $data['userIds'];
    $this->users = $data['users'];
    $this->table = $data['table'];
  }

}
