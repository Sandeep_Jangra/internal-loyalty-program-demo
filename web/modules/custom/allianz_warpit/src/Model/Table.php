<?php

namespace Drupal\allianz_warpit\Model;

/**
 * Class Table.
 *
 * @package \Drupal\allianz_warpit\Model
 */
class Table {

  /**
   * Table name.
   *
   * @var string
   */
  public $name;

  /**
   * Table type.
   *
   * @var string
   */
  public $type;

  /**
   * Class constructor.
   *
   * @param string $name
   *   Table name.
   * @param string $type
   *   Table type.
   */
  public function __construct($name, $type) {
    $this->name = $name;
    $this->type = $type;
  }

}
