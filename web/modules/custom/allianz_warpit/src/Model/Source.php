<?php

namespace Drupal\allianz_warpit\Model;

/**
 * Class Source.
 *
 * @package \Drupal\allianz_warpit\Model
 */
class Source {

  /**
   * ID.
   *
   * @var int
   */
  public $id;

  /**
   * Name.
   *
   * @var string
   */
  public $name;

  /**
   * Active status.
   *
   * @var bool
   */
  public $active;

  /**
   * Order.
   *
   * @var int
   */
  public $order;

  /**
   * Class constructor.
   *
   * @param array $data
   *   Data.
   */
  public function __construct(array $data) {
    $this->id = $data['id'];
    $this->name = $data['name'];
    $this->active = $data['active'];
    $this->order = $data['order'];
  }

}
