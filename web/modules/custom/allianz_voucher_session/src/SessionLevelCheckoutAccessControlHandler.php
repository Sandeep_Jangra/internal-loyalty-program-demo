<?php

namespace Drupal\allianz_voucher_session;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Session Level Checkout entity.
 *
 * @see \Drupal\allianz_voucher_session\Entity\SessionLevelCheckout.
 */
class SessionLevelCheckoutAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\allianz_voucher_session\Entity\SessionLevelCheckoutInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished session level checkout entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published session level checkout entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit session level checkout entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete session level checkout entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add session level checkout entities');
  }

}
