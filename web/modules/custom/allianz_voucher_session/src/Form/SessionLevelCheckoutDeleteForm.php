<?php

namespace Drupal\allianz_voucher_session\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Session Level Checkout entities.
 *
 * @ingroup allianz_voucher_session
 */
class SessionLevelCheckoutDeleteForm extends ContentEntityDeleteForm {


}
