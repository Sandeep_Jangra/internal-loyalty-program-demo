<?php

namespace Drupal\allianz_voucher_session;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for session_level_checkout.
 */
class SessionLevelCheckoutTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
