<?php

namespace Drupal\allianz_voucher_session\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Session Level Checkout entities.
 */
class SessionLevelCheckoutViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
