<?php

namespace Drupal\allianz_voucher_session\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Session Level Checkout entities.
 *
 * @ingroup allianz_voucher_session
 */
interface SessionLevelCheckoutInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Session Level Checkout name.
   *
   * @return string
   *   Name of the Session Level Checkout.
   */
  public function getName();

  /**
   * Sets the Session Level Checkout name.
   *
   * @param string $name
   *   The Session Level Checkout name.
   *
   * @return \Drupal\allianz_voucher_session\Entity\SessionLevelCheckoutInterface
   *   The called Session Level Checkout entity.
   */
  public function setName($name);

  /**
   * Gets the Session Level Checkout creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Session Level Checkout.
   */
  public function getCreatedTime();

  /**
   * Sets the Session Level Checkout creation timestamp.
   *
   * @param int $timestamp
   *   The Session Level Checkout creation timestamp.
   *
   * @return \Drupal\allianz_voucher_session\Entity\SessionLevelCheckoutInterface
   *   The called Session Level Checkout entity.
   */
  public function setCreatedTime($timestamp);

}
