<?php

namespace Drupal\allianz_voucher_session\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;

/**
 * A handler to provide a variation attribute field.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("voucher_stock_alert")
 */
class VoucherStockAlert extends FieldPluginBase {
  /**
   * The current display.
   *
   * @var string
   *   The current display of the view.
   */
  protected $currentDisplay;

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->currentDisplay = $view->current_display;
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $voucher = $values->_entity;
    $voucher_stock_alert = "false";
    $voucher_id = $voucher->id();
    if (!empty($voucher_id)) {
      // Query to validate coupon lifetime purchase limit.
      $request = \Drupal::request();
      $authorization_token = trim(str_replace('Bearer ', '', $request->headers->get('Authorization')));
      // Load jwt transcoder object through services.
      $transcoder = \Drupal::service('jwt.transcoder');
      $token = $transcoder->decode($authorization_token);
      $account_uid = $token->getPayload()->drupal->uid;
      // Check if Stock Alert Entity for this user and variation already exist.
      $stock_alert_service = \Drupal::service('allianz_custom.stock_check');
      if ($stock_alert_service->existStockAlert($voucher_id, $account_uid)) {
        $voucher_stock_alert = "true";
      }
    }
    return $voucher_stock_alert;
  }

}
