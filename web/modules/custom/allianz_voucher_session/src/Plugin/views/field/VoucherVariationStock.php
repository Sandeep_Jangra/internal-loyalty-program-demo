<?php

namespace Drupal\allianz_voucher_session\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\commerce_product\Entity\Product;

/**
 * A handler to provide a variation attribute field.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("voucher_variation_stock")
 */
class VoucherVariationStock extends FieldPluginBase {
  /**
   * The current display.
   *
   * @var string
   *   The current display of the view.
   */
  protected $currentDisplay;

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->currentDisplay = $view->current_display;
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $voucher = $values->_entity;
    $voucher_stock = 0;
    $voucher_id = $voucher->id();
    if (!empty($voucher_id)) {
      $voucher = Product::load($voucher_id);
      if ($voucher->bundle() == 'voucher') {
        // Get default variation ids.
        $variations = $voucher->getVariations();
        if (!empty($variations)) {
          foreach ($variations as $key => $variation) {
            $voucher_stock = $voucher_stock + $variation->field_stock->value;
          }
        }
      }
    }
    return $voucher_stock;
  }

}
