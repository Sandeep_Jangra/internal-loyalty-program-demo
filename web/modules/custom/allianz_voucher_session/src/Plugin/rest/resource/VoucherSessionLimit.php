<?php

namespace Drupal\allianz_voucher_session\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\allianz_voucher_session\Entity\SessionLevelCheckout;
use Drupal\user\Entity\User;
use Drupal\commerce_product\Entity\Product;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Provides a resource to manage voucher session level purchase limit.
 *
 * @RestResource(
 *   id = "voucher_session_limit",
 *   label = @Translation("Voucher Session Limit"),
 *   uri_paths = {
 *     "create" = "/api/v1/voucher/session/limit"
 *   }
 * )
 */
class VoucherSessionLimit extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new VoucherSessionLimit object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_voucher_session'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param string $payload
   *   Get data object from request.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post($payload) {
    if (!empty($payload) && array_key_exists("voucher_id", $payload) && array_key_exists("quantity", $payload)) {
      $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
      // Load jwt transcoder object through services.
      $transcoder = \Drupal::service('jwt.transcoder');
      $jwt = $authorization_token;
      $token = $transcoder->decode($jwt);
      $account_uid = $token->getPayload()->drupal->uid;
      // Get db connection.
      $connection = Database::getConnection();
      // Select query to get requested user token for authentication.
      $query = $connection->select('custom_jwt_token', 'c')
        ->fields('c', ['uid', 'jwt_token'])
        ->condition('c.uid', $account_uid, '=')
        ->execute();
      $results = $query->fetchAssoc();
      if ((!empty($results) && $results['jwt_token'] == $jwt)) {
        // Create session checkout object with attached file.
        $voucher = Product::load($payload['voucher_id']);
        if (!empty($voucher) && $voucher->isPublished() == 1) {
          if (!empty($voucher->field_session_level_limit) && !empty($voucher->field_session_level_limit->value)) {
            $session_level_limit = $voucher->field_session_level_limit->value;
            $purchased_quantity = 0;
            // Select query to get session purchased limit for voucher.
            $query = \Drupal::entityQuery('session_level_checkout');
            $query->condition('field_user_id', $account_uid, '=');
            $query->condition('field_voucher_id', $payload['voucher_id'], '=');
            $query->accessCheck(FALSE);
            $session_limit_result = $query->execute();
            if (!empty($session_limit_result)) {
              foreach ($session_limit_result as $session_limit_key => $session_limit_value) {
                $session_limit = SessionLevelCheckout::load($session_limit_value);
                if (!empty($session_limit)) {
                  if (!empty($session_limit->field_quantity) && !empty($session_limit->field_quantity->value)) {
                    $purchased_quantity = $purchased_quantity + $session_limit->field_quantity->value;
                  }
                }
              }
            }
            $session_level_limit = $session_level_limit - $purchased_quantity;
            if ($payload['quantity'] <= $session_level_limit) {
              $account = User::load($account_uid);
              $session_checkout = SessionLevelCheckout::create([
                'name'        => 'session_limit_' . $account_uid,
                'field_user_id'        => $account,
                'field_voucher_id'     => $voucher,
                'field_quantity'     => $payload['quantity'],
              ]);
              $session_checkout->save();
              $response['status'] = 'success';
              $error = 200;
            }
            else {
              // Return 500 if product bundle is from another bundle.
              $response['status'] = 'failure';
              $response['error'] = 'You can not purchase more than session level limit';
              $error = 500;
            }
          }
          else {
            // Return 500 if product bundle is from another bundle.
            $response['status'] = 'failure';
            $response['error'] = 'Something went wrong!';
            $error = 500;
          }
        }
        else {
          // Return 404 if requested url does not exists.
          $response['status'] = 'failure';
          $response['error'] = 'Requested voucher does not exist or unpublished.';
          $error = 404;
        }
      }
      else {
        // Return 401 if requested token is invalid.
        $response['status'] = 'failure';
        $response['error'] = 'Permission denied';
        $error = 401;
      }
    }
    else {
      // Return 500 if requested params is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Something went wrong!';
      $error = 500;
    }

    $response = new ResourceResponse($response, $error);
    // Disable api cache.
    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);
    $response->addCacheableDependency($disable_cache);

    return $response;
  }

}
