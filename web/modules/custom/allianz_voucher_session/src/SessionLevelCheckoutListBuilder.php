<?php

namespace Drupal\allianz_voucher_session;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Session Level Checkout entities.
 *
 * @ingroup allianz_voucher_session
 */
class SessionLevelCheckoutListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Session Level Checkout ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\allianz_voucher_session\Entity\SessionLevelCheckout $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.session_level_checkout.edit_form',
      ['session_level_checkout' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
