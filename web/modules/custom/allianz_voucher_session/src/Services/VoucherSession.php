<?php

namespace Drupal\allianz_voucher_session\Services;

use Drupal\allianz_voucher_session\Entity\SessionLevelCheckout;
use Drupal\user\Entity\User;

/**
 * Class VocuherSession used to define voucher session related methods.
 *
 * @package \Drupal\allianz_voucher_session\Services
 */
class VoucherSession {

  /**
   * VoucherSession constructor.
   */
  public function __construct() {

  }

  /**
   * Returns Session Level Checkout Limit.
   */
  public function getSessionCheckoutLimit($account_uid, $product) {
    $session_level_limit = 'not_exist';
    if (!empty($product->field_session_level_limit) && !empty($product->field_session_level_limit->value)) {
      $session_level_limit = (int) $product->field_session_level_limit->value;
      // Select query to get session purchased limit for product.
      $query = \Drupal::entityQuery('session_level_checkout');
      $query->condition('field_user_id', $account_uid, '=');
      $query->condition('field_voucher_id', $product->id(), '=');
      $query->accessCheck(FALSE);
      $session_limit_result = $query->execute();
      if (!empty($session_limit_result)) {
        $purchased_quantity = 0;
        foreach ($session_limit_result as $session_limit_key => $session_limit_value) {
          $session_limit = SessionLevelCheckout::load($session_limit_value);
          if (!empty($session_limit)) {
            if (!empty($session_limit->field_quantity) && !empty($session_limit->field_quantity->value)) {
              $purchased_quantity = $purchased_quantity + $session_limit->field_quantity->value;
            }
          }
        }
        $session_level_limit = $session_level_limit - $purchased_quantity;
      }
      // Calculate session level limit w.r.t added items in the cart.
      $account = User::load($account_uid);
      $storeId = 1;
      $entityManager = \Drupal::entityManager();
      $cartManager = \Drupal::service('commerce_cart.cart_manager');
      $cartProvider = \Drupal::service('commerce_cart.cart_provider');
      $store = \Drupal::entityTypeManager()
        ->getStorage('commerce_store')
        ->load($storeId);
      $cart = $cartProvider->getCart('default', $store, $account);
      if (!empty($cart) && !empty($cart->getItems())) {
        $order_items = $cart->getItems();
        foreach ($order_items as $item_key => $order_item) {
          $variation = $order_item->getPurchasedEntity();
          $voucher_id = $variation->getProductId();
          if ($voucher_id == $product->id()) {
            $quantity = (int) $order_item->quantity->value;
            $session_level_limit = $session_level_limit - $quantity;
          }
        }
      }
    }

    return $session_level_limit;
  }

  /**
   * Returns Session Level Checkout Limit.
   */
  public function getCartListSessionLimit($account_uid, $product) {
    $session_level_limit = 'not_exist';
    if (!empty($product->field_session_level_limit) && !empty($product->field_session_level_limit->value)) {
      $session_level_limit = (int) $product->field_session_level_limit->value;
      // Select query to get session purchased limit for product.
      $query = \Drupal::entityQuery('session_level_checkout');
      $query->condition('field_user_id', $account_uid, '=');
      $query->condition('field_voucher_id', $product->id(), '=');
      $query->accessCheck(FALSE);
      $session_limit_result = $query->execute();
      if (!empty($session_limit_result)) {
        $purchased_quantity = 0;
        foreach ($session_limit_result as $session_limit_key => $session_limit_value) {
          $session_limit = SessionLevelCheckout::load($session_limit_value);
          if (!empty($session_limit)) {
            if (!empty($session_limit->field_quantity) && !empty($session_limit->field_quantity->value)) {
              $purchased_quantity = $purchased_quantity + $session_limit->field_quantity->value;
            }
          }
        }
        $session_level_limit = $session_level_limit - $purchased_quantity;
      }
    }

    return $session_level_limit;
  }

}
