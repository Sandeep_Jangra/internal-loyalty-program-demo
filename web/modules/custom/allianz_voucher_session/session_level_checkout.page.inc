<?php

/**
 * @file
 * Contains session_level_checkout.page.inc.
 *
 * Page callback for Session Level Checkout entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Session Level Checkout templates.
 *
 * Default template: session_level_checkout.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_session_level_checkout(array &$variables) {
  // Fetch SessionLevelCheckout Entity Object.
  $session_level_checkout = $variables['elements']['#session_level_checkout'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
