<?php

/**
 * @file
 * Contains allianz_voucher_session.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Database\Database;

/**
 * Implements hook_help().
 */
function allianz_voucher_session_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the allianz_voucher_session module.
    case 'help.page.allianz_voucher_session':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Allianz voucher module for session level checkout') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_cron().
 */
function allianz_voucher_session_cron() {
  // Get Table name for stock email thershold field.
  $storage = \Drupal::entityTypeManager()->getStorage('commerce_product_variation');
  $table_name = $storage->getTableMapping()->getFieldTableName('field_stock_email_threshold');
  // Get db connection.
  $connection = Database::getConnection();
  $query = $connection->select($table_name, 'es');
  $query->leftJoin('commerce_product_variation__field_stock', 's', 's.entity_id = es.entity_id');
  $query->fields('es', ['entity_id']);
  $query->Where('es.field_stock_email_threshold_value > s.field_stock_value');
  $results = $query->execute()->fetchAll();
  if (!empty($results)) {
    /** @var QueueFactory $queue_factory */
    $queue_factory = \Drupal::service('queue');
    /** @var QueueInterface $queue */
    $queue = $queue_factory->get('send_variation_email_queue');
    foreach ($results as $key => $value) {
      $queue->createItem($value->entity_id);
    }
  }

}

/**
 * Implements hook_views_data().
 */
function allianz_voucher_session_views_data() {
  $data['views']['table']['group'] = t('Custom Global');
  $data['views']['table']['join'] = [
    '#global' => [],
  ];
  $data['views']['voucher_stock_alert'] = [
    'title' => t('Voucher stock alert'),
    'help' => t('Display voucher stock alert.'),
    'field' => [
      'id' => 'voucher_stock_alert',
    ],
  ];
  $data['views']['voucher_variation_stock'] = [
    'title' => t('Voucher variation stock field'),
    'help' => t('Display voucher variation stock field.'),
    'field' => [
      'id' => 'voucher_variation_stock',
    ],
  ];
  return $data;
}
