<?php

namespace Drupal\allianz_sso\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\user\Entity\User;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "transaction_reversal",
 *   label = @Translation("Transaction Reversal"),
 *   uri_paths = {
 *     "create" = "/api/v1/transaction/reversal"
 *   }
 * )
 */
class TransactionReversal extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new TransactionReversal object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_sso'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param string $payload
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post($data) {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    if (!empty($authorization_token)) {
      // Validate OAuth token service.
      $validate_token_service = \Drupal::service('allianz_sso.validate_token');
      $token = $validate_token_service->validateOauthToken($authorization_token);
      if ($token['error'] < 400) {
        try {
          // Load user object for request user.
          $account = User::load($this->currentUser->id());
          if (!empty($account)) {
            if (!empty($data['order_id']) && !empty($data['product_id']) && !empty($data['reverse_points']) && !empty($data['date'])) {
              $error = 200;
              $response['status'] = "success";
            }
            else {
              // Return 404 if requested user does not exist.
              $response['status'] = 'failure';
              $response['error'] = 'Something went wrong!';
              $error = 500;
            }
          }
          else {
            // Return 404 if requested user does not exist.
            $response['status'] = 'failure';
            $response['error'] = 'User does not exits.';
            $error = 404;
          }
        }
        catch (RequestException $e) {
          $response['status'] = 'failure';
          $response['error'] = 'Something went wrong!';
          $error = 500;
        }
      }
      else {
        $response['status'] = 'failure';
        $response['error'] = 'Invalid or expired token!';
        $error = 401;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }

    return new ModifiedResourceResponse($response, $error);
  }

}
