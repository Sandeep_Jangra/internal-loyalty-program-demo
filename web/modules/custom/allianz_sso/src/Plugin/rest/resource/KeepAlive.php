<?php

namespace Drupal\allianz_sso\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a resource to maintain session.
 *
 * @RestResource(
 *   id = "keep_alive",
 *   label = @Translation("Keep alive"),
 *   uri_paths = {
 *     "create" = "/api/v1/keepalive"
 *   }
 * )
 */
class KeepAlive extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new KeepAlive object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_sso'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param string $payload
   *   Get data object from request.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post($payload) {
    // Get oauth2 token.
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    if (!empty($authorization_token)) {
      $cim_base_url = $_ENV['AMBASE_URL'];
      $request_url = $cim_base_url . '/custom/keepalive.jsp';
      $headers = [
        'Cookie' => $payload['cookie'],
      ];
      // Call drupal httpClient service.
      $client = \Drupal::httpClient();
      try {
        // Get request to check user session.
        $request = $client->request('GET', $request_url, [
          'headers' => $headers,
          'verify' => FALSE,
          'http_errors' => FALSE,
        ]);
        if ($request->getStatusCode() < 400) {
          $response = json_decode($request->getBody(), TRUE);
          $error = $request->getStatusCode();
        }
        else {
          $response = json_decode($request->getBody(), TRUE);
          $error = $request->getStatusCode();
        }
      }
      catch (RequestException $exception) {
        $response['error'] = 'Something went wrong';
        $error = 500;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['error'] = 'Permission denied';
      $error = 401;
    }
    $response = new ResourceResponse($response, $error);
    // Disable api cache.
    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);
    $response->addCacheableDependency($disable_cache);

    return $response;
  }

}
