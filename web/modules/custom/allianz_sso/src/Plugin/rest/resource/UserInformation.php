<?php

namespace Drupal\allianz_sso\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\user\Entity\User;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Provides a resource to get userinformation using access token.
 *
 * @RestResource(
 *   id = "user_information",
 *   label = @Translation("User information"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/userinfo"
 *   }
 * )
 */
class UserInformation extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new UserInformation object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
      array $configuration,
      $plugin_id,
      $plugin_definition,
      array $serializer_formats,
      LoggerInterface $logger,
      AccountProxyInterface $current_user,
      Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->getParameter('serializer.formats'),
          $container->get('logger.factory')->get('allianz_sso'),
          $container->get('current_user'),
          $container->get('request_stack')->getCurrentRequest()
      );
  }

  /**
   * Responds to GET requests.
   *
   * @param string $payload
   *   Get data object from request.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get($payload) {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    if (!empty($authorization_token)) {
      // Validate OAuth token service.
      $validate_token_service = \Drupal::service('allianz_sso.validate_token');
      $token = $validate_token_service->validateOauthToken($authorization_token);
      if ($token['error'] < 400) {
        try {
          // Load user object for request user.
          $account = User::load($this->currentUser->id());
          if (!empty($account)) {
            $response['status'] = 'success';
            $error = 200;
            $response['data']['title'] = $account->field_title->value;
            $response['data']['salutation'] = $account->field_salutation->value;
            $response['data']['email'] = $account->getEmail();
            $response['data']['first_name'] = $account->field_first_name->value;
            $response['data']['last_name'] = $account->field_last_name->value;
          }
          else {
            // Return 401 if requested token is invalid.
            $response['status'] = 'failure';
            $response['error'] = 'Permission denied';
            $error = 401;
          }
        }
        catch (RequestException $e) {
          $response['status'] = 'failure';
          $response['error'] = 'Something went wrong!';
          $error = 500;
        }
      }
      else {
        $response['status'] = 'failure';
        $response['error'] = 'Invalid or expired token!';
        $error = 401;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }
    $response = new ResourceResponse($response, $error);
    // Disable api cache.
    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);
    $response->addCacheableDependency($disable_cache);

    return $response;
  }

}
