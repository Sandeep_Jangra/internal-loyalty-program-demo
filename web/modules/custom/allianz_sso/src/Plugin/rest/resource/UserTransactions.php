<?php

namespace Drupal\allianz_sso\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\user\Entity\User;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "user_transactions",
 *   label = @Translation("User Transactions"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/transaction"
 *   }
 * )
 */
class UserTransactions extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new UserAddressInfo object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_sso'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Responds to GET requests.
   *
   * @param string $payload
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get($data) {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    if (!empty($authorization_token)) {
      // Validate OAuth token service.
      $validate_token_service = \Drupal::service('allianz_sso.validate_token');
      $token = $validate_token_service->validateOauthToken($authorization_token);
      if ($token['error'] < 400) {
        try {
          // Load user object for request user.
          $account = User::load($this->currentUser->id());
          if (!empty($account)) {
            if ($member_details['error'] < 400) {
              $response['status'] = 'success';
              $accountId = [
                "uniqueId" => "avpClientNumber5.Bonus Points",
                "ctxType" => ""
              ];
              $bookings = [
                [
                  "balance" => 199974.5,
                  "timestamp" => "2020-03-14T11:42:17.1413420Z",
                  "date" => "2020-03-14T00:00:00.0000000Z",
                  "amount" => 35,
                  "relatedAccount" => "ALLIANZ",
                  "receiptId" => [
                    "uniqueId" => "artcle_001-REVERSAL",
                    "ctxType" => ""
                  ],
                  "description" => "string",
                  "references" => []
                ],
                [
                  "balance" => 200000,
                  "timestamp" => "22020-02-18T06:57:39.1638246Z",
                  "date" => "2020-02-18T00:00:00.0000000Z",
                  "amount" => 200000,
                  "relatedAccount" => "ALLIANZ",
                  "receiptId" => [
                    "uniqueId" => "artcle_002-REVERSAL",
                    "ctxType" => ""
                  ],
                  "description" => "string",
                  "references" => []
                ]
              ];
              $response['data']['accountId'] = $accountId;
              $response['data']['ccy'] = "_BPT";
              $response['data']['balance'] = 199974.5;
              $response['data']['date'] = "2020-03-14T00:00:00.0000000Z";
              $response['data']['totalBookings'] = 5;
              $response['data']['since'] = "2019-01-01T11:25:09.1190040Z";
              $response['data']['until'] = "2020-03-14T12:13:40.7198550Z";
              $response['data']['bookings'] = $bookings;

              $error = 200;
            }
            else {
              // Return 400 if requested member key does not exist on prime cloud.
              $response['status'] = 'failure';
              $response['error'] = 'Invalid member key!';
              $error = 400;
            }
          }
          else {
            // Return 404 if requested user does not exist.
            $response['status'] = 'failure';
            $response['error'] = 'User does not exits.';
            $error = 404;
          }
        }
        catch (RequestException $e) {
          $response['status'] = 'failure';
          $response['error'] = 'Something went wrong!';
          $error = 500;
        }
      }
      else {
        $response['status'] = 'failure';
        $response['error'] = 'Invalid or expired token!';
        $error = 401;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }
    $response = new ResourceResponse($response, $error);
    // Disable api cache.
    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);
    $response->addCacheableDependency($disable_cache);

    return $response;
  }

}
