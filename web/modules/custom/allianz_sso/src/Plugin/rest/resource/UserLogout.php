<?php

namespace Drupal\allianz_sso\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * UserLogout custom rest api that invalidate user token on logout.
 *
 * @RestResource(
 *   id = "user_logout",
 *   label = @Translation("User Logout for PD"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/user/logout"
 *   }
 * )
 */
class UserLogout extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new UserLogout object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
      array $configuration,
      $plugin_id,
      $plugin_definition,
      array $serializer_formats,
      LoggerInterface $logger,
      AccountProxyInterface $current_user,
      Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->getParameter('serializer.formats'),
          $container->get('logger.factory')->get('allianz_cim'),
          $container->get('current_user'),
          $container->get('request_stack')->getCurrentRequest()
      );
  }

  /**
   * Responds to GET requests.
   *
   * @param string $payload
   *   Get payload on request.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get($payload) {
    $authorization_token = trim(str_replace('Bearer ', '', $this->request->headers->get('Authorization')));
    if (!empty($authorization_token)) {
      $connection = Database::getConnection();
      $query = $connection->select('custom_jwt_token', 'c')
        ->fields('c', ['uid', 'jwt_token', 'oidc_token'])
        ->condition('c.uid', $this->currentUser->id(), '=')
        ->execute();
      $results = $query->fetchAssoc();
      if (!empty($results)) {
        // Hit cim logout api.
        $redirect_uri = $_ENV['REDIRECT_URI'];
        $logout_url = $redirect_uri . '/pd_logout?oidc_token=' . $results['oidc_token'];
        $result = $connection->update('custom_jwt_token')
          ->fields([
            'jwt_token' => '',
          ])
          ->condition('uid', $this->currentUser->id(), '=')
          ->execute();
        // invalidate user access token for pd  
        $num_deleted = $connection->delete('oauth2_token')
          ->condition('auth_user_id', $this->currentUser->id())
          ->condition('bundle', "access_token")
          ->execute();  
        $response['status'] = 'success';
        $response['logut_url'] = $logout_url;
        $error = 200;
      }
      else {
        // invalidate user access token for pd  
        $num_deleted = $connection->delete('oauth2_token')
          ->condition('auth_user_id', $this->currentUser->id())
          ->condition('bundle', "access_token")
          ->execute();
        // Return 200 if requested user  invalid.
        $response['status'] = 'success';
        $error = 200;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }
    $response = new ResourceResponse($response, $error);
    // Disable api cache.
    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);
    $response->addCacheableDependency($disable_cache);

    return $response;
  }

}
