<?php

namespace Drupal\allianz_sso\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\Client;

/**
 * Class OauthTokenValidateService used to validate oauth token.
 *
 * @package \Drupal\allianz_sso\Services
 */
class OauthTokenValidateService {

  /**
   * Config Factory Interface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Guzzle HTTP Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * OauthTokenValidateService constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Config Factory Interface.
   * @param \GuzzleHttp\Client $client
   *   Guzzle HTTP Client.
   */
  public function __construct(LoggerChannelFactoryInterface $logger, ConfigFactoryInterface $config, Client $client) {
    $this->logger = $logger->get('allianz_sso');
    $this->client = $client;
  }

  /**
   * Retrieves the Base URL.
   */
  protected function getBaseUrl() {
    $base_url = \Drupal::request()->getSchemeAndHttpHost();
    return $base_url;
  }

  /**
   * Get request to validate oauth token.
   */
  public function validateOauthToken($access_token) {
    $request_url = $this->getBaseUrl() . '/oauth/debug';
    $headers = [
      'Authorization' => 'Bearer ' . $access_token,
      'providerId' => 'oauth2',
      'Content-Type'  => 'application/json'
    ];

    try {
      $request = $this->client->request('GET', $request_url, ['headers' => $headers, 'http_errors' => FALSE]);
      if ($request->getStatusCode() < 400) {
        $response['response_data'] = json_decode($request->getBody(), TRUE);
        $response['error'] = $request->getStatusCode();
      }
      else {
        $response['error'] = $request->getStatusCode();
      }
    }
    catch (RequestException $exception) {
      $this->logger->error($exception->getMessage());
      $response['error'] = 500;
    }

    return $response;
  }

}
