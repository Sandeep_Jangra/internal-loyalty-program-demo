<?php

namespace Drupal\allianz_sso\Controller;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\simple_oauth\Entities\UserEntity;
use Drupal\simple_oauth\KnownClientsRepositoryInterface;
use Drupal\simple_oauth\Plugin\Oauth2GrantManagerInterface;
use GuzzleHttp\Psr7\Response;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Entities\ScopeEntityInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\RequestTypes\AuthorizationRequest;
use Symfony\Bridge\PsrHttpMessage\HttpMessageFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\user\Entity\User;
use Drupal\Core\Database\Database;

/**
 * Oauth2AuthorizeController.
 */
class Oauth2AuthorizeController extends ControllerBase {

  /**
   * The message factory.
   *
   * @var \Symfony\Bridge\PsrHttpMessage\HttpMessageFactoryInterface
   */
  protected $messageFactory;

  /**
   * The grant manager.
   *
   * @var \Drupal\simple_oauth\Plugin\Oauth2GrantManagerInterface
   */
  protected $grantManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The known client repository service.
   *
   * @var \Drupal\simple_oauth\KnownClientsRepositoryInterface
   */
  protected $knownClientRepository;

  /**
   * Oauth2AuthorizeController construct.
   *
   * @param \Symfony\Bridge\PsrHttpMessage\HttpMessageFactoryInterface $message_factory
   *   The PSR-7 converter.
   * @param \Drupal\simple_oauth\Plugin\Oauth2GrantManagerInterface $grant_manager
   *   The plugin.manager.oauth2_grant.processor service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\simple_oauth\KnownClientsRepositoryInterface $known_clients_repository
   *   The known client repository service.
   */
  public function __construct(
    HttpMessageFactoryInterface $message_factory,
    Oauth2GrantManagerInterface $grant_manager,
    ConfigFactoryInterface $config_factory,
    KnownClientsRepositoryInterface $known_clients_repository
  ) {
    $this->messageFactory = $message_factory;
    $this->grantManager = $grant_manager;
    $this->configFactory = $config_factory;
    $this->knownClientRepository = $known_clients_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('psr7.http_message_factory'),
      $container->get('plugin.manager.oauth2_grant.processor'),
      $container->get('config.factory'),
      $container->get('simple_oauth.known_clients')
    );
  }

  /**
   * Authorizes the code generation or prints the confirmation form.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming request.
   *
   * @return mixed
   *   The response.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function authorize(Request $request) {
    $authorization_token = trim(str_replace('Bearer ', '', $request->headers->get('Authorization')));
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $jwt = $authorization_token;
    $token = $transcoder->decode($jwt);
    $account_uid = $token->getPayload()->drupal->uid;
    $account = User::load($account_uid);
    // Get db connection.
    $connection = Database::getConnection();
    // Select query to get requested user token for authentication.
    $query = $connection->select('custom_jwt_token', 'c')
      ->fields('c', ['uid', 'jwt_token'])
      ->condition('c.uid', $account_uid, '=')
      ->execute();
    $results = $query->fetchAssoc();
    if ((!empty($results) && $results['jwt_token'] == $jwt)) {
      $client_uuid = $request->get('client_id');
      if (empty($client_uuid)) {
        // Return 500 if client is invalid.
        $response['status'] = 'failure';
        $response['error'] = 'Invalid client';
        $error = 500;
      }
      $consumer_storage = $this->entityTypeManager()->getStorage('consumer');
      $client_drupal_entities = $consumer_storage
        ->loadByProperties([
          'uuid' => $client_uuid,
        ]);
      if (empty($client_drupal_entities)) {
        // Return 500 if client is invalid.
        $response['status'] = 'failure';
        $response['error'] = 'Invalid client';
        $error = 500;
      }

      $client_drupal_entity = reset($client_drupal_entities);
      $is_third_party = $client_drupal_entity->get('third_party')->value;

      $scopes = [];
      if ($request->query->get('scope')) {
        $scopes = explode(' ', $request->query->get('scope'));
      }
      $known_client = 1;
      if ($this->currentUser()->isAnonymous()) {
        // Return 302 if requested token is invalid.
        $response['status'] = 'failure';
        $response['error'] = 'An external client application is requesting access to your data in this site. Please log in first to authorize the operation.';
        $error = 302;
      }
      else {
        // Login user may skip the grant step if the client is not third party or
        // known.
        if ($request->get('response_type') == 'code') {
          $grant_type = 'code';
        }
        elseif ($request->get('response_type') == 'token') {
          $grant_type = 'implicit';
        }
        else {
          $grant_type = NULL;
        }
        try {
          $server = $this->grantManager->getAuthorizationServer($grant_type, $client_drupal_entity);
          $ps7_request = $this->messageFactory->createRequest($request);
          $auth_request = $server->validateAuthorizationRequest($ps7_request);
        }
        catch (OAuthServerException $exception) {
          // Return 500 if unable to authorization server.
          $response['status'] = 'failure';
          $response['error'] = 'Unable to get the authorization server.';
          $error = 500;
        }
        if ($auth_request) {
          $can_grant_codes = $this->currentUser()
            ->hasPermission('grant simple_oauth codes');
          return static::redirectToCallback(
            $auth_request,
            $server,
            $this->currentUser,
            $can_grant_codes
          );
        }
        else {
          // Return 500 if auth request is invalid.
          $response['status'] = 'failure';
          $response['error'] = 'Unable to get the authorization server.';
          $error = 500;
        }

      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Permission denied';
      $error = 401;
    }
    \Drupal::service('page_cache_kill_switch')->trigger();
    return new JsonResponse($response, $error);

  }

  /**
   * Generates a redirection response to the consumer callback.
   *
   * @param \League\OAuth2\Server\RequestTypes\AuthorizationRequest $auth_request
   *   The auth request.
   * @param \League\OAuth2\Server\AuthorizationServer $server
   *   The authorization server.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The user to be logged in.
   * @param bool $can_grant_codes
   *   Weather or not the user can grant codes.
   * @param bool $remembers_clients
   *   Weather or not the sites remembers consumers that were previously
   *   granted access.
   * @param \Drupal\simple_oauth\KnownClientsRepositoryInterface|null $known_clients_repository
   *   The known clients repository.
   */
  public static function redirectToCallback(
    AuthorizationRequest $auth_request,
    AuthorizationServer $server,
    AccountInterface $current_user,
    $can_grant_codes,
    $remembers_clients = FALSE,
    KnownClientsRepositoryInterface $known_clients_repository = NULL
  ) {
    // Once the user has logged in set the user on the AuthorizationRequest.
    $user_entity = new UserEntity();
    $user_entity->setIdentifier($current_user->id());
    $auth_request->setUser($user_entity);
    // Once the user has approved or denied the client update the status
    // (true = approved, false = denied).
    $auth_request->setAuthorizationApproved($can_grant_codes);
    // Return the HTTP redirect response.
    $response = $server->completeAuthorizationRequest($auth_request, new Response());

    return new JsonResponse($response->getHeaders());
  }

}
