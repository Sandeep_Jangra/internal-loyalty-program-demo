<?php

namespace Drupal\allianz_cim\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\jwt\Authentication\Event\JwtAuthGenerateEvent;
use Drupal\jwt\JsonWebToken\JsonWebToken;
use Drupal\jwt\Authentication\Event\JwtAuthEvents;
use Drupal\allianz_voucher_session\Entity\SessionLevelCheckout;

/**
 * ValidateUser custom rest post api that validate user on login.
 *
 * @RestResource(
 *   id = "validate_user",
 *   label = @Translation("Validate User"),
 *   uri_paths = {
 *     "create" = "/api/v1/validate/user"
 *   }
 * )
 */
class ValidateUser extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new ValidateUser object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_checkout'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
      );
  }

  /**
   * Responds to POST requests.
   *
   * @param string $data
   *   Get data object on request.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post($data) {
    try {
      // Load dispatcher object through services.
      $dispatcher = \Drupal::service('event_dispatcher');
      // Load jwt transcoder object through services.
      $transcoder = \Drupal::service('jwt.transcoder');
      $event = new JwtAuthGenerateEvent(new JsonWebToken());
      $dispatcher->dispatch(JwtAuthEvents::GENERATE, $event);
      // Generate jwt token for requested user.
      $event->addClaim('exp', strtotime('+2000 minutes'));
      $event->addClaim(['drupal', 'uid'], $this->currentUser->id());
      $jwt = $event->getToken();
      $token = $transcoder->encode($jwt);
      // Get db connection.
      $connection = Database::getConnection();
      // Select query to get requested user token for authentication.
      $query = $connection->select('custom_jwt_token', 'c')
        ->fields('c', ['uid'])
        ->condition('c.uid', $this->currentUser->id(), '=')
        ->execute();
      $results = $query->fetchAssoc();
      if (empty($results)) {
        // Insert the jwt token for requeted user.
        $connection->insert('custom_jwt_token')
          ->fields([
            'uid',
            'jwt_token',
            'oidc_token',
          ])
          ->values([
            $this->currentUser->id(),
            $token,
            $token,
          ])
          ->execute();
      }
      else {
        // Update token for requested user.
        $token_updated = $connection->update('custom_jwt_token')
          ->fields([
            'jwt_token' => $token,
          ])
          ->condition('uid', $this->currentUser->id(), '=')
          ->execute();
      }
      // Deleted session level limit records for voucher during login.
      $query = \Drupal::entityQuery('session_level_checkout');
      $query->condition('field_user_id', $this->currentUser->id(), '=');
      $query->accessCheck(FALSE);
      $session_limit_result = $query->execute();
      if (!empty($session_limit_result)) {
        $purchased_quantity = 0;
        foreach ($session_limit_result as $session_limit_key => $session_limit_value) {
          $session_limit = SessionLevelCheckout::load($session_limit_value);
          if (!empty($session_limit)) {
            $session_limit->delete();
          }
        }
      }
      $response['status'] = 'success';
      $response['data']['token'] = $token;
      \Drupal::logger('drupal_jwt')->debug('<pre>' . json_encode($token) . '</pre>');
      $error = 200;
    }
    catch (RequestException $e) {
      $response['status'] = 'failure';
      $response['error'] = 'Something went wrong!';
      $error = 500;
    }
    \Drupal::service('page_cache_kill_switch')->trigger();
    $response = new ModifiedResourceResponse($response, $error);

    return $response;
  }

}
