<?php

namespace Drupal\allianz_cim\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;

/**
 * ValidateToken custom rest post api that validate user token.
 *
 * @RestResource(
 *   id = "validate_token",
 *   label = @Translation("Validate Token"),
 *   uri_paths = {
 *     "create" = "/api/v1/validate/token"
 *   }
 * )
 */
class ValidateToken extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new ValidateToken object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('allianz_checkout'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
      );
  }

  /**
   * Responds to POST requests.
   *
   * @param string $data
   *   Get data object on request.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post($data) {
    // Load jwt transcoder object through services.
    $transcoder = \Drupal::service('jwt.transcoder');
    $jwt = $data['token'];
    $token = $transcoder->decode($jwt);
    if (!empty($token)) {
      $token_exp = $token->getPayload()->exp;
      $account_uid = $token->getPayload()->drupal->uid;
      // Get db connection.
      $connection = Database::getConnection();
      // Select query to get requested user token for authentication.
      $query = $connection->select('custom_jwt_token', 'c')
        ->fields('c', ['uid', 'jwt_token'])
        ->condition('c.uid', $account_uid, '=')
        ->execute();
      $results = $query->fetchAssoc();
      if ((!empty($results) && $results['jwt_token'] == $jwt)) {
        if ($token_exp > time()) {
          $response['status'] = 'success';
          $error = 200;
        }
        else {
          // Return 401 if requested token is expired.
          $response['status'] = 'failure';
          $response['error'] = 'Invalid Token!';
          $error = 401;
        }
      }
      else {
        // Return 401 if requested token is invalid.
        $response['status'] = 'failure';
        $response['error'] = 'Invalid Token!';
        $error = 401;
      }
    }
    else {
      // Return 401 if requested token is invalid.
      $response['status'] = 'failure';
      $response['error'] = 'Invalid Token!';
      $error = 401;
    }

    $response = new ModifiedResourceResponse($response, $error);
    return $response;
  }

}
